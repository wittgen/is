package is.util;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.HashMap;

public class MainFrame extends JPanel implements ComponentListener
{
    private static final Color bgColor = new java.awt.Color( 255, 225, 170 );    
    private static final int initial_font_size = 12;
    private int font_size = initial_font_size;
    private Dimension dimension;
    private JPanel labels_panel = new JPanel();
    private JPanel values_panel = new JPanel();
              
    public MainFrame( String partition_name, JFrame parent )
    {
        setLayout( new BoxLayout( this, BoxLayout.LINE_AXIS ) );
	setBorder( new EmptyBorder( 5, 5, 5, 5 ) );
        setBackground( bgColor );
        
	labels_panel.setLayout( new GridLayout( 0, 1, 2, 2 ) );
	values_panel.setLayout( new GridLayout( 0, 1, 2, 2 ) );
	labels_panel.setBorder( new EmptyBorder( 0, 0, 0, 10 ) );
        labels_panel.setBackground( bgColor );
        values_panel.setBackground( bgColor );
        
        add( labels_panel );
        add( values_panel );
        
        ipc.Partition partition = new ipc.Partition( partition_name );
        
        InfoReceiver r = new RunInfoReceiver( partition, bgColor );
        r.layout( labels_panel, values_panel );
        
        r = new TriggerSMKReceiver( partition, bgColor );
        r.layout( labels_panel, values_panel );
        
        r = new LumiInfoReceiver( partition, bgColor );
        r.layout( labels_panel, values_panel );
        
        r = new LumiLengthReceiver( partition, bgColor );
        r.layout( labels_panel, values_panel );
        
        r = new RunDurationReceiver( partition, bgColor );
        r.layout( labels_panel, values_panel );
        
        r = new RunStateReceiver( partition, bgColor );
        r.layout( labels_panel, values_panel );
        
        r = new InfoReceiver( partition, "DF", ".*:HLT.Counters.Global", 
        		new int[] { 2, 3 }, new String[] { "Built Events", "Event Building Rate" }, bgColor );
        r.layout( labels_panel, values_panel );
        
        r = new DetectorMaskReceiver( partition, bgColor, parent );
        r.layout( labels_panel, values_panel );

        addComponentListener( this );
	
        // wait half second so subscribers will be notified by IS
        // and fill up their values to widgets
        // In this case initial size of the window will be correctly calculated
        try {
            Thread.sleep( 500 );
	}
	catch( java.lang.InterruptedException ex ) {
	    return;
	}
    }
    
    public void componentResized(ComponentEvent e) 
    {        
        if ( dimension == null )
        {
            dimension = getSize();
            return;
        }
        
        Dimension d = getSize();
        float k = Math.min( (float)d.height/(float)dimension.height, (float)d.width/(float)dimension.width );
        
        int s = (int)((float)initial_font_size * k + ( k > 1.0 ? -0.5 : 0.5 ) );
        if ( font_size != s )
        {
            Component[] l = labels_panel.getComponents();
            Component[] v = values_panel.getComponents();
            for ( int i = 0; i < l.length; ++i )
            {
		l[i].setFont( l[i].getFont().deriveFont( (float)s ) );
		v[i].setFont( v[i].getFont().deriveFont( (float)s ) );
            }
            font_size = s;
        }
    }
    
    public void componentHidden(ComponentEvent e) { }

    public void componentMoved(ComponentEvent e) { }

    public void componentShown(ComponentEvent e) { }
}

