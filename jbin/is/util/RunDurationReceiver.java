package is.util;

import java.awt.*;
import java.util.*;
import javax.swing.JLabel;

class RunDurationReceiver extends InfoReceiver
{
    class Publisher extends TimerTask
    {
        RunDurationReceiver receiver;
        int counter = 0;
        
        public Publisher( RunDurationReceiver receiver )
        {
            this.receiver = receiver;
        }

        //! the action to be performed when the timer takes action
        public void run()
        {
	    if ( counter++ == 10 )
            	cancel();
            else
		receiver.update( );
        }
    }
    
    private int seconds;
    private Publisher publisher;
    private Timer timer = new Timer();
    private boolean first_notification_received = false;
    
    RunDurationReceiver( ipc.Partition partition, Color bg_color )
    {
	super( partition, "RunParams.RunInfo",
        	new int[] { 0 }, new String[] { "Active Time" }, bg_color );
    }

    protected void update( int pos, Object v )
    {        
        try {
            seconds = (Integer)v;
        }
        catch( Exception ex ) {
	    super.update( pos, v );
            return ;
        }
        
	if ( first_notification_received == false ) {
	    first_notification_received = true;
	    update( );
            return;
        }
	
        timer.purge();
        
        try {
            timer.schedule( publisher = new Publisher(this), 0, 1000 );
        }
        catch( Exception ex ) {
	    System.err.println( ex );
        }
    }
    
    protected void update( )
    {        
	++seconds;
        String h = String.valueOf( seconds/3600 );
	if ( h.length() == 1 ) h = "0" + h;
	String m = String.valueOf( seconds%3600/60 );
	if ( m.length() == 1 ) m = "0" + m;
	String s = String.valueOf( seconds%3600%60 );
	if ( s.length() == 1 ) s = "0" + s;
        String v = h + ":" + m + ":" + s;

	super.update( 0, v );
    }
}

