package is.util;

import java.awt.*;
import java.util.*;
import javax.swing.JLabel;

class TriggerSMKReceiver extends InfoReceiver
{
    private static final java.text.SimpleDateFormat time_formatter
		 = new java.text.SimpleDateFormat( "HH:mm:ss", java.util.Locale.US );
                 
    TriggerSMKReceiver( ipc.Partition partition, Color bg_color )
    {
	super( partition, "RunParams.TrigConfSmKey", 
        	new int[] { 0 }, new String[] { "Super Master Key" }, bg_color );
    }

    protected void updateAll( is.AnyInfo info )
    {                
	try {
            update( 0, info.getAttribute( 0 ) );
            values[0].setToolTipText( (String)info.getAttribute( 1 ) );
	}
	catch( Exception ex ) {
	    super.updateAll( info );
	}        	
    }
}

