package is.util;

import java.awt.*;
import java.util.*;
import javax.swing.JLabel;

class RunInfoReceiver extends InfoReceiver
{
    private static final java.text.SimpleDateFormat time_formatter
		 = new java.text.SimpleDateFormat( "yyyy-MMM-dd HH:mm:ss", java.util.Locale.US );
    private static final String empty_string = new String( " " );
                 
    RunInfoReceiver( ipc.Partition partition, Color bg_color )
    {
	super( partition, "RunParams", ".OR_RunParams", 
        	new int[] { 0, 4, 9, 10, 11 }, 
                new String[] { "Run Number", "Run Type", "T0 Project Tag", "Start time", "Stop time" }, bg_color );
    }

    protected void update( int pos, Object v )
    {        
        if ( pos > 2 )
        {
	    try {
		java.util.Date d = (java.util.Date)v;
                if ( d.getTime() != 0 )
		    v = time_formatter.format( d );
                else
                    v = empty_string;
	    }
	    catch( Exception ex ) {
	    }
	}
        	
        super.update( pos, v );
    }
}

