package is.util;

import java.awt.*;
import java.awt.*;
import javax.swing.*;

class DetectorMaskReceiver extends InfoReceiver
{
    private DetectorMask dmask;
    
    DetectorMaskReceiver( ipc.Partition partition, Color bg_color, JFrame parent )
    {
	super( partition, "RunParams.RunParams", 
        	new int[] { 5 }, new String[] { "Detector Mask" }, bg_color );

        JLabel value = values[0];
	value.addMouseListener(new java.awt.event.MouseAdapter()  {
	    public void mouseClicked(java.awt.event.MouseEvent e) {
		dmask.show();
	    }
	});
        
        value.setHorizontalTextPosition( SwingConstants.LEADING );
        value.setIcon( new ImageIcon( getClass().getResource("/images/clickme.gif") ) );        
    }

    protected void update( int pos, Object v )
    {
	if ( dmask == null )
	{
	    dmask = new DetectorMask();
	}
       
	try {
            String s = (String)v;
            dmask.setValue( s );
	    super.update( pos, s );
	}
	catch( Exception ex ) {
	}        
    }
}

