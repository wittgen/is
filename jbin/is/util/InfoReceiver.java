package is.util;

import java.util.Date;
import java.util.TreeMap;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.HashMap;

class InfoReceiver extends is.InfoWatcher implements Runnable
{
    private ipc.Partition partition;
    private Thread thread;
    private boolean subscribed = false;
    private boolean repository_exists = false;
    private String server;
    private int indexes[];
    private PartitionStateListener listener;
    
    private static final java.awt.Font label_font = new java.awt.Font("Tahoma", 1, 12);
    private static final java.awt.Font value_font = new java.awt.Font("Tahoma", 0, 12);
    private static final Color fgColor = new java.awt.Color( 0, 64, 128 );    
    
    protected JLabel labels[];
    protected JLabel values[];
    
    protected static final String UNDEFINED = "";
    
    protected static class DummyListener implements PartitionStateListener
    {
	public void stopped() { ; }
	public void started() { ; }
    }

    protected class GUIUpdater implements Runnable
    {
	private is.AnyInfo info;
        
        public GUIUpdater( is.AnyInfo info )
        {
            this.info = info;
        }
        
        public void run()
        {
            if ( info != null )
            	updateAll( info );
            else
            	resetAll();
	}
    }
    
    private static Integer[] sortByTime( is.InfoList list )
    {
    	TreeMap<Date,Integer> tm = new TreeMap<Date,Integer>();
        for ( int i = 0; i < list.getSize(); ++i )
        {
            tm.put( list.getTime( i ), i );
        }
        
        return tm.values().toArray( new Integer[0] );
    }

    InfoReceiver(	ipc.Partition partition, 
    			String server_name, 
                        String pattern,
                        int indexes[], 
                        String names[],
                        Color bgColor )
    {
	super( partition, server_name, java.util.regex.Pattern.compile( pattern ) );
        this.partition = partition;
	this.server = server_name;
	this.indexes = indexes;
        this.listener = new DummyListener();
	
        labels = new JLabel[names.length];
        values = new JLabel[names.length];
        for ( int i = 0; i < names.length; ++i )
        {
            labels[i] = new JLabel( names[i] );
	    labels[i].setFont( label_font );
	    labels[i].setBackground( bgColor );
	    labels[i].setForeground( fgColor );
            
            values[i] = new JLabel( " " );
            values[i].setOpaque( true );
	    values[i].setFont( value_font );
	    values[i].setBackground( bgColor );
        }
        
	resetAll();

	thread = new Thread( this );
	thread.start();
    }

    InfoReceiver(	ipc.Partition partition, 
    			String info_name, 
                        int indexes[], 
                        String names[],
                        Color bgColor )
    {
	super( partition, info_name );
        this.partition = partition;
	this.server = info_name.substring( 0, info_name.indexOf( '.' ) );
	this.indexes = indexes;
        this.listener = new DummyListener();
	
        labels = new JLabel[names.length];
        values = new JLabel[names.length];
        for ( int i = 0; i < names.length; ++i )
        {
            labels[i] = new JLabel( names[i] );
	    labels[i].setFont( label_font );
	    labels[i].setBackground( bgColor );
	    labels[i].setForeground( fgColor );
            
            values[i] = new JLabel( " " );
            values[i].setOpaque( true );
	    values[i].setFont( value_font );
	    values[i].setBackground( bgColor );
        }
        
	resetAll();

	thread = new Thread( this );
	thread.start();
    }

    public void layout( JPanel labels_panel, JPanel values_panel )
    {
        for ( int i = 0; i < labels.length; ++i )
        {
	    labels_panel.add( labels[i] );
        }
        
        for ( int i = 0; i < values.length; ++i )
        {
	    values_panel.add( values[i] );
        }
    }
    
    public void setPartitionStateListener( PartitionStateListener listener )
    {
	this.listener = listener;
    }
    
    public void finalize()
    {
	if ( subscribed )
	{
	    try {
		unsubscribe();
	    }
	    catch( Exception ex ) {
		// do not care
	    }
	}
    }
    
    void updateRepositoryStatus( boolean new_status )
    {
	if ( repository_exists != new_status )
	{
	    repository_exists = new_status;
	    if ( repository_exists )
	    {
		listener.started();
	    }
	    else
	    {
		listener.stopped();
	    }
	}
    }

    public void run()
    {
	while( true )
	{
	    if ( !subscribed )
	    {
		try {
		    subscribe();
		    subscribed = true;
		    updateRepositoryStatus( true );
		}
		catch( is.RepositoryNotFoundException ex ) {
		    updateRepositoryStatus( false );
		}
		catch( Exception ex ) {
		    // must never happen
		}
		continue;
	    }

	    try {
		updateRepositoryStatus( partition.isObjectValid( is.repository.class, server ) );
	    }
	    catch(ipc.InvalidPartitionException e) {
		updateRepositoryStatus( false );
	    }

	    try {
		thread.sleep( 5000 );
	    }
	    catch( java.lang.InterruptedException ex ) {
		return;
	    }
	}
    }

    protected void update( int pos, Object value )
    {
	values[pos].setText( value.toString() );
    }

    protected void updateAll( is.AnyInfo info )
    {
	for ( int i = 0; i < indexes.length; ++i )
        {
            update( i, info.getAttribute( indexes[i] ) );
        }
    }

    protected void resetAll(  )
    {
	for ( int i = 0; i < indexes.length; ++i )
        {
            update( i, UNDEFINED );
        }
    }

    /**
     * Invoked when information has been updated.
     * @param e info event
     */
    public void infoSubscribed( is.InfoEvent e )
    {
	is.AnyInfo info = new is.AnyInfo();
        try {
            e.getValue( info );
            SwingUtilities.invokeLater( new GUIUpdater( info ) );
        }
        catch ( Exception ex ) {
            // must not happen;
        }
    }

    /**
     * Invoked when information has been created.
     * @param e info event
     */
    public void infoCreated( is.InfoEvent e )
    {
	is.AnyInfo info = new is.AnyInfo();
        try {
            e.getValue( info );
            SwingUtilities.invokeLater( new GUIUpdater( info ) );
        }
        catch ( Exception ex ) {
            // must not happen;
        }
    }

    /**
     * Invoked when information has been updated.
     * @param e info event
     */
    public void infoUpdated( is.InfoEvent e )
    {
	is.AnyInfo info = new is.AnyInfo();
        try {
            e.getValue( info );
            SwingUtilities.invokeLater( new GUIUpdater( info ) );
        }
        catch ( Exception ex ) {
            // must not happen;
        }
    }

    /**
     * Invoked when information has been deleted.
     * @param e info event
     */
    public void infoDeleted( is.InfoEvent e )
    {
        SwingUtilities.invokeLater( new GUIUpdater( null ) );
    }
}

