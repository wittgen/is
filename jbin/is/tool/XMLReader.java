package is.tool;

import java.io.*;
import java.util.*;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

class XMLReader extends DefaultHandler
{
    private static final String	XML_CLASS = "class";
    private static final String	XML_SUPERCLASS = "superclass";
    private static final String XML_ATTRIBUTE = "attribute";
    private static final String XML_RELATIONSHIP = "relationship";
    private static final String	XML_NAME = "name";
    private static final String	XML_TYPE = "type";
    private static final String	XML_CLASS_TYPE = "class-type";
    private static final String	XML_RANGE = "range";
    private static final String	XML_DESCRIPTION = "description";
    private static final String	XML_ATTRIBUTE_MULTI_VALUE = "is-multi-value";
    private static final String	XML_RELATIONSHIP_MULTI_VALUE = "high-cc";
    private static final String	XML_RELATIONSHIP_MULTI_VALUE_IMPL = "multi-value-implementation";
    private static final String	XML_INIT_VALUE = "init-value";
    private static final String	XML_ENUM_TYPE = "enum";
    
    private Repository.File	file;
    private Repository.Class	current_class;
    
    private XMLReader( Repository.File file )
    {
        super();
	this.file = file;
    }

    static void read( Repository.File file )
    {	
	try
	{           
	    SAXParserFactory factory = SAXParserFactory.newInstance();
	    factory.setValidating(true);
	    
	    SAXParser saxParser = factory.newSAXParser();

            XMLReader xml_handler = new XMLReader( file );
	    
	    FileReader reader = new FileReader(file.name);
            saxParser.parse( new InputSource(reader), xml_handler );
        }
        catch (FileNotFoundException e)
	{
            System.out.println("ERROR [File Not Found] : " + file.name ) ;
            e.printStackTrace() ;
        }
        catch (IOException e)
        {
            System.out.println("ERROR [IO] :" + e ) ;
            e.printStackTrace() ;
        }
        catch ( SAXException e )
	{
            System.out.println("ERROR [XML Parsing] :" + e ) ;
            e.printStackTrace() ;
        }
	catch ( ParserConfigurationException e )
	{
            System.out.println("ERROR [XML Parsing] :" + e ) ;
            e.printStackTrace() ;
        }
    }

    public void startDocument () {}


    public void endDocument ()
    {
	if ( current_class != null )
	{
	    if ( current_class.superclass == null )
	    {
		System.err.println(   "ERROR:: The " + current_class.name + " class does not inherit the Info class.\n" );
		System.err.println(   "        This class will be ignored." );
		System.err.println(   "ADVICE: Execute the `is_edit_repository.sh <your_xml_file>` command and\n"
				    + "        makes all your classes inherit the Info class (either directly or indirectly)." );
	    }
	    else
	    {
		file.addClass( current_class );	    
	    }
	}
    }


    public void startElement(String namespaceURI,
                             String sName, // simple name (localName)
                             String qName, // qualified name
                             Attributes attrs)
    {
        String eName = sName; // element name
        if ("".equals(eName)) eName = qName; // namespaceAware = false

	if ( XML_CLASS.equals(eName) )
	{
	    if ( current_class != null )
	    {
	    	if ( current_class.superclass == null )
		{
		    System.err.println(   "ERROR:: The " + current_class.name + " class does not inherit the Info class.\n" );
		    System.err.println(   "        This class will be ignored." );
		    System.err.println(   "ADVICE: Execute the `is_edit_repository.sh <your_xml_file>` command and\n"
				  	+ "        makes all your classes inherit the Info class (either directly or indirectly)." );
		}
		else
		{
		    file.addClass( current_class );	    
		}
	    }
	    //
	    // Reading class
	    //
	    String name = null, description = null;
	    for (int i = 0; i < attrs.getLength(); i++)
	    {
                String aName = attrs.getLocalName(i);
                if ("".equals(aName)) 
			aName = attrs.getQName(i);
		
		if (XML_NAME.equals(aName))
			name = attrs.getValue( aName );
		else if (XML_DESCRIPTION.equals(aName))
			description = attrs.getValue( aName );
	    }
	    
	    current_class = file.newClass( name, description );
	}
	else if ( XML_SUPERCLASS.equals(eName) )
	{
	    if ( current_class == null )
	    {
	    	return;
	    }
	    
	    if ( current_class.superclass != null )
	    {
		System.err.println( "ERROR:: The " + current_class.name + " class uses multiple inheritance,"
				    + " which is not supported by the IS." );
		System.err.println( "        This class will be ignored." );
		current_class = null;
		return;
	    }
	    //
	    // Reading superclass
	    //
	    for (int i = 0; i < attrs.getLength(); i++)
	    {
                String aName = attrs.getLocalName(i);
                if ("".equals(aName)) 
			aName = attrs.getQName(i);
		
		if (XML_NAME.equals(aName))
			current_class.superclass = attrs.getValue( aName );
	    }
	}
	else if ( XML_ATTRIBUTE.equals(eName) )
	{
	    if ( current_class == null )
	    {
	    	return;
	    }
	    //
	    // Reading attribute
	    //
	    Repository.Attribute a = current_class.newAttribute( );
	    for (int i = 0; i < attrs.getLength(); i++)
	    {
                String aName = attrs.getLocalName(i); // Attr name
                if ("".equals(aName)) 
			aName = attrs.getQName(i);
		if (XML_NAME.equals(aName))
			a.name = attrs.getValue( aName );
		else if (XML_TYPE.equals(aName))
			a.type = attrs.getValue( aName );
		else if (XML_DESCRIPTION.equals(aName))
			a.description = attrs.getValue( aName );
		else if (XML_ATTRIBUTE_MULTI_VALUE.equals(aName))
			a.multi_value = "yes".equals( attrs.getValue( aName ) ) 
					? new Boolean( true )
					: new Boolean( false );
		else if (XML_INIT_VALUE.equals(aName))
			a.init_value = attrs.getValue( aName );
		else if (XML_RANGE.equals(aName))
		{
		    if ( XML_ENUM_TYPE.equals( a.type ) )
		    {
			a.type += "{" + attrs.getValue( aName ) + "}";
		    }
		}
	    }
	    current_class.attributes.add( a );
	}
	else if ( XML_RELATIONSHIP.equals(eName) )
	{
	    if ( current_class == null )
	    {
	    	return;
	    }
	    //
	    // Reading attribute
	    //
	    Repository.Attribute a = current_class.newAttribute( );
	    for (int i = 0; i < attrs.getLength(); i++)
	    {
                String aName = attrs.getLocalName(i); // Attr name
                if ("".equals(aName)) 
			aName = attrs.getQName(i);
		if (XML_NAME.equals(aName))
			a.name = attrs.getValue( aName );
		else if (XML_CLASS_TYPE.equals(aName))
			a.type = attrs.getValue( aName );
		else if (XML_DESCRIPTION.equals(aName))
			a.description = attrs.getValue( aName );
		else if (XML_RELATIONSHIP_MULTI_VALUE.equals(aName))
			a.multi_value = "many".equals( attrs.getValue( aName ) ) 
					? new Boolean( true )
					: new Boolean( false );
		else if (XML_RELATIONSHIP_MULTI_VALUE_IMPL.equals(aName))
			a.use_vector = "vector".equals( attrs.getValue( aName ) ) 
					? new Boolean( true )
					: new Boolean( false );
	    }
	    current_class.attributes.add( a );
	}
	else
	{
	    // Skip all the other elements
	}
    }

    public void endElement( String namespaceURI, String sName, String qName ) {}

    public void error( SAXParseException e ) throws SAXParseException
    {
	 throw e;
    }
}
