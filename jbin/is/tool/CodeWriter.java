package is.tool;

import java.io.*;
import java.util.Vector;
import java.util.Date;
import javax.swing.JTextArea;

abstract class CodeWriter
{
    static final String  beginUserCode   = "// <<BeginUserCode>>";
    static final String  endUserCode     = "// <<EndUserCode>>";

    static String write( Repository repository, 
			 Object object , 
			 String language,
			 String dir,
			 CodeProducer.Options opt )
    {
	String log = new String();
	if ( object == null )
	{
	    return log;
	}
	
	Generator gen = repository.getGenerator( language );

	    	
	if ( object instanceof Repository.File )
	{
	    log += writeFile( repository, (Repository.File) object, language, dir, opt );
	}
	else if ( object instanceof Repository.Class )
	{
	    log += writeClass( repository, (Repository.Class)object, language, dir, opt );
	}
	else
	{
	    for ( int i = 0; i < repository.files.size() ; i++ )
	    {
	    	log += writeFile( repository, (Repository.File) repository.files.get( i ), 
					language, dir, opt );
	    }
	}
	return log;
    }

    private static String writeFile( Repository repository, Repository.File file, 
    					String language, String dir, CodeProducer.Options opt )
    {
	String log = new String();
	for ( int i = 0; i < file.classes.size() ; i++ )
	{
	    log += writeClass( repository, (Repository.Class) file.classes.get( i ), 
	    			language, dir, opt );
	}
	return log;
    }
    
    private static String writeClass( Repository repository, Repository.Class cl, 
    					String language, String dir, CodeProducer.Options opt )
    {
	String log = new String();
	Generator gen = repository.getGenerator( language );
	String extension = repository.getFileExtension( language );
	
	String fullname = dir;
	if ( !fullname.endsWith( "/" ) )
	{
	    fullname += "/";
	}
	String fname = cl.getClassName( opt.with_named_template );
	fname += extension;
	
	fullname += fname;
    	Vector<String> old_user_code = readUserCode( fullname );
	
	try
	{
	    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fullname)));
	    out.print( gen.produceHeader( fname ) );
	    	
	    writeCode( cl.getCode( language, opt ), old_user_code, out );
	    	
	    out.print( gen.produceFooter( fname ) );
	    out.flush();
	    out.close();
	    Date date = new Date();
	    log = date.toString() + ":: file " + fullname + " has been generated.\n";
	}
	catch( java.io.IOException e )
	{
	    Date date = new Date();
	    log = date.toString() + ":: ERROR WRITING FILE " + fullname + ". Check directory permissions.\n";
	}
	return log;
    }

    private static void writeCode( String code, Vector<String> old_user_code, PrintWriter out )
    {
	
	if ( old_user_code == null )
	{	    
	    out.print( code );
	    out.flush();	    
	    return;
	}
	
	int off = 0;
	int pos = 0;
	for ( int i = 0; i < old_user_code.size(); i++ )
	{
	    pos = code.indexOf( beginUserCode, off );
	    if ( pos == -1 )
	    	break;
	    pos += beginUserCode.length();
	    out.write( code, off, pos - off );
	    off = pos;
	    pos = code.indexOf( endUserCode, off );
	    if ( pos == -1 )
	    	break;
		
	    if (    old_user_code.get(i) != null 
	         && code.substring( off, pos ).equals( "\n\n" ) )
	    {
	    	out.write( old_user_code.get( i ), 0, old_user_code.get( i ).length() );
	    }
	    else
	    {
	    	out.write( code, off, pos - off );
	    }
	    off = pos;
	}
	out.write( code, off, code.length() - off );
    }
    
    private static Vector<String> readUserCode( String filename )
    {
	Vector<String> result = null;
	BufferedReader in;
	try
	{           
	    in = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
	    
	    String line = "";
	    String code = "\n";
	    result = new Vector<String>();
	    while ( ( line = in.readLine() ) != null )
	    {
		if ( line.indexOf( beginUserCode ) != -1 )
	    	{
	    	    do
	    	    {
	    	    	line = in.readLine();
	    	    	if( line != null && line.indexOf( endUserCode ) == -1 )
	    	    	{
	    	    	    code += line + "\n";
	    	    	}
	    	    	else
	    	    	{
	    	    	    break;
	    	    	}
	    	    }
	    	    while( true );
	    	    
	    	    if ( !code.equals( "\n" ) )
	    	    {
	    	    	result.add( code );
	    	    	code = "\n";
	    	    }
	    	}
	    }
	    in.close();
        }
        catch (FileNotFoundException e)
	{
	    return null;
        }
        catch (IOException e)
	{
	    return null;
        }
	return result;
    }
}
