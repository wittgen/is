package is.tool;

import java.io.*;
import java.util.*;

class JavaGenerator extends Generator
{        
    static private final int	MAX_LINE_LENGTH = 80;
    
    private Hashtable<String,String>	types = new Hashtable<String,String>();
    private String			superclass_name;
    private int				max_type_length = 20;
    
    private void initialize( Repository.Class cl, CodeProducer.Options opt )
    {
	types.clear();
	types.put( "bool", "boolean" );
	types.put( "s8",  "byte" );
	types.put( "s16", "short" );
	types.put( "s32", "int" );
	types.put( "s64", "long" );
	types.put( "u8",  "byte" );
	types.put( "u16", "short" );
	types.put( "u32", "int" );
	types.put( "u64", "long" );
	types.put( "float", "float" );
	types.put( "double", "double" );
	types.put( "string", "String" );
	types.put( "time", "Time" );
	types.put( "date", "Date" );
	
	superclass_name = cl.getSuperclassName( opt.with_named_template );
	if ( cl.superclass.equals( "Info" ) )
	    superclass_name = "is." + superclass_name;
    }
    
    public String produceHeader( String filename )
    {
	return "";	
    }
    
    public String produceFooter( String filename )
    {
	return "\n";	
    }
    
    public String produceCode( Repository.Class cl, CodeProducer.Options opt )
    {
	initialize( cl, opt );
	StringWriter sw = new StringWriter( );
	PrintWriter out = new PrintWriter( sw );
	
	declarePackage( out, cl, opt );
	declareImports( out, cl, opt );
	declareInfo( out, cl, opt );
	return sw.toString();
    }
    
    void declarePackage( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	if ( opt.namespaces.size() != 0 )
        {
	    out.print ( "package " );
	    for ( int i = 0; i < opt.namespaces.size() - 1; i++ )
		out.print ( opt.namespaces.get( i ) + "." );
	    out.println ( opt.namespaces.get( opt.namespaces.size() - 1 ) + ";\n");
	}
    }

    void declareImports( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
        boolean need_date = false;
        boolean need_time = false;
        
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    if ( a.type.equals( "date" ) )
	    {
		need_date = true;
	    }
	    else if ( a.type.equals( "time" ) )
	    {
		need_time = true;
	    }
            else if ( types.get( a.type ) == null )
            {
		int l = a.use_vector ? "java.lang.Vector<>".length() + a.type.length() + 3 : a.type.length() + 3;
                max_type_length = max_type_length > l ? max_type_length : l;
            }
	}
	
        if ( need_time )
            out.println( "import is.Time;" );
        if ( need_date )
	    out.println( "import is.Date;" );
    }	
    
    void declareInfo( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {	
	printUserCodeComments( out );
	
	printClassComment( out, cl.getDescription() );
			
	out.print( "public class " + cl.getClassName( opt.with_named_template ) + " extends ");
	out.println( superclass_name + " {" );
        declareType( out, cl, opt );

	declareAttributes ( out, cl );

	if ( opt.with_named_template )
	    declareNamedConstructor( out, cl, opt );
	else
	    declareConstructor( out, cl, opt );
	
	declarePublishGuts( out, cl );
	declareRefreshGuts( out, cl );
	
	printUserCodeComments( out );
	
	out.println( "}" );
	
	printUserCodeComments( out );
    }
    
    void declareConstructor( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    public " + cl.getClassName( opt.with_named_template ) + "() {" );
	out.println( "\tthis( \"" + cl.getName() + "\" );" );
	out.println( "    }" );
	out.println( );
	out.println( "    protected " + cl.getClassName( opt.with_named_template ) + "( String type ) {" );
	out.println( "\tsuper( type );" );
	defineConstructor( out, cl, false );
    }
    
    void declareNamedConstructor( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    public " + cl.getClassName( opt.with_named_template ) + "( ipc.Partition partition, String name ) {" );
	out.println( "\tthis( partition, name, \"" + cl.getName() + "\" );" );
	out.println( "    }" );
	out.println( );
	out.println( "    public " + cl.getClassName( opt.with_named_template ) + "( ipc.Partition partition, String name, String type ) {" );
	out.println( "\tsuper( partition, name, type );" );
	defineConstructor( out, cl, true );
    }
    
    void defineConstructor( PrintWriter out, Repository.Class cl, boolean is_named )
    {
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    
            if ( a.init_value.length() == 0 && !a.type.equals( "string" ) && !a.multi_value )
	    {
		continue;
	    }
	    
	    String attribute_type = types.get( a.type );
            if ( attribute_type == null )
            	attribute_type = a.use_vector ? "java.util.Vector<" + a.type + ">" : a.type;
		
	    String attribute_name = a.getName();
	    if (    ( attribute_name.equals( "name" ) && is_named )
		 || ( attribute_name.equals( "partition" ) && is_named )
		 ||   attribute_name.equals( "type" ) )
	    {
		attribute_name = "this." + attribute_name;
	    }
	    String init_start = "";
	    String init_end = "";
	    String init_value;
	    if (   a.type.equals( "date" ) 
		|| a.type.equals( "time" ) )
	    {
		init_start = "new " + attribute_type + "( \"";
		init_end = "\" )";		
	    }
	    else if ( a.type.equals( "string" ) )
	    {
		init_start = "\"";
		init_end = "\"";		
	    }
	    else if ( a.type.equals( "float" ) )
	    {
		init_start = "";
		init_end = "f";		
	    }
	    if ( a.multi_value )
	    {	    
		StringTokenizer st = new StringTokenizer( a.init_value, "," );
		int size = st.countTokens();
		String size_argument = a.use_vector	? "(" + size + ");"
							: "[" + size + "];";
                out.println( "\t" + attribute_name + " = new " + attribute_type + size_argument );
		for( int j = 0; j < size; j++ )
		{
		    init_value = st.nextToken().trim();
		    if ( a.type.equals( "bool" ) )
		    {
			init_value = parseBoolean( init_value );
		    }
		    else if ( a.type.startsWith( "enum" ) )
		    {
			init_value = attribute_type + "." + init_value;
                    }
		    if ( a.use_vector )
			out.println( "\t" + attribute_name +  ".add(" + init_start + init_value + init_end + ");" );
		    else
                        out.println( "\t" + attribute_name + "[" + j + "] = " + init_start + init_value + init_end + ";" );
		}
	    }
	    else
	    {
		init_value = a.init_value;
		if ( a.type.equals( "bool" ) )
		{
		    init_value = parseBoolean( init_value );
		}
		else if ( a.type.startsWith( "enum" ) )
		{
		    init_value = attribute_type + "." + init_value;
		}
		out.println( "\t" + attribute_name + " = " + init_start + init_value + init_end + ";" );
	    }
	}
	
	printUserCodeComments( out );
	
	out.println( "    }" );
	out.println( );
    }
    
    void declarePublishGuts( PrintWriter out, Repository.Class cl )
    {
	out.println( "    public void publishGuts( is.Ostream out ) {" );
	
	out.println( "\tsuper.publishGuts( out );" );
	
	String output = "\tout";
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    String TypeArgument = types.get( a.type ) != null ? "" : ", " + a.type + ".type ";
	    if (    a.type.equals( "s8" ) 
		 || a.type.equals( "s16" ) 
		 || a.type.equals( "s32" )
		 || a.type.equals( "s64" ) )
	    {
		output += ".put( " + a.getName() + ", true )";
	    }
	    else if (	   a.type.equals( "u8" ) 
		   	|| a.type.equals( "u16" )
		   	|| a.type.equals( "u32" )
			|| a.type.equals( "u64" ) )
	    {
		output += ".put( " + a.getName() + ", false )";
	    }
	    else
	    {
		output += ".put( " + a.getName() + TypeArgument + " )";
	    }
	    
	    if ( output.length() > MAX_LINE_LENGTH )
	    {
		out.println( output + ";" );
		output = "\tout";
	    }
	}
	if ( !output.equals( "\tout" )  )
	{
	    out.println( output + ";" );
	}
	
	out.println( "    }" );
	out.println( );
    }
    
    void declareRefreshGuts( PrintWriter out, Repository.Class cl )
    {
	out.println( "    public void refreshGuts( is.Istream in ) {" );
	
	out.println( "\tsuper.refreshGuts( in );" );

	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    String TypePrefix = a.type.startsWith( "enum" ) ? "Enum" : types.get( a.type );
	    String ClassArgument = "";
            if ( TypePrefix == null )
            {
		TypePrefix = "Info";
                ClassArgument = a.type + ".class";
	    }
            else if ( a.type.startsWith( "enum" ) )
            {
                ClassArgument = types.get( a.type ) + ".class";
	    }
            else
            {
		TypePrefix = StringUtil.Capitalise( TypePrefix );
            }
               
	    if ( a.multi_value )
	    {
		out.println( "\t" + a.getName() + " = in.get" + TypePrefix + (a.use_vector ? "Vector" : "Array") + "( " + ClassArgument + " );" );
	    }
	    else
	    {
		out.println( "\t" + a.getName() + " = in.get" + TypePrefix + "( " + ClassArgument + " );" );
	    }
	}
	
	out.println( "    }" );
	out.println( );
    }
    
    void declareAttributes( PrintWriter out, Repository.Class cl )
    {
	declareEnumerations( out, cl );
	
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);

	    printAttributeComment( out, a.getDescription() );
	    
	    String brackets = new String ( a.multi_value && !a.use_vector ? "[]" : "" );

	    String attribute_type = types.get( a.type );
            if ( attribute_type == null )
            	attribute_type = a.use_vector ? "java.util.Vector<" + a.type + ">" : a.type;

	    String s1 = attribute_type + brackets;
	    s1 += addSpaces( max_type_length - s1.length() );
	    String s2 = a.getName() + ";";
	    out.println( "    public " + s1 + s2 );
	    out.println( );
	}
	out.println( );
    }

    void declareEnumerations( PrintWriter out, Repository.Class cl )
    {
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    String type = types.get( a.type );
	    if ( type == null && a.type.indexOf( "enum" ) >= 0 )
	    {
		types.put( a.type, StringUtil.InvertCapitalisation( a.getName() ) );
		StringTokenizer st = new StringTokenizer( a.type , ",{}" );
		
		String tok = st.nextToken();
		if ( tok.equals( "enum" ) )
		{
		    out.print( "    public enum " + types.get( a.type ) + " { " );
		    
		    while( st.hasMoreTokens() )
		    {
			out.print( st.nextToken().trim() + ( st.hasMoreTokens() ? ", " : "" ) );
		    }
		    out.println( " }" );
		}
		max_type_length = max_type_length > ( a.type.length() + 3 )
			    ? max_type_length
			    : ( a.type.length() + 3 );
	    }
	}
	out.println( );
    }    
    
    void declareType( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	String params = ( opt.with_named_template ) ? " new ipc.Partition(), \".\" " : " ";
	out.println( "    public static final is.Type type = new is.Type( new "
        			+ cl.getClassName( opt.with_named_template ) 
                        	+ "(" + params + ") );" );
	out.println( );
    }
    
    String parseBoolean( String val )
    {
    	if ( val.equals( "0" ) )
	    return "false";
    	if ( val.equals( "1" ) )
	    return "true";
	return val;
    }
    
    String addSpaces( int size )
    {
    	char spaces[] = new char[size];
	for ( int i = 0; i < size; i++ )
	    spaces[i] = ' ';
	return new String ( spaces );
    }
}
