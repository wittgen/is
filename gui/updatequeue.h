#ifndef IS_UPDATE_QUEUE_H
#define IS_UPDATE_QUEUE_H

#include <functional>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

#include <Xm/Xm.h>

#include <is/type.h>
#include <owl/time.h>

class InfoDialog;

class UpdateQueue
{  
  public:
    UpdateQueue( InfoDialog & dialog, Widget table );
    ~UpdateQueue( );

    void add( const std::string & name );
    void add( const std::string & name, const OWLTime & time );
    void add( const std::string & name, const OWLTime & time, const ISType & type );
    
  private:
    void thread_wrapper();
    void update( const std::pair<std::string,OWLTime> & udata );

  private:
    typedef std::function<void (void)>	Task;
    typedef std::queue<Task> 		TaskQueue;
    
    InfoDialog &		m_dialog;
    Widget			m_table;
    bool			m_terminated;
    std::mutex			m_mutex;
    std::condition_variable	m_condition;
    TaskQueue			m_updates;
    std::thread	                m_thread;
};

#endif
