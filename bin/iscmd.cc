////////////////////////////////////////////////////////////////////////
//        iscmd.cc
//
//        Utility application for the IS component
//
//        Sergei Kolos,    September 2005
//
//        description:
//               Send command utility
//	    
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <sstream>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <is/infoiterator.h>
#include <is/infodictionary.h>
#include <is/infoany.h>  

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }
    
    // Declare arguments
    CmdArgBool		verbose ( 'v', "verbose", "turn on verbose mode." );
    CmdArgStr		partition_name( 'p', "partition", "partition-name", "partition to work in." );
    CmdArgStr		command( 'c', "command", "command", "command to send.", CmdArg::isREQ );
    CmdArgStr		server_name( 'n', "server", "server-name", "server to work with.",CmdArg::isREQ );
    CmdArgStr   	regexp ('R', "rexpression", "regular-expression", "regular expression for information names to send command to." );
    CmdArgStrList	names('i', "info", "info-names", "names of information to send command to.", CmdArg::isLIST);

    // Declare command object and its argument-iterator
    CmdLine    cmd(*argv, &verbose, &partition_name, &server_name, &command, &names, &regexp, NULL);
    
    CmdArgvIter    arg_iter(--argc, ++argv);
 
    cmd.description(	"This program is part of the IS utility suit.\n"
			"It send commands to information object providers.");
        
    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition p(partition_name);
    int error_count = 0;
    
    for ( size_t i = 0; i < names.count(); i++ )
    {
	std::string infoname( server_name );
        infoname += ".";
        infoname += names[i];

        ISInfoAny any;

	if ( verbose )
	{
	    std::cerr << "Sending command \"" << command << "\" to the \"" << infoname << "\" info ... ";
        }

    	try
        {
	    ISInfoDictionary isd( p );
	    isd.getValue( infoname, any );
	    any.sendCommand( (const char*)command );
	    if ( verbose )
	    {
		std::cout << "success" << std::endl;
	    }
        }
        catch( daq::is::Exception & ex )
        {
	    error_count++;
            if ( verbose )
	    {
		std::cout << "error" << std::endl;
		ers::error( ex );
	    }
        }
    }

    if ( regexp.flags() && CmdArg::GIVEN )
    {
	try
        {
	    ISInfoIterator it( p, (const char*)server_name, ISCriteria(std::string(regexp)) );
	    while ( it() )
	    {
		if ( verbose )
		{
		    std::cerr << "Sending command \"" << command << "\" to the \"" << it.name() << "\" info ... ";
		}

		try
		{
		    it.sendCommand( (const char*)command );
		    if ( verbose )
		    {
			std::cout << "success" << std::endl;
		    }
		}
		catch( daq::is::Exception & ex )
		{
		    error_count++;
		    if ( verbose )
		    {
			std::cout << "error" << std::endl;
			ers::error( ex );
		    }
		}
	    }
        }
        catch( ers::Issue & ex )
        {
            ers::fatal( ex );
	    error_count++;
        }
    }

    return error_count;
}

