////////////////////////////////////////////////////////////////////////
//      isls.cc
//
//      IS binary
//
//      Sergei Kolos,  January 2000
//
//      description:
//           Lists Information Service servers in the specific partition
//		as well as the contents of the servers   
//		
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <memory>
#include <cmdl/cmdargs.h>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <is/infoany.h>
#include <is/infodictionary.h>
#include <is/infostream.h>
#include <is/serveriterator.h>

extern void print_info(const IPCPartition & p, ISInfoAny & isa, const ISType & type,
    std::ostream & out,
    bool print_name, bool print_type, bool print_descr, const char * offset = "      ");

void printStream(ISInfoStream& ii, const char * server_name,
		    bool print_name, bool print_type, bool print_descr,
		    bool print_value, bool print_tag, bool print_provider,
		    bool print_annotations)
{
    std::cout << "  Server \"" << server_name << "\" contains " << ii.entries()
	<< " object(s):" << std::endl;
    while (!ii.eof())
    {
	std::cout << "    " << ii.name();
	if (print_tag)
	{
	    std::cout << " <" << ii.tag() << ">";
	}
	std::cout << " <" << ii.time() << "> " << "<" << ii.type() << ">";
	if (print_value || print_provider || print_annotations)
	{
	    ISInfoAny isa;
	    ii >> isa;
	    if (print_provider)
	    {
		std::cout << " <" << isa.provider() << ">";
	    }
            if (print_annotations)
            {
                typedef std::pair<std::string, ISAnnotation> P;

                std::cout << " <";
                const ISInfo::Annotations & a = isa.annotations();
                std::for_each(a.begin(), a.end(), [](const P & p) {
                    std::cout << "[" << p.first << "=" << p.second <<"]";});
                std::cout << ">";
            }
            std::cout << std::endl;

	    if (print_value)
	    {
	        print_info(ii.partition(), isa, isa.type(), std::cout, print_name,
	                print_type, print_descr);
	    }
	}
	else
	{
	    ii.skip();
	    std::cout << std::endl;
	}
    }
}

int main(int argc, char ** argv)
{
    try
    {
	IPCCore::init(argc, argv);
    }
    catch (daq::ipc::Exception & ex)
    {
	ers::fatal(ex);
	return 1;
    }

    CmdArgStr partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStr server_name('n', "server", "server-name", "server to work with.");
    CmdArgStr regexp('R', "regex", "regular-expression",
	"regular expression for information to be printed.");
    CmdArgBool print_value('v', "print-value", "print information values.");
    CmdArgBool print_tag('t', "print-tag", "print object tag.");
    CmdArgBool print_descr('D', "print-description",
	"print description of information attributes (if available).");
    CmdArgBool print_name('N', "print-name",
	"print names of information attributes (if available).");
    CmdArgBool print_type('T', "print-type", "print types of information attributes.");
    CmdArgBool print_annotations('A', "print-annotations", "print information annotations.");
    CmdArgBool print_provider('P', "print-provider", "print information provider id.");
    CmdArgBool print_history('H', "print-history", "print information history.");

    // Declare command object and its argument-iterator
    CmdLine cmd(*argv, &partition_name, &server_name, &regexp, &print_provider,
	&print_value, &print_name, &print_type, &print_descr, &print_history, &print_tag,
	&print_annotations, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);

    cmd.description("Lists Information Service servers in the specific partition\n"
	"as well as the contents of the servers\n");

    regexp = ".*";

    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition partition(partition_name);

    int history_depth = print_history ? -1 : (print_value || print_provider || print_annotations) ? 1 : 0;
    try
    {
	if (!server_name.isNULL())
	{
	    ISInfoStream in(partition, std::string(server_name),
			    ISCriteria(std::string(regexp)), true, history_depth);
	    printStream(in, server_name, print_name, print_type, print_descr,
			    print_value, print_tag || print_history, print_provider, print_annotations);
	    return 0;
	}

	ISServerIterator ss(partition);
	std::cout << "Partition \"" << partition.name() << "\" contains " << ss.entries()
	    << " IS server(s):" << std::endl;
	while (ss())
	{
	    ISInfoStream in(partition, ss.name(), ISCriteria(std::string(regexp)),
			    true, history_depth);
	    printStream(in, ss.name(), print_name, print_type, print_descr,
			    print_value, print_tag || print_history, print_provider, print_annotations);
	}
    }
    catch (ers::Issue & ex)
    {
	ers::fatal(ex);
	return 2;
    }

    return 0;
}
