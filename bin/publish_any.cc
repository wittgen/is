////////////////////////////////////////////////////////////////////////
//    publish_info.cc
//
//    Test application for the IS library
//
//    Sergei Kolos, March 2011
//
//    description:
//          Publish arbitrary infomation to IS
//      
////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <is/infodynany.h>
#include <is/infodictionary.h>

template <class T>
struct value_helper
{
    static void 
    set( T & v, const std::string & value )
    {
	std::istringstream in( value );
	in >> v;
    }
};

template <>
struct value_helper<OWLTime>
{
    static void
    set( OWLTime & v, const std::string & value )
    {
	v = OWLTime( value.c_str() );
    }
};

template <>
struct value_helper<std::string>
{
    static void
    set( std::string & v, const std::string & value )
    {
	v = value;
    }
};

template <>
struct value_helper<OWLDate>
{
    static void
    set( OWLDate & v, const std::string & value )
    {
	v = OWLDate( value.c_str() );
    }
};

template <>
struct value_helper<ISInfoDynAny>
{
    static void
    set( ISInfoDynAny & , const std::string & )
    {
	std::cerr << "ERROR: nested objects are not supported" << std::endl;
    }
};

#define SET_SIMPLE_ATTRIBUTE(x,y)		case ISType::y : value_helper<x>::set( object.getAttributeValue<x>( indices[i] ), values[i] ); break;					   
#define SET_SIMPLE_ATTRIBUTE_SEPARATOR
#define SET_SIMPLE_ATTRIBUTE_OBJECT_TYPE	ISInfoDynAny

template <class I>
void 
setValues( ISInfoDynAny & object, const std::vector<I> & indices, const std::vector<std::string> & values )
{
    for ( size_t i = 0; i < indices.size(); i++ )
    {
	if ( object.isAttributeArray( indices[i] )  )
	{
	    std::cerr << "WARNING: multi-value attributes are not supported ('" 
            	<< indices[i] << "' attribute is an array)" << std::endl;
            continue;
	}
	switch ( object.getAttributeType( indices[i] ) )
	{
	    IS_TYPES( SET_SIMPLE_ATTRIBUTE )
	    default : ERS_ASSERT( false );
	}
    }
}

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    // Declare arguments
    CmdArgStr		partition_name	 ('p', "partition", 
    						"partition-name", "partition to work in.");
    CmdArgStr		object_name	 ('n', "object", 
    						"name", "name of information object.", CmdArg::isREQ);
    CmdArgStr		type_name	 ('t', "type", 
    						"type-name", "object type.");
    CmdArgInt		tag	 	 ('T', "tag",
    						"tag-value", "update information value using this tag.");
    CmdArgBool		keep_history	 ('K', "keep_history",
    						"update information value using new tag.");
    CmdArgBool		read_first	 ('r', "read-first", 
    						"read object from IS before publishing.");
    CmdArgStrList	attribute_names	 ('a', "attributes", "attributes-names", 
    						"names of attributes to be modified.", CmdArg::isLIST);
    CmdArgIntList	attribute_indices('i', "positions", "attributes-indices", 
    						"indices of attributes to be modified.", CmdArg::isLIST);
    CmdArgStrList	attribute_values ('v', "values", "attributes-values", 
    						"values of attributes to be set.", CmdArg::isLIST);
 
    // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &partition_name, &object_name, &type_name, &read_first,
    			&attribute_names, &attribute_indices, &attribute_values, &tag, &keep_history, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);
 
    cmd.description("This program publishes arbitrary information to the Information Service.");
  
    keep_history = false;

    // Parse arguments
    cmd.parse(arg_iter);
    
    IPCPartition	partition(partition_name);
    ISInfoDictionary	dictionary( partition );
    
    std::string name(object_name);
    
    std::unique_ptr<ISInfoDynAny> object( new ISInfoDynAny() );
    
    bool type_is_known = false;
    if ( type_name.flags() && CmdArg::GIVEN )
    {
	try {
	    object.reset( new ISInfoDynAny( partition, (const char *)type_name ) );
            type_is_known = true;
	}
	catch( daq::is::Exception & ex ) {
	    ers::error( ex );
	}
    }
    
    if ( ( read_first.flags() && CmdArg::GIVEN ) 
         || !type_is_known )
    {
	try {
	    dictionary.getValue( name, *object );
	}
	catch( daq::is::Exception & ex ) {
	    if ( type_is_known )
            {
            	ers::error( ex );
            }
            else
            {
            	ers::fatal( ex );
                return 1;
            }
	}
    }
    
    std::vector<std::string> values;
    for( unsigned int i = 0; i < attribute_values.count(); ++i ) {
	values.push_back( (const char*)attribute_values[i] );
    }
    
    if ( attribute_names.count() == attribute_values.count() )
    {
	std::vector<std::string> names;
	for( unsigned int i = 0; i < attribute_names.count(); ++i )
	    names.push_back( (const char*)attribute_names[i] );
    	setValues( *object, names, values );
    }
    else if ( attribute_indices.count() == attribute_values.count() )
    {
	std::vector<int> indices;
	for( unsigned int i = 0; i < attribute_indices.count(); ++i )
	    indices.push_back( attribute_indices[i] );
    	setValues( *object, indices, values );
    }
    else
    {
    	std::cerr << "ERROR: Number of the given attributes values"
    	    " does not match neither the number of the given indices nor the number of the given names" << std::endl;
        return 1;
    }

    try {
	if (tag.flags() && CmdArg::GIVEN ) {
	    dictionary.checkin( name, tag, *object );
	} else {
	    dictionary.checkin( name, *object, keep_history );
	}
    }
    catch( daq::is::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

    return 0;
}
