////////////////////////////////////////////////////////////////////////
//        test_annotations.cc
//
//        Test application for the IS library
//
//        Sergei Kolos,    January 2000
//
//        description:
//                Test the functionality of the ISInfoDictionary class
//	    
////////////////////////////////////////////////////////////////////////

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/infoany.h>
#include <is/infodynany.h>
#include <is/infostream.h>
#include <is/test/LargeInfo.h>

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    // Declare arguments
    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStr   server_name('n', "server", "server-name", "server to work with.",CmdArg::isREQ);
 
    // Declare command object and its argument-iterator
    CmdLine    cmd(*argv, &partition_name, &server_name, NULL);
    CmdArgvIter    arg_iter(--argc, ++argv);
 
    cmd.description("This program is part of the IS functionality test suit.");

    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition	partition(partition_name);
    ISInfoDictionary	dictionary(partition);
    std::string         serverName = std::string((const char*)server_name);
    

    is::test::LargeInfo	info;
    std::string name = serverName + ".AnnotationTest";
    try {
        dictionary.checkin(name, info);

        is::test::LargeInfo info1;
        dictionary.getValue(name, info1);
        ERS_ASSERT(info1.annotations().size() == 0);

        info.setAnnotation("ann1", 123);
        info.setAnnotation("ann2", 123.456);
        dictionary.checkin(name, info);

        std::function<void(ISInfo & info)> test = [](ISInfo & info) {
            ERS_ASSERT(info.annotations().size() == 2);
            ERS_ASSERT(info.getAnnotation("ann1").getValue<int>() == 123);
            ERS_ASSERT(info.getAnnotation("ann2").getValue<double>() == 123.456);
        };

        is::test::LargeInfo info2;
        dictionary.getValue(name, info2);
        test(info2);
        dictionary.getValue(name, info2);
        test(info2);

        ISInfoAny ia;
        dictionary.getValue(name, ia);
        test(ia);
        dictionary.getValue(name, ia);
        test(ia);

        ISInfoDynAny ida;
        dictionary.getValue(name, ida);
        test(ida);
        dictionary.getValue(name, ida);
        test(ida);

        ISInfoStream stream(partition, serverName, ISCriteria("AnnotationTest"));
        is::test::LargeInfo info3;
        while (!stream.eof()) {
            stream >> info3;
            test(info3);
        }

        ISInfoStream stream1(partition, serverName, ISCriteria("AnnotationTest"));
        ISInfoAny ia1;
        while (!stream1.eof()) {
            stream1 >> ia1;
            test(ia1);
        }
    }
    catch (ers::Issue & ex) {
        ers::fatal(ex);
        return 1;
    }
    
    return 0;
}
