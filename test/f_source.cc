////////////////////////////////////////////////////////////////////////
//        f_source.cc
//
//        Test application for the IS library
//
//        Sergei Kolos,    January 2000
//
//        description:
//                Test the functionality of the ISInfoDictionary class
//	    
////////////////////////////////////////////////////////////////////////

#include <unistd.h>

#include <iostream>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <is/test/arraytest.h>
#include <is/test/LargeInfo.h>
#include <is/test/MediumInfo.h>
#include <is/infodocument.h>
#include <is/infodictionary.h>
#include <is/infoprovider.h>
#include <is/infoT.h>
#include <is/infodynany.h>

using is::test::LargeInfo;
using is::test::MediumInfo;

int  test( ISInfoDictionary& id, ISInfo& ti, const std::string & test_name, int verbose );
int  testHistory( ISInfoDictionary& id );
int  testHistoryWithTags( ISInfoDictionary& id );
void testDocument( IPCPartition & p, ISInfo& ti );
void testDocumentIterator( IPCPartition & p );
void printDocument( const ISInfoDocument & isd );

static CmdArgStrList	info_names('N', "names", "name", "names of information to create.", CmdArg::isREQ | CmdArg::isLIST);
static CmdArgStr	server_name('n', "server", "server-name", "server to work with.",CmdArg::isREQ);
static CmdArgInt	delay ('d', "delay", "seconds","delay between different operations");

class MyCommandListener : public ISCommandListener
{
  public:
    void command( const std::string & name, const std::string & cmd )
    {
    	std::cout << "MyCommandListener:: command \"" << cmd << "\" received for the \"" 
		  << name << "\" info" << std::endl;
    }
};

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    // Declare arguments
    CmdArgBool	verbose ('v', "verbose", "turn on verbose mode.");
    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgInt	wait('w', "wait", "wait-time", "time to wait before exiting.");
 
    // Declare command object and its argument-iterator
    CmdLine    cmd(*argv, &verbose, &delay, &partition_name, &server_name, &info_names, &wait, NULL);
    CmdArgvIter    arg_iter(--argc, ++argv);
 
    cmd.description(	"This program is part of the IS functionality test suit.\n"
			"It creates, updates and deletes information objects." );
    
    wait = 10;
    
    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition	p(partition_name);
    ISInfoDictionary	id(p);
    
    int result = 0;
    
    LargeInfo lt;    
    result += test(id, lt, "Generated information class (large)", verbose);

    MediumInfo mt;    
    result += test(id, mt, "Generated information class (medium)", verbose);

    ISInfoChar	isc;
    isc = 'a';
    result += test(id, isc, "character", verbose);

    ISInfoShort	issh;
    issh = 32755;
    result += test(id, issh, "short", verbose);

    ISInfoInt	isi;
    isi = 32000;
    result += test(id, isi, "integer", verbose);

    ISInfoLong	isl;
    isl = -9997888L;
    result += test(id, isl, "long", verbose);

    ISInfoUnsignedChar	isuc;
    isuc = 233;
    result += test(id, isuc, "unsigned character", verbose);

    ISInfoUnsignedInt	isui;
    isui = 65535;
    result += test(id, isui, "unsigned integer", verbose);

    ISInfoUnsignedShort	isush;
    isush = 65535;
    result += test(id, isush, "unsigned short", verbose);

    ISInfoUnsignedLong	isul;
    isul = 9997888L;    
    result += test(id, isul, "unsigned long", verbose);

    ISInfoFloat	    isf;
    isf = 3.14731812l;
    result += test(id, isf, "float", verbose);

    ISInfoDouble	    isd;
    isd = -3.14731812l;
    result += test(id, isd, "double", verbose);
    
    ISInfoString	    iss;
    iss = "Information Test String";
    result += test(id, iss, "std::string", verbose);

    testHistory( id );
    testHistoryWithTags( id );
    testDocument( p, lt );
    testDocument( p, mt );
    testDocumentIterator( p );
    
    MyCommandListener lst1;
    MyCommandListener lst2;
    
    ISInfoProvider::instance().addCommandListener( &lst1 );
    ISInfoProvider::instance().addCommandListener( &lst2 );
    
    std::cout << "Waiting " << wait << " seconds for commands ... " << std::flush;
    sleep( wait );
    std::cout << "done" << std::endl;
    
    ISInfoProvider::instance().removeCommandListener( &lst1 );
    ISInfoProvider::instance().removeCommandListener( &lst2 );
    return result;
}

int test(ISInfoDictionary& id, ISInfo& ti, const std::string & test_name, int verbose )
{    
    std::cout << "------------------------------------" << test_name << "------------------------------------" << std::endl;
    int err_count = 0;
    
    for ( unsigned int i = 0; i < info_names.count(); i++ )
    {
    	std::cerr << "Information Name is " << info_names[i] << std::endl;
    	std::string	name(server_name);
    	name += "." + test_name + ".";
    	name += info_names[i];
    	
	try
        {
            if ( id.contains(name) )
            {
		ISType ist;
		id.findType( name, ist );
		std::cout << "Information has type: " << ist << std::endl;
	    }
            else
            {
		id.insert( name, ti );
		std::cout << "Insert test passed " << ti.time() << std::endl;
	    }
            std::cout << "contains(" << name << ") return " <<  id.contains(name) << std::endl;
	}
        catch( daq::is::Exception & ex )
        {
            err_count++;
            ers::error( ex );
        }
        
    	sleep((unsigned)delay);
    	
	try
        {
	    id.update( name, ti );
	    std::cout << "Update test passed " << ti.time() << std::endl;

	    sleep((unsigned)delay);
            
	    id.findValue( name, ti );
	    std::cout << "findValue test passed " << ti.time() << std::endl;

	    ISInfoAny	    isa;

	    id.findValue( name, isa );
     	    std::cout << "findValue test passed " << std::endl;
     	    if ( verbose )
	    {
     	    	std::cout << isa;
     	    }
            
            isa >>= ti;
     	    std::cout << "ISInfoAny::operator>>= test passed " << ti.time() << std::endl;

            isa <<= ti;
            if ( verbose )
            {
                std::cout << isa;
            }

            ISInfoAny isa1;
            isa1 <<= ti;
            if ( verbose )
            {
                std::cout << isa1;
            }
            std::cout << "ISInfoAny::operator<<= test passed " << ti.time() << std::endl;
            
	    sleep((unsigned)delay);
            
	    ISInfoDynAny dany;
	    id.getValue( name, dany );
	    std::cout << "Information has type: " << dany.type() << std::endl;

	    id.update( name, dany );
	    std::cout << "Update test passed " << dany.time() << std::endl;
            
            sleep((unsigned)delay);
            
	    id.remove( name );
	    std::cout << "Remove test passed " << std::endl;
 
	}
        catch( daq::is::Exception & ex )
        {
            err_count++;
            ers::error( ex );
        }
	    	    	
    }
    return err_count;
}

int testHistory( ISInfoDictionary& id )
{
    int		 err_count = 0;
    LargeInfo	 info;    
    unsigned int UpdateNumber = 10;
    
    for ( unsigned int i = 0; i < info_names.count(); i++ )
    {
	std::string	name(server_name);
	name += ".";
	name += info_names[i];
    
	try
	{
	    id.insert( name, info );
	    std::cout << "Information " << name << " was successfully inserted." << std::endl;
	}
	catch( daq::is::Exception & ex )
	{
            err_count++;
	    ers::error( ex );
	}
    
	for ( unsigned int i = 0; i < UpdateNumber; i++ )
	{
	    try
            {
		info.int_ = i;
		id.update( name, info, true );
		std::cout << "Information " << name << " was successfully updated (keeping it's history)." << std::endl;

		info.int_ = 0;
		id.getValue( name, info );
		std::cout << "getValue test passed " << std::endl;
		std::cout << "attribute int_ = " << info.int_ << std::endl;
            }
            catch( daq::is::Exception & ex )
            {
		err_count++;
            	ers::error( ex );
            }
	}
	
        try
	{
	    std::vector<LargeInfo> values;
	    id.getValues( name, values );
	    std::cout << "Information " << name << " was successfully read (with it's history)." << std::endl;

	    if ( values.size() != UpdateNumber + 1 )
	    {
		std::cerr << "ERROR::getAllValues gets wrong history size " << values.size() << std::endl;
	    }
	    for ( unsigned int i = 0; i < values.size(); i++ )
	    {
		std::cout << "attribute int_ = " << values[i].int_ << std::endl;
	    }
	}
	catch( daq::is::Exception & ex )
	{
	    err_count++;
	    ers::error( ex );
	}
    }
    
    return err_count;
}

int testHistoryWithTags( ISInfoDictionary& id )
{
    int		 err_count = 0;
    LargeInfo	 info;    
    unsigned int UpdateNumber = 10;
    
    for ( unsigned int i = 0; i < info_names.count(); i++ )
    {
	std::string	name(server_name);
	name += ".";
	name += info_names[i];
        name += ".tagged";
    
	try
	{
	    id.insert( name, info );
	    std::cout << "Information " << name << " was successfully inserted." << std::endl;
	}
	catch( daq::is::Exception & ex )
	{
            err_count++;
	    ers::error( ex );
	}
    
	for ( unsigned int i = 0; i < UpdateNumber; i++ )
	{
	    try
            {
		info.int_ = i;
		id.update( name, i, info );
		std::cout << "Information " << name << " was successfully updated (keeping it's history)." << std::endl;

		info.int_ = 0;
		id.getValue( name, info );
		std::cout << "getValue for the last value test passed " << std::endl;
		std::cout << "tag = " << info.tag() << " attribute int_ = " << info.int_ << std::endl;
		
                id.getValue( name, i, info );
		std::cout << "getValue for the tagged value test passed " << std::endl;
		std::cout << "tag = " << info.tag() << " attribute int_ = " << info.int_ << std::endl;
                
                if ( i != 0 )
                {
		    id.getValue( name, i-1, info );
		    std::cout << "getValue for the previous value test passed " << std::endl;
		    std::cout << "tag = " << info.tag() << " attribute int_ = " << info.int_ << std::endl;
                }
            }
            catch( daq::is::Exception & ex )
            {
		err_count++;
            	ers::error( ex );
            }
	}
	
        try
	{
	    std::vector<LargeInfo> values;
	    id.getValues( name, values );
	    std::cout << "Information " << name << " was successfully read (with it's history)." << std::endl;

	    if ( values.size() != UpdateNumber )
	    {
		std::cerr << "ERROR::getAllValues gets wrong history size " << values.size() << std::endl;
	    }
	    for ( unsigned int i = 0; i < values.size(); i++ )
	    {
		std::cout << "tag = " << values[i].tag() << " attribute int_ = " << values[i].int_ << std::endl;
	    }
	}
	catch( daq::is::Exception & ex )
	{
	    err_count++;
	    ers::error( ex );
	}
    }
    
    return err_count;
}

void testDocument( IPCPartition & p, ISInfo & info )
{
    std::cout << "Meta-information is available for the following classes:" << std::endl;
    
    ISInfoDocument::Iterator it( p );
    
    while ( it() )
    {
        printDocument( *it++ );
    }    
    std::cout << std::endl;
    
    std::cout << "Information for the " << info.type().name() << " type is:" << std::endl;
    try
    {
	ISInfoDocument isd( p, info );
	printDocument( isd );
    }
    catch( daq::is::Exception & ex )
    {
    	ers::error( ex );
    }
}

void printDocument( const ISInfoDocument & isd )
{    
    std::cout << "class " << isd.name() << " {    // " << isd.description() << std::endl;
    for ( unsigned int i = 0; i < isd.attributeCount(); i++ )
    {
        const ISInfoDocument::Attribute & a = isd.attribute( i );
        std::cout << "\t" << a.typeName() << ( a.isArray() ? "[] " : " " )
		  << a.name() << ";    // " << a.description() << std::endl;
    }
    std::cout << "};" << std::endl;
}

void testDocumentIterator( IPCPartition & p )
{
    {
	std::cout << "Testing operator++:" << std::endl;
	
	ISInfoDocument::Iterator it ( p );
	
	while ( it )
	{
	    ISInfoDocument::Iterator i = ++it;
	    if ( i )
	    	std::cout << (*i).name() << std::endl;
	    else
	    	std::cout << "[end]" << std::endl;
	}

	std::cout << "Testing operator--:" << std::endl;
	
	do
	{
	    ISInfoDocument::Iterator i = --it;
	    if ( i )
	    	std::cout << (*i).name() << std::endl;
	    else
	    	std::cout << "[end]" << std::endl;
	}
	while( it );
    }
    {
	std::cout << "Testing operator++( int ):" << std::endl;
	
	ISInfoDocument::Iterator it ( p );
	
	while ( it )
	{
	    ISInfoDocument::Iterator i = it++;
	    if ( i )
	    	std::cout << (*i).name() << std::endl;
	    else
	    	std::cout << "[end]" << std::endl;
	}

	std::cout << "Testing operator--( int ):" << std::endl;
	
	do
	{
	    ISInfoDocument::Iterator i = it--;
	    if ( i )
	    	std::cout << (*i).name() << std::endl;
	    else
	    	std::cout << "[end]" << std::endl;
	}
	while( it );
    }
}

