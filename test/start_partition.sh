#!/bin/sh
######################################################################
#
#	Script to start the partition server
#	Created by Sergei Kolos; 10.08.01
#
######################################################################

start_partition()
{
	ref_file="$1.$4".ref
	err_file="$1.$4".err

	rm -f $ref_file
	rm -f $err_file
	
	echo
	$@ > $ref_file 2> $err_file &

	if test -n "$6"
	then
		echo "Waiting for \"$6\" server to start up... "
	elif test -n "$4"
	then
		echo "Waiting for \"$4\" server to start up... "
	else
		echo "Waiting for Global $1 server to start up... "
	fi
	
	count=0
	while test ! -s $ref_file -a ! -s $err_file -a $count -lt 30
	do
		sleep 1
		count=`expr $count + 1`
	done
		
	if test -s $err_file
	then
		cat $err_file
		AlreadyExist=`grep "Already exist" $err_file`
		JustWarning=`grep "WARNING" $err_file`
		if test -n "$AlreadyExist" -o -n "$JustWarning"
		then
			echo "OK: Server running"
		else
    			echo "Failed!"
			echo "(Server was not started)"
			exit 1
		fi
	else
		if test ! -s $ref_file
		then
    			echo "Failed!"
			echo "(Server was not started)"
			exit 1
		else
			echo "OK!"
		fi
	fi
}
trap deactivate 1 2 3 4 5 6 7 8 10 12 13 14 15

partition="ipc_server -i- "

if test "$1" != ""
then
        pname="-p ${1}"
fi

echo command is \"$partition $pname\"

start_partition $partition $pname

echo "##########################################################################"
echo "partition server has been started at `date`"
echo "##########################################################################"

