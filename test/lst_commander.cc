////////////////////////////////////////////////////////////////////////
//        f_source.cc
//
//        Test application for the IS library
//
//        Sergei Kolos,    January 2000
//
//        description:
//              Test sending commands to IS object providers
//	    
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <unistd.h>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <is/infoiterator.h>
#include <owl/timer.h>

int test(ISInfoDictionary& id, ISInfo& ti, const char * test_name, int verbose);   

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    // Declare arguments
    CmdArgBool	verbose ( 'v', "verbose", "turn on verbose mode." );
    CmdArgStr	partition_name( 'p', "partition", "partition-name", "partition to work in." );
    CmdArgStr	command( 'c', "command", "command", "command to send.", CmdArg::isREQ );
    CmdArgStr	server_name( 'n', "server", "server-name", "server to work with.",CmdArg::isREQ );
    CmdArgStr	info('r', "info", "info_objects", "objects to send command to (regular expression)");
 
    // Declare command object and its argument-iterator
    CmdLine    cmd(*argv, &verbose, &partition_name, &server_name, &command, &info, NULL);
    
    CmdArgvIter    arg_iter(--argc, ++argv);
 
    cmd.description(	"This program is part of the IS test suit.\n"
			"It send commands to information object providers.");
    
    info = ".*";
    
    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition p(partition_name);
    
    int error_count = 0;
    
    std::string expr((const char*)info);
    expr += "\\.0";
    
    try
    {
	OWLTimer timer;
	timer.start();
	ISInfoIterator it( p, (const char*)server_name, ISCriteria(expr) );
	while ( it() )
	{
	    if ( verbose )
	    {
		std::cerr << "Information Name is " << it.name() << std::endl;
	    }

	    try
	    {
		it.sendCommand( (const char*)command );
		if ( verbose )
		{
		    std::cout << "sendCommand successfull " << std::endl;
		}
	    }
	    catch( daq::is::Exception & ex )
	    {
		error_count++;
		ers::error( ex );
	    }
	}
	timer.stop();

	if ( verbose )
	{
	    std::cout << "total time = " << timer.totalTime() * 1000 << std::endl;
	}
    }
    catch( daq::ipc::Exception & ex )
    {
    	ers::fatal ( ex );
        return 1;
    }
    
    return error_count;
}

