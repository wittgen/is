////////////////////////////////////////////////////////////////////////
//	named_info.cc
//
//	Test application for the IS library
//
//	Sergei Kolos,    January 2000
//
//	description:
//		Test the functionality of the ISNamedInfo class
//		
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <is/test/LargeInfoNamed.h>
#include <is/test/MediumInfoNamed.h>

using is::test::LargeInfoNamed;
using is::test::MediumInfoNamed;

void test( ISNamedInfo& ti, const std::string & name );

int main( int argc, char ** argv )
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    CmdArgStr		partition_name ('p', "partition", "partition-name", "partition to work in.");
    CmdArgStrList	info_names ('N', "names", "name", "names of information to create.", CmdArg::isREQ | CmdArg::isLIST);
    CmdArgStr		server_name ('n', "server", "server-name", "server to work with.",CmdArg::isREQ);
 
    CmdLine	cmd(*argv, &partition_name, &server_name, &info_names, NULL);
    CmdArgvIter	arg_iter(--argc, ++argv);
 
    cmd.description(	"This program is part of the IS functionality test suit.\n"
			"It verifies the ISNamedInfo class functionality." );
    
    cmd.parse(arg_iter);

    IPCPartition p(partition_name);
    
    try
    {
	for ( size_t i = 0; i < info_names.count(); i++ )
	{
	    std::cerr << "Information Name is " << info_names[i] << std::endl;
	    std::string	    name(server_name);
	    name += ".";
	    name += info_names[i];
	    LargeInfoNamed  li( p, name.c_str() );
	    test( li, name );
	}

	for ( size_t i = 0; i < info_names.count(); i++ )
	{
	    std::cerr << "Information Name is " << info_names[i] << std::endl;
	    std::string	    name(server_name);
	    name += ".";
	    name += info_names[i];
	    MediumInfoNamed mi( p, name.c_str() );
	    test( mi, name );
	}
    }
    catch( daq::is::Exception & ex )
    {
    	is::fatal( ex );
    }
    
    return 0;
}
    
void test( ISNamedInfo & ti, const std::string & name )
{        
    ti.checkin();
    std::cout << "Checkin successfull " << std::endl; 		
	
    if ( !ti.isExist() )
    {
	std::cerr << "call to isExist for " << name << " fails " << std::endl;
    }
    else
    {
	std::cout << "call to isExist successfull " << std::endl; 		
    }
    	
    ti.checkout();
    std::cerr << "Checkout for " << name << " successfull " << std::endl;
    	    	
    ti.remove();
    std::cout << "Remove for " << name << " successfull " << std::endl; 		
}
