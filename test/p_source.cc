////////////////////////////////////////////////////////////////////////
//            p_source.cc
//
//            Test application for the IS library
//
//            Sergei Kolos,    August 2002
//
//            description:
//                measures the performance of the IS update operation
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>

#include <unistd.h>
#include <stdio.h>
#include <math.h>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <cmdl/cmdargs.h>
#include <owl/timer.h>

#include <is/test/LargeInfo.h>
#include <is/test/MediumInfo.h>
#include <is/infodictionary.h>
#include <is/infoT.h>

using is::test::LargeInfo;
using is::test::MediumInfo;

typedef char is_string[256];

static CmdArgBool	quiet ('q', "quiet", "print only summary lines to standard error.");
static CmdArgBool	no_summary ('U', "no-summary", "don't print summary lines to standard error.");
static CmdArgBool	use_tags ('T', "test-tags", "publish objects with different tags.");
static CmdArgInt	delay ('t', "delay", "seconds","delay between different operations (1 by default).");
static CmdArgInt	initial_delay ('D', "initial-delay", "seconds","delay before starting tests (0 by default).");
static CmdArgInt	info_num('N', "number", "information-number", "number of informations to be created ( 10 by default ).");
static CmdArgInt	repeate_num('r', "repetition", "repetition-number", "number of repetitions (1000 by default).");
static CmdArgStr	server_name('n', "server", "server-name", "server to work with.",CmdArg::isREQ);

static int 		errors;
static int		count;
static double		tmin = 1000000.;
static double		tmax = 0.;
static is_string *	name;
static double *		data;

void	test(ISInfoDictionary& id, ISInfo& ti);
void	start(ISInfoDictionary& id, ISInfo& ti);
void	stop(ISInfoDictionary& id);
void	stat( double & mean, double & dev );

// This information is used to test the IS monitor
struct INFO : public ISInfo
{
    int i1, i2, i3, i4, i5;
	
    INFO() {
	i1 = i2 = i3 = i4 = i5 = 0;
    }
	
    void publishGuts( ISostream & out ) {
	out << i1++ << i2++ << i3++ << i4++ << i5++;
    }
	
    void refreshGuts( ISistream & in ) {
	in >> i1 >> i2 >> i3 >> i4 >> i5;
    }
};

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStr	suffix('S', "suffix", "name_suffix", "suffix for info name to be unique.",CmdArg::isREQ);
    CmdArgChar	size ('s', "size", "object_size", "define size of the information objects to be used\n"
    					"S - small objects [one integer attribute]\n"
    					"M - medium objects (default) [one simple attribute of each basic type]\n"
    					"L - large objects [one simle attribute plus one array attribute for each basic type]\n");

    CmdLine    cmd(*argv, &quiet, &no_summary, &delay, &partition_name, &server_name, 
    			  &info_num, &repeate_num, &suffix, &size, &initial_delay, &use_tags, NULL);
    CmdArgvIter    arg_iter(--argc, ++argv);
 
    delay = 1;
    size = 'M';
    repeate_num = 1000;
    info_num = 10;
    initial_delay = 0;
    use_tags = false;
 
    cmd.description(	"This program is part of the IS performance test suit.\n"
			"It creates N information objects, updates them r times and then delete them.\n" );

    cmd.parse(arg_iter);
    
    IPCPartition	p(partition_name);
    ISInfoDictionary	id(p);
    
    data = new double[ info_num * repeate_num ];
    
    name = new is_string[info_num]; 
    for ( int i = 0; i < info_num; i++ )
    {
    	sprintf(name[i],"%s.%s.%d",(const char*)server_name,(const char*)suffix,i);
    }
        
    //
    // Set up standard output streams' format
    //
    std::cout.setf( std::ios::fixed, std::ios::floatfield );
    std::cout << std::setprecision( 2 );
    std::cerr.setf( std::ios::fixed, std::ios::floatfield );
    std::cerr << std::setprecision( 2 );
    
    if ( !no_summary )
    {
	std::cerr << "IS test started (pid = " << getpid() << ") : info size is \"" << size << "\", info number is " << info_num;
	std::cerr << ", repetitions number is " << repeate_num;
	std::cerr << ", delay between repetitions is " << delay << " second(s)." << std::endl;
    }
        
    LargeInfo li;
    
    MediumInfo mi;
    
    ISInfoLong isl;
    isl = 13;
    
    INFO iii;
    
    ISInfo * ptr;
    switch ( size )
    {
    	case 'L':
		ptr = &li;
		break;
     	case 'M':
		ptr = &mi;
		break;
    	case 'S':
		ptr = &isl;
		break;
    	case 'I':
		ptr = &iii;
		break;
	default:
 		std::cerr << "[" << size << "] - invalid value for the size parameter " << std::endl;
		return 1;
    }
    
    if ( !use_tags )
    	start( id, *ptr );
    for ( int j = 0; j < repeate_num; j++ )
    {
	
	test(id, *ptr);
	if ( delay != 0 )
	    sleep( (int)delay );
    }
    if ( !use_tags )
    	stop( id );
    

    if ( !no_summary )
    {
    	double mean, dev;
	
	stat( mean, dev );
	std::cerr << "IS test finished (pid = " << getpid() << ") : " ;
	std::cerr << count << " updates done, " << errors << " updates failed : " << std::endl;
	
	std::cerr << "min/avg/max/dev = " << tmin * 1000. << "/"
				       << mean * 1000. << "/" 
				       << tmax * 1000. << "/" 
				       << dev  * 1000. << " ms" << std::endl;
    }
 
    delete[] data;
    return 0;
}

void test( ISInfoDictionary& id, ISInfo& ti )
{            
    for ( int i = 0; i < info_num; i++ )
    {    	
	OWLTimer t;
	t.start();
	
	try
        {
            id.checkin(name[i],ti,use_tags);
        }
        catch( daq::is::Exception & ex )
        {
            errors++;
            ers::error( ex );
        }

 	t.stop();
	
	data[count++] = t.totalTime();
	
	tmin = ( tmin < t.totalTime() ) ? tmin : t.totalTime();
	tmax = ( tmax > t.totalTime() ) ? tmax : t.totalTime();
	
	if ( !quiet )
	{
	    std::cout << count << "\t" << t.totalTime() * 1000 << std::endl;
	}
    }
        
    if ( errors > 20 )
    {
    	std::cerr << "Too many errors. Exiting." << std::endl;
	exit( 1 );
    }
}

void start( ISInfoDictionary& id, ISInfo& ti )
{    
    OWLTimer t;
    
    t.start();
    for ( int i = 0; i < info_num; i++ )
    {
	try
        {
            id.insert( name[i], ti );
	}
        catch( daq::is::Exception & ex )
        {
            errors++;
            ers::error( ex );
        }
    }
    t.stop();
    
    if ( t.totalTime() < initial_delay )
    	usleep( (int)( ( initial_delay - t.totalTime() ) * 1000000 ) );
}

void stop( ISInfoDictionary& id )
{
    for ( int i = 0; i < info_num; i++ )
    {
	try
        {
            id.remove(name[i]);
	}
        catch( daq::is::Exception & ex )
        {
            errors++;
            ers::error( ex );
        }
    }
}

void stat( double & mean, double & dev )
{
    int i;
    mean = 0.;

    for ( i = 0; i < count; i++ )
    {
	mean += data[i];
    }
    mean /= count;

    dev = 0.;

    for ( i = 0; i < count; i++ )
    {
	dev += ( data[i] - mean ) * ( data[i] - mean );
    }
    dev /= count;

    dev = sqrt( dev );
}
