#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include "Person.h"
#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Describes a worker person
 * 
 * @author  produced by the IS generator
 */

class Employee : public Person {
public:

    /**
     * Employee's salary
     */
    unsigned short                salary;


    static const ISType & type() {
	static const ISType type_ = Employee( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	Person::print( out );
	out << std::endl;
	out << "salary: " << salary << "\t// Employee's salary";
	return out;
    }

    Employee( )
      : Person( "Employee" )
    {
	initialize();
    }

    ~Employee(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Employee( const std::string & type )
      : Person( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	Person::publishGuts( out );
	out << salary;
    }

    void refreshGuts( ISistream & in ){
	Person::refreshGuts( in );
	in >> salary;
    }

private:
    void initialize()
    {
	salary = 3000;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Employee & info ) {
    info.print( out );
    return out;
}

#endif // EMPLOYEE_H
