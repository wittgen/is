#!/bin/sh
#################################################################
#
#	Test Script for the IS package
#	Created by Sergei Kolos; 10.02.02
#
#################################################################

if test $TDAQ_IPC_INIT_REF
then
	ref_param="-Dtdaq.ipc.init.ref=$TDAQ_IPC_INIT_REF"
fi

echo
echo "Creating information ..."
$1is_create_info -n IStest -P Person1 -E Employee1
echo
echo "Reading information ..."
echo "Java command is {$2 -classpath $3 $ref_param ReadInfo IStest Person1 Employee1}"
$2 -classpath $3 $ref_param ReadInfo IStest Person1 Employee1
echo "done."
