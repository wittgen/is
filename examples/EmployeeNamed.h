#ifndef EMPLOYEENAMED_H
#define EMPLOYEENAMED_H

#include "PersonNamed.h"
#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Describes a worker person
 * 
 * @author  produced by the IS generator
 */

class EmployeeNamed : public PersonNamed {
public:

    /**
     * Employee's salary
     */
    unsigned short                salary;


    static const ISType & type() {
	static const ISType type_ = EmployeeNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	PersonNamed::print( out );
	out << std::endl;
	out << "salary: " << salary << "\t// Employee's salary";
	return out;
    }

    EmployeeNamed( const IPCPartition & partition, const std::string & name )
      : PersonNamed( partition, name, "Employee" )
    {
	initialize();
    }

    ~EmployeeNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    EmployeeNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : PersonNamed( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	PersonNamed::publishGuts( out );
	out << salary;
    }

    void refreshGuts( ISistream & in ){
	PersonNamed::refreshGuts( in );
	in >> salary;
    }

private:
    void initialize()
    {
	salary = 3000;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const EmployeeNamed & info ) {
    info.print( out );
    return out;
}

#endif // EMPLOYEENAMED_H
