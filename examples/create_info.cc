////////////////////////////////////////////////////////////////////////
//    create_info.cc
//
//    Test application for the IS library
//
//    Sergei Kolos,  January 2002
//
//    description:
//          Test the functionality of the ISNamedInfo class
//      
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <is/infodynany.h>

#include "PersonNamed.h"
#include "EmployeeNamed.h"

#define PRINT_SIMPLE_ATTRIBUTE(x,y)		case ISType::y : is::print( out, any.getAttributeValue<x>( i ) ) << ", "; break;					   
#define PRINT_SIMPLE_ATTRIBUTE_SEPARATOR
#define PRINT_SIMPLE_ATTRIBUTE_OBJECT_TYPE	ISInfoDynAny

#define PRINT_VECTOR_ATTRIBUTE(x,y)		case ISType::y : is::print( out, any.getAttributeValue<std::vector<x> >( i ) ) << ", "; break;					   
#define PRINT_VECTOR_ATTRIBUTE_SEPARATOR
#define PRINT_VECTOR_ATTRIBUTE_OBJECT_TYPE	ISInfoDynAny

void 
print_dynany( std::ostream & out, const ISInfoDynAny & any )
{
    out << "value = [ ";
    for ( size_t i = 0; i < any.getAttributesNumber(); ++i )
    {
	if ( any.isAttributeArray( i )  )
	{
	    switch ( any.getAttributeType( i ) )
	    {
		IS_TYPES( PRINT_VECTOR_ATTRIBUTE )
		default	:	ERS_ASSERT( false ); //must not happen
	    }
	}
	else
	{
	    switch ( any.getAttributeType( i ) )
	    {
		IS_TYPES( PRINT_SIMPLE_ATTRIBUTE )
		default	:	ERS_ASSERT( false ); //must not happen
	    }
	}
        
        if ( i != any.getAttributesNumber() - 1 ) 
	    out << ", ";
    }
    out << " ] " << std::endl;
}

void 
test_dynany( const IPCPartition & p, const std::string & name )
{
    ISInfoDictionary id( p );
    
    ISInfoDynAny ida;
    id.getValue( name, ida );
    std::cout << ">>>>>>>>> getValue = ok" << std::endl;
    print_dynany( std::cout, ida );

    std::vector<ISInfoDynAny> & children = ida.getAttributeValue<std::vector<ISInfoDynAny> >( "children" );
    size_t s = children.size();
    children.resize( s + 1 );
    ISInfoDynAny & c = children[s];
    c.getAttributeValue<std::string>( "name" ) = "Anton";
    c.getAttributeValue<OWLDate>( "birth date" ) = OWLDate( "06/06/07" );
    c.getAttributeValue<int>( "sex" ) = PersonNamed::male;
    id.update( name, ida );
    std::cout << ">>>>>>>>> update = ok" << std::endl;
    
    id.getValue( name, ida );
    std::cout << ">>>>>>>>> getValue = ok" << std::endl;
    print_dynany( std::cout, ida );    
    
    ISInfoDynAny ida1( id.partition(), ida.type().name() );
    
    ida1.getAttributeValue<std::string>( "name" ) = "Adam";
    ida1.getAttributeValue<OWLDate>( "birth date" ) = OWLDate( "12/12/70" );
    ida1.getAttributeValue<int>( "sex" ) = PersonNamed::male;
    
    ISInfoDynAny & a = ida1.getAttributeValue<ISInfoDynAny>( "address" );
    a.getAttributeValue<std::string>( "street" ) = "Broadway";
    a.getAttributeValue<std::string>( "town" ) = "New York";
    a.getAttributeValue<unsigned short>( "number" ) = 22;
    a.getAttributeValue<unsigned int>( "zip" ) = 12345;
    
    std::vector<ISInfoDynAny> & children1 = ida1.getAttributeValue<std::vector<ISInfoDynAny> >( "children" );
    children1.resize( 2 );
    ISInfoDynAny & c1 = children1[0];
    c1.getAttributeValue<std::string>( "name" ) = "Avel";
    c1.getAttributeValue<OWLDate>( "birth date" ) = OWLDate( "12/12/22" );
    c1.getAttributeValue<int>( "sex" ) = PersonNamed::male;
    
    ISInfoDynAny & c2 = children1[1];
    c2.getAttributeValue<std::string>( "name" ) = "Cain";
    c2.getAttributeValue<OWLDate>( "birth date" ) = OWLDate( "13/11/29" );
    c2.getAttributeValue<int>( "sex" ) = PersonNamed::male;
    std::cout << "Dynamically constructed ISInfoDynAny contains: " << std::endl;
    print_dynany( std::cout, ida1 );    
        
    id.checkin( name + "_dyn", ida1 );
    std::cout << ">>>>>>>>> checkin = ok" << std::endl;
    
    ISInfoDynAny ida2;
    id.getValue( name + "_dyn", ida2 );
    std::cout << ">>>>>>>>> getValue = ok" << std::endl;
    print_dynany( std::cout, ida2 );    
}

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    // Declare arguments
    CmdArgStr    partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStr    person_name('P', "person-name", "name", "name of information to create.", CmdArg::isREQ);
    CmdArgStr    employee_name('E', "employee-name", "name", "name of information to create.", CmdArg::isREQ);
    CmdArgStr    server_name('n', "server", "server-name", "server to work with.",CmdArg::isREQ);
 
    // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &partition_name, &server_name, &person_name, &employee_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);
 
    cmd.description("This program implements functionality tests for the Information Service.\n"
        "It creates, updates and deletes information objects.");
  
    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition      p(partition_name);
    
    std::string    name(server_name);
    name += ".";
    name += person_name;
    
    PersonNamed    person( p, name );
    
    person.name = (const char*)person_name;
    person.birth_date = OWLDate("13/12/1970");
    person.sex = PersonNamed::male;
    person.address.number = 14;
    person.address.street = "rue des Hautains";
    person.address.town = "Saint Genis Pouilly";
    person.address.zip = 1630;
    person.children.resize( 1 );
    person.children[0].name = "Nikita";
    person.children[0].birth_date = OWLDate("28/10/2001");
    person.children[0].sex = Child::male;
    
    try
    {
	std::cout << "----------------------------- Person object name is '" << name << "' -----------------------------" << std::endl;
	
	person.checkin();
	std::cout << ">>>>>>>>> checkin = ok" << std::endl;

	PersonNamed	   person1( p, name );
	person1.checkout();
	std::cout << ">>>>>>>>> checkout = ok" << std::endl;
	std::cout << person1 << std::endl;
        
	std::cout << "Person's first child is " << person1.children[0] << std::endl;

        test_dynany( p, name );
        
	name = server_name;
	name += ".";
	name += employee_name;
	EmployeeNamed    employee( p, name );

	std::cout << "----------------------------- Employee object name is '" << name << "' -----------------------------" << std::endl;
	employee.checkin();
	std::cout << ">>>>>>>>> checkin = ok" << std::endl;

	employee.checkout();
	std::cout << ">>>>>>>>> checkout = ok" << std::endl;
	std::cout << employee << std::endl;
        
	PersonNamed	   plymorphic_person( p, name );
	plymorphic_person.checkout();
	std::cout << ">>>>>>>>> checkout of Emplyee as Person = ok" << std::endl;
	std::cout << employee << std::endl;
	
        test_dynany( p, name );        
    }
    catch( daq::is::Exception & ex )
    {
    	is::fatal( ex );
    }
    
    return 0;
}
