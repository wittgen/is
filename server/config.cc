/*
 * config.cc
 *
 *  Created on: Jun 12, 2013
 *      Author: kolos
 */

#include <stdlib.h>
#include <limits>
#include <sstream>

#include <boost/lexical_cast.hpp>

#include <ers/ers.h>

#include <is/server/config.h>

namespace
{
    template <typename T>
    T get_environment_value(const char * name, T default_value)
    {
	char * var = getenv(name); // default value (in bytes)
	T value;
	if (var)
	{
	    try
	    {
		value = boost::lexical_cast<T>(var);
		ERS_DEBUG(0, name << " is set to " << value << " ");
		return value;
	    }
	    catch(boost::bad_lexical_cast &)
	    {
		ERS_DEBUG(0,
		    " invalid value '" << var << "' has been used for the "
		        << name << " variable." << " Default value (" << default_value
		        << ") will be used");
		return default_value;
	    }
	}

	ERS_DEBUG(0, name << " variable is set to its default value " << default_value << " ");
	return default_value;
    }
}

bool ISConfig::ForcedHistory = get_environment_value(
    "IS_SERVER_FORCED_HISTORY", false);

unsigned int ISConfig::HistoryNumDepth = get_environment_value<unsigned int>(
    "IS_SERVER_HISTORY_DEPTH", 100);

int64_t ISConfig::HistoryTimeDepth = get_environment_value<int64_t>(
    "IS_SERVER_HISTORY_TIME_RANGE", std::numeric_limits<int64_t>::max());

unsigned int ISConfig::MaxToleratedTimeouts = get_environment_value<unsigned int>(
    "IS_SERVER_MAX_TOLERATED_TIMEOUTS", 100);

unsigned int ISConfig::WorkerThreads =
    get_environment_value<unsigned int>("IS_SERVER_WORKER_THREADS", 16);

unsigned int ISConfig::MaxConcurrentCallbacks =
    get_environment_value<unsigned int>("IS_SERVER_MAX_ACTIVE_INVOCATIONS",
        ISConfig::WorkerThreads + 1);

unsigned int ISConfig::MaxCallbacksQueueSize =
    get_environment_value<unsigned int>("IS_SERVER_MAX_QUEUE_SIZE", 100000);

unsigned int ISConfig::CallbacksReportThreshold =
    get_environment_value<unsigned int>("IS_SERVER_REPORT_THRESHOLD",
        ISConfig::MaxCallbacksQueueSize / 50);

unsigned int ISConfig::ReceiversCheckupPeriod =
    get_environment_value<unsigned int>("IS_SERVER_CHECKUP_THRESHOLD", 60);

