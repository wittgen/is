#include <is/criteria.h>

#include <is/server/criteriahelper.h>
#include <is/server/exceptions.h>

using namespace std::placeholders;

bool
is::types_equal( const is::type & t1, const is::type & t2 )
{
    if (strcmp( t1.id, t2.id )) {
        return false;
    }
    return !strcmp( t1.name, t2.name );
}

bool
is::super_sub_type( const is::type & t1, const is::type & t2 )
{
    const char * p1 = t1.id;
    const char * p2 = t2.id;

    while (*p1) {
        if (!*p2) {
            return false;
        }
        if (*p1++ != *p2++) {
            return false;
        }
    }
    return true;
}

ISCriteriaHelper::ISCriteriaHelper( const is::criteria & criteria )
  : m_criteria( criteria )
{ 
    std::ostringstream o;
    o << m_criteria;
    m_string = o.str();

    TypeMatcher tmatcher;
    switch ( m_criteria.type_.logic_ )
    {
        case is::criteria::type::Ignore:
            break;
        case is::criteria::type::Exact:
            tmatcher = std::bind( &ISCriteriaHelper::matchTypeExact, this, _1 );
            break;
        case is::criteria::type::Base:
            tmatcher = std::bind( &ISCriteriaHelper::matchTypeBase, this, _1 );
            break;
        case is::criteria::type::Not:
            tmatcher = std::bind( &ISCriteriaHelper::matchTypeNot, this, _1 );
            break;
        case is::criteria::type::NotBase:
            tmatcher = std::bind( &ISCriteriaHelper::matchTypeNotBase, this, _1 );
            break;
        default:
            throw is::InvalidCriteria( "Bad type logic value" );
    }

    if ( !m_criteria.name_.ignore_ )
    {
	m_owl_regex = m_criteria.name_.value_;
        
        try {
            m_boost_regex = boost::regex( m_criteria.name_.value_ );
        }
        catch ( boost::bad_expression & ex ){
	    throw is::InvalidCriteria( m_criteria.name_.value_ );
        }

        NameMatcher nmatcher;
        std::string re = m_criteria.name_.value_.in();
        if (re == ".*" || re == ".+") {
            nmatcher = [](const char * ) { return true; };
        }
        else if ( m_owl_regex.status() == OWLRegexp::Ok ) {
	    nmatcher = std::bind( &ISCriteriaHelper::matchName, this, _1 );
        }
	else {
            nmatcher = std::bind( &ISCriteriaHelper::matchNameBoost, this, _1 );
	}

	if ( tmatcher ) {
            switch ( m_criteria.logic_ )
            {
                case is::criteria::And:
                    m_match = std::bind( &ISCriteriaHelper::AND,
                            this, nmatcher, tmatcher, _1, _2 );
                    break;
                case is::criteria::Or:
                    m_match = std::bind( &ISCriteriaHelper::OR,
                            this, nmatcher, tmatcher, _1, _2 );
                    break;
                default:
                    throw is::InvalidCriteria( "Bad logic value" );
            }
	} else {
            m_match = std::bind( nmatcher, _1 );
	}
    }
    else {
	if ( tmatcher ) {
            m_match = std::bind( tmatcher, _2 );
        } else {
            throw is::InvalidCriteria( "Void criteria" );
        }
    }
}

const std::string &
ISCriteriaHelper::toString() const
{
    return m_string;
}
