package is

author	Serguei.Kolos@cern.ch
manager	Serguei.Kolos@cern.ch

use OnlinePolicy
use owl
use cmdl
use rdb
use tbb
use xmext
use jeformat

macro sw.repository.binary.is_server:name		"is_server"
macro sw.repository.binary.is_server:default.parameters	"-s -p env(TDAQ_PARTITION) -n env(TDAQ_APPLICATION_NAME)"
macro sw.repository.binary.is_rm:name			"is_rm"
macro sw.repository.binary.is_rm:default.parameters	"-p env(TDAQ_PARTITION)"
macro sw.repository.binary.is_ls:name			"is_ls"
macro sw.repository.binary.is_ls:default.parameters	"-p env(TDAQ_PARTITION)"
macro sw.repository.java.is.jar:name			"is java api" 
#===============================================================================#
public
#===============================================================================#
make_fragment	generate-is	-suffix=xml

#################################################################################
#       Jar installation is in public section in order to 
#	allow automatic CLASSPATH composition
#################################################################################
apply_pattern	install_jar	name=is_lib	files=is.jar

apply_pattern	install_jar	name=is_util	files=is_util.jar

apply_pattern	install_jar	name=is_gen	files=is_generator.jar

#===============================================================================#
private
#===============================================================================#
macro_remove	pp_cppflags	"-DERS_NO_DEBUG"

#################################################################################
#	Library targets
#################################################################################
library		is	 $(lib_opts)	"../idl/is.idl \
					 ../src/*.cc"
                                                         
library		isproxy	 $(lib_opts)	"../proxy/*.cc"
							
#################################################################################
#	Applications targets
#################################################################################
application	is_server	-suffix="-server" \
				"../server/*.cc "
						 
application	is_ls		"../bin/isls.cc \
				 ../bin/printany.cc"
						 
application	is_rm		"../bin/isrm.cc"

application	is_cmd		"../bin/iscmd.cc"

application	is_write	"../bin/publish_any.cc"
						 
application	is_monitor	"../gui/main.cc \
				 ../gui/printvalue.cc \
				 ../gui/infodialog.cc \
				 ../gui/updatequeue.cc \
				 ../gui/tables.cc \
				 ../gui/ismonitor.cc"

#################################################################################
#	Include statements
#################################################################################
macro	app_is_monitor_pp_cppflags	"$(X_includes) "

#################################################################################
#	Linker options
#################################################################################
macro	is_shlibflags		"-lrdb -lipc -lowl -lers -lomniORB4 -lomnithread -lz"
macro	isproxy_shlibflags	"-lis"

macro	is_serverlinkopts	"-lis -ltbb -lboost_filesystem-$(boost_libsuffix) \
				 -lboost_regex-${boost_libsuffix} -lpmgsync -lcmdline"
macro	is_rmlinkopts		"-lis -lcmdline"
macro	is_writelinkopts	"-lis -lcmdline"
macro	is_cmdlinkopts		"-lis -lcmdline"
macro	is_lslinkopts		"-lis -lcmdline"
macro	is_monitorlinkopts	"-lis $(xmext_libs) $(X_linkopts) -luuid"

#################################################################################
#	Dependencies
#################################################################################
macro	is_server_dependencies		"is"
macro	isproxy_dependencies		"is"
macro	is_rm_dependencies		"is"
macro	is_cmd_dependencies		"is"
macro	is_ls_dependencies		"is"
macro	is_write_dependencies		"is"
macro	is_monitor_dependencies		"is"

################################################################################
#	Java stuff
############################################################################### 
apply_pattern	build_jar	name=is			src_dir=../jsrc \
                                                	sources=is/*.java \
                                               		idl_sources="../idl/is.idl"

apply_pattern	build_jar	name=is_generator	src_dir=../jbin \
                                                	sources=is/tool/*.java \
                                                	manifest=manifest.mf \
							others="images/*.gif"

apply_pattern	build_jar	name=is_util		src_dir=../jbin \
                                                	sources=is/util/*.java \
							others="images/ontop.gif \
                                                        	images/offtop.gif \
                                                                images/close.gif \
                                                                images/clickme.gif"

macro_prepend	classpath	"$(bin)java/is_generator.jar:$(bin)java/is.jar:"

apply_pattern	build_jar	name=is_test	src_dir="." \
						sources="../jtest/*.java $(bin)is/test/*.java" 

apply_pattern	build_jar	name=is_example	src_dir="." \
						sources="../examples/*.java $(bin)is/*.java" 
				
document        generate-is	example_gen	-s=../data \
						namespace="is" \
						package="is" \
						example.xml
macro		example_gen_options		"--named --simple"

document        generate-is    test_gen		-s=../data \
						namespace="is::test" \
						package="is.test" \
						test.xml
macro		test_gen_classes		"LargeInfo MediumInfo"
macro		test_gen_options		"--named --simple"

macro           test_gen_dependencies		"is_generator.jar"
macro           example_gen_dependencies	"is_generator.jar"

macro           is_generator.jar_dependencies	"is.jar"
macro           is_util.jar_dependencies	"is.jar"
macro           is_example.jar_dependencies	"is_generator.jar example_gen"
macro           is_test.jar_dependencies	"is_generator.jar test_gen"
				
################################################################################
#	Installation patterns
################################################################################
apply_pattern	install_headers		src_dir="../is/test" \
					files="*.h" \
					target_dir="test"
				
apply_pattern   install_libs		files="libis$(lib_suffix) libisproxy$(lib_suffix)"

apply_pattern   install_apps		files=" is_server is_ls is_rm is_cmd is_monitor is_write \
						is_test_source is_test_receiver \
						is_f_source is_f_receiver \
						is_lst_provider is_lst_commander"

apply_pattern   install_examples        name=cpp_examples	src_dir=../examples	\
								target_dir=cpp		\
								files="create_info.cc README makefile.CPP"
								
apply_pattern   install_examples        name=java_examples	src_dir=../examples	\
								target_dir=java		\
								files="ReadInfo.java README makefile.JAVA"
								
apply_pattern   install_examples        name=data_examples	src_dir=../data		\
								target_dir=data		\
								files="example.xml"
								
apply_pattern   install_data		name=data	   	src_dir=../data		\
								files="is.xml"

########################################################################################################
#	C++ test applications
########################################################################################################
application	is_f_source			"../test/f_source.cc"
application	is_f_receiver			"../test/f_receiver.cc"
application	is_f_commander			"../test/f_commander.cc"
application	is_test_source			"../test/p_source.cc"
application	is_test_receiver		"../test/p_receiver.cc"
application	is_lst_provider			"../test/lst_provider.cc"
application	is_lst_commander		"../test/lst_commander.cc"
application	is_named_info			"../test/named_info.cc"
						 
application	is_create_info			"../examples/create_info.cc"

macro		is_f_sourcelinkopts		"-lis -lcmdline"
macro		is_f_receiverlinkopts		"-lis -lcmdline"
macro		is_f_commanderlinkopts		"-lis -lcmdline"
macro		is_test_sourcelinkopts		"-lis -lcmdline"
macro		is_test_receiverlinkopts	"-lis -lcmdline"
macro		is_lst_providerlinkopts		"-lis -lcmdline"
macro		is_lst_commanderlinkopts	"-lis -lcmdline"
macro		is_named_infolinkopts		"-lis -lcmdline"
macro		is_create_infolinkopts		"-lis -lcmdline"

macro 		is_f_source_dependencies	"is test_gen"
macro 		is_f_receiver_dependencies	"is test_gen"
macro 		is_f_commander_dependencies	"is test_gen"
macro 		is_test_source_dependencies	"is test_gen"
macro 		is_test_receiver_dependencies	"is test_gen"
macro 		is_lst_provider_dependencies	"is test_gen"
macro 		is_lst_commander_dependencies	"is test_gen"
macro 		is_named_info_dependencies	"is test_gen"
macro 		is_create_info_dependencies	"is is_generator.jar example_gen test_gen"

########################################################################################################
#	Check target
########################################################################################################

macro		is_check_check_args	"../ $(bin) $(bin) $(bin) '$(java)' \
					$(bin)java/is_example.jar:$(bin)java/is_test.jar:$(bin)java/is.jar:$(TDAQ_CLASSPATH)"
		
document 	script_launcher is_check -group=check -s=.. execute_check_target.sh 
