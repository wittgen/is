package is;

public interface Callback
{    
    public is.callback objectReference();
    
    public int eventMask();
}
