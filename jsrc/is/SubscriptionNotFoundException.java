package is;

/**
 * Thrown to indicate that the subscription for the IS repository
 * does not exist.
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class SubscriptionNotFoundException extends Exception 
{
    /**
     * Constructs an SubscriptionNotFoundException with the specified
     * infromation name.
     *
     * @param name information name
     */
    public SubscriptionNotFoundException( String name ) 
    {
	super( "Subscription to the '" + name + "' information is not found" );
    }
    
    /**
     * Constructs an SubscriptionNotFoundException with the specified
     * infromation name.
     *
     * @param server_name IS server name
     * @param criteria subscription criteria
     */
    public SubscriptionNotFoundException( String server_name, Criteria criteria ) 
    {
	super( "Subscription to the '" + server_name + 
        	"' IS server with '" + criteria.toString() + "' criteria is not found" );
    }
}
