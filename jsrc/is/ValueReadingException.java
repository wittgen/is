package is;

/**
 * Thrown to indicate that the IS repository either is not running
 * the infromation object does not exist.
 *
 * @author  Serguei Kolos
 * @version 10/10/15
 * @since   
 */
public class ValueReadingException extends RuntimeException 
{
    /**
     * Constructs an ValueReadingException with the specified
     * detail message.
     *
     * @param partition the TDAQ partition
     * @param name the IS server name
     */
    public ValueReadingException( ipc.Partition partition, String name ) 
    {
	super( "IS repository '" + name + "' is not found in the '" 
        	+ partition.getName() + "' partition" );
    }
    
    /**
     * Constructs an ValueReadingException with the specified
     * detail message.
     *
     * @param partition the TDAQ partition
     * @param name the IS server name
     */
    public ValueReadingException( ipc.Partition partition, String serverName, String infoName ) 
    {
	super( "IS information object '" + infoName + "' is not found in the '" + serverName + "' server in '"
        	+ partition.getName() + "' partition" );
    }
    
}
