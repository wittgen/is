package is;

/**
 * This interface has to be extended by an IS subscriber which wants
 * to receive full IS infomation when certain events happens in the
 * IS repository.
 * 
 * @author Sergei Kolos
 * @see InfoListener
 * @see Repository
 */

public interface InfoReceiver extends Listener
{
}
