package is;

import ipc.Partition;

/**
 * Alternative base class for the IS information type definition. Any specific information class must inherit 
 * either this or the Info class and implement the publushGuts and refreshGutes methods defined by the Streamable interface.
 * Contrary to the Info class this class itself contains methods to work with the IS repository.
 * 
 * @author Sergei Kolos
 */
public abstract class NamedInfo extends Info implements CommandListener
{
    private Repository	repository;
    
    /**
     * Constructs the information object that will has the <tt>name</tt> name and belong to the 
     * <tt>partition</tt> partition. The <tt>checkin</tt> and <tt>checkout</tt> methods will use
     * these name and partition to work with the appropriate IS server in the appropriate partition.
     * @param partition working partition
     * @param name information name
     * @deprecated Available for the compatibility with the previous IS versions. One should use
     * the constructor which provides the type name for the information object.
     */
    public NamedInfo( Partition partition, String name )
    {
    	this( partition, name, Type.UnknownName );
    }
    
    /**
     * Constructs the information object that will has the <tt>name</tt> name and belong to the 
     * <tt>partition</tt> partition. The <tt>checkin</tt> and <tt>checkout</tt> methods will use
     * these name and partition to work with the appropriate IS server in the appropriate partition.
     * @param partition working partition
     * @param name information name
     * @param type name of the information type
     */
    public NamedInfo( Partition partition, String name, String type )
    {
        super ( type );
        repository = new Repository( partition );
        
        if ( name.indexOf( "." ) == -1 )
        {
            throw new is.InvalidNameException(name);
        }
	setName( name );
        
	try {
	    Class<?> cl = getClass().getMethod("command", String.class).getDeclaringClass();
	    if ( !cl.equals( is.NamedInfo.class ) )
	    {
		InfoProvider.instance().addCommandListener( this );
	    }
	}
        catch( Exception ex )
        {
        }
    }
    
    /**
     * Deregister this object from the command receiver. Call it to make
     * the object available for the garbage collector. 
     * Without this call the object will remain listening for external
     * commands but will never be reclaimed from memory.
     */
    public void release()
    {
	InfoProvider.instance().removeCommandListener( this );
    }
    
    /**
     * Creates the new information item in the IS repository using the name supplied 
     * at construction. If object with the same name already exist in the repository,
     * updates the value of this object.
     * @exception is.RepositoryNotFoundException an appropriate IS server is not running
     * @exception is.InfoNotCompatibleException information with the same name already exist in the IS repository and has different type
     */
    public void checkin ( ) throws	is.RepositoryNotFoundException,
                			is.InfoNotCompatibleException
    {        
    	repository.checkin( getName(), this );
    }

    /**
     * Creates the new information item in the IS repository using the name supplied 
     * at construction. If object with the same name already exist in the repository,
     * updates the value of this object.
     * @param keep_history keep the previous value of this information in the IS repository
     * @exception is.RepositoryNotFoundException an appropriate IS server is not running
     * @exception is.InfoNotCompatibleException information with the same name already exist in the IS repository and has different type
     */
    public void checkin ( boolean keep_history ) throws	is.RepositoryNotFoundException,
                					is.InfoNotCompatibleException
    {        
    	repository.checkin( getName(), this, keep_history );
    }
    
    /**
     * Creates the new information item in the IS repository using the name supplied 
     * at construction. If object with the same name already exist in the repository,
     * updates the value of this object.
     * @param tag tag for the new value of IS object, the previous value will be keept in the IS repository
     * @exception is.RepositoryNotFoundException an appropriate IS server is not running
     * @exception is.InfoNotCompatibleException information with the same name already exist in the IS repository and has different type
     */
    public void checkin ( int tag ) throws	is.RepositoryNotFoundException,
                				is.InfoNotCompatibleException
    {        
    	repository.checkin( getName(), this, tag );
    }
    
    /**
     * Update all the fields of this object according to the field values of the information object with the same
     * name in the IS repository.
     * @exception is.RepositoryNotFoundException an appropriate IS server is not running
     * @exception is.InfoNotFoundException information with this name does not exist in the IS repository
     * @exception is.InfoNotCompatibleException information with this name exists in the IS repository and has different type
     */
    public void checkout ( ) throws	is.RepositoryNotFoundException, 
                			is.InfoNotFoundException,
                			is.InfoNotCompatibleException
    {            
	repository.getValue( getName(), this );
    }
    
    /**
     * Update all the fields of this object according to the field values of the information object with the same
     * name and tag from the IS repository.
     * @param tag the tag of the value which has to be taken from IS
     * @exception is.RepositoryNotFoundException an appropriate IS server is not running
     * @exception is.InfoNotFoundException information with this name and tag does not exist in the IS repository
     * @exception is.InfoNotCompatibleException information with this name exists in the IS repository and has different type
     */
    public void checkout ( int tag ) throws	is.RepositoryNotFoundException, 
                				is.InfoNotFoundException,
                				is.InfoNotCompatibleException
    {            
	repository.getValue( getName(), this, tag );
    }
    
    /**
     * Remove the corresponding information object form the IS repository.
     * @exception is.RepositoryNotFoundException an appropriate IS server is not running
     * @exception is.InfoNotFoundException information object with this name does not exist in the IS repository
     */
    public void remove ( ) throws	is.RepositoryNotFoundException, 
                			is.InfoNotFoundException
    {            
	repository.remove( getName() );
    }
    
    /**
     * Gets the name associated with this information object.
     * @return information name
     */
    public String getName()
    {
        return super.getName();
    }
    
    /**
     * Sets the new name to this information object. The new name will 
     * replace the old one that was given at construction.
     * @param name new information name
     */
    public void setName( String name )
    {
        super.setName(name);
    }
    
    /**
     * Is called when the command for this information object is received. 
     * This method itself does nothing. User can override this method to handle commands.
     * @param cmd command
     */
    public void command( String cmd )
    {
    }
    
    public final void command( String name, String cmd )
    {
	if ( getName().equals( name ) )
	    command( cmd );
    }
}
