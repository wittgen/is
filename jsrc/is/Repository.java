package is;

import ipc.Partition;

import java.util.Hashtable;
import java.lang.reflect.*;

/**
 * This class provides main interface to the IS repository. By using the methods
 * of this class an application can create a new information in the IS repository, 
 * update or delete already existing information and also subscribe for the changes
 * in the IS repository. 
 * @author Sergei Kolos
 */
public class Repository
{
    private Partition					partition;
    private java.util.Hashtable<String, Callback>	subscriptions;
    
    /**
     * Creates interface object to the IS repository in the default partition.
     */
    public Repository( )
    {
	this( new Partition() );
    }
    
    /**
     * Creates interface object to the IS repository in the <tt>partition</tt> partition.
     */
    public Repository( Partition partition )
    {
	this.partition = partition;
	subscriptions = new java.util.Hashtable<String, Callback>();
    }

    /**
     * Returns partition which was used at the construction of this object.
     * @return partition
     */
    public ipc.Partition getPartition()
    {
    	return partition;
    }

    /**
     * If infomation already exists in the IS repository it will be updated, otherwise
     * inserts new information object in the IS repository.
     * @param name information name
     * @param ii new value for the information object
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void checkin ( String name, Info ii ) throws	is.RepositoryNotFoundException,
							is.InfoNotCompatibleException
    {
    	checkin( name, ii, false, false, ii.getTag() );
    }
    
    /**
     * If infomation already exists in the IS repository it will be updated, otherwise
     * inserts new information object in the IS repository.
     * @param name information name
     * @param ii new value for the information object
     * @param keep_history keep the previous value of this information in the IS repository
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void checkin ( String name, Info ii, boolean keep_history ) throws	is.RepositoryNotFoundException,
										is.InfoNotCompatibleException
    {
    	checkin( name, ii, keep_history, false, ii.getTag() );
    }
    
    /**
     * If infomation already exists in the IS repository it will be updated, otherwise
     * inserts new information object in the IS repository.
     * @param name information name
     * @param ii new value for the information object
     * @param tag tag for the new value of IS object, the previous value will be keept in the IS repository
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void checkin ( String name, Info ii, int tag ) throws	is.RepositoryNotFoundException,
									is.InfoNotCompatibleException
    {
    	checkin( name, ii, true, true, tag );
    }
    
    /**
     * Checks if the information with the <tt>name</tt> name exists in the IS repository.
     * @param name information name
     * @return true if information with such name exists, otherwise false
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     */
    public boolean contains ( String name ) throws is.RepositoryNotFoundException
    {		
	try
	{
	    is.repository rep = Repository.resolve( partition, name );
	    return rep.contains( getObjectName( name ) );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
    }
    
    /**
     * Inserts new information object to the IS repository.
     * @param name information name
     * @param ii information object
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoAlreadyExistException information object with the <tt>name</tt> name already exist
     */
    public void insert ( String name, Info ii ) throws	is.RepositoryNotFoundException,
							is.InfoAlreadyExistException
    {
	is.InfoOutStream out = new is.InfoOutStream( );
	ii.publishGuts( out );
	is.value value = new is.value( 
	        new is.attributes(ii.getTag(), ii.getTime().getTimeMicro(), ii.getAddress(), ii.flatAnnotations()), out.getData() );
	is.info  info  = new is.info( getObjectName( name ), ii.getType().getType(), value );
	
	try
	{
	    is.repository rep = Repository.resolve( partition, name );
	    rep.create( info );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.AlreadyExist ex )
	{
	    throw new is.InfoAlreadyExistException(name);
	}
    }
    
    /**
     * Inserts new information object to the IS repository.
     * @param name information name
     * @param ii information object
     * @param tag information objects tag
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoAlreadyExistException information object with the <tt>name</tt> name already exist
     */
    public void insert ( String name, Info ii, int tag ) throws	is.RepositoryNotFoundException,
							is.InfoAlreadyExistException
    {
	ii.setTag( tag );
        is.InfoOutStream out = new is.InfoOutStream( );
	ii.publishGuts( out );
	is.value value = new is.value( 
	        new is.attributes(ii.getTag(), ii.getTime().getTimeMicro(), ii.getAddress(), ii.flatAnnotations()), out.getData() );
	is.info  info  = new is.info( getObjectName( name ), ii.getType().getType(), value );
	
	try
	{
	    is.repository rep = Repository.resolve( partition, name );
	    rep.create( info );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.AlreadyExist ex )
	{
	    throw new is.InfoAlreadyExistException(name);
	}
    }
    
    /**
     * Updates information object in the IS repository.
     * @param name information name
     * @param ii new value for the information object
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void update ( String name, Info ii ) throws	is.RepositoryNotFoundException,
							is.InfoNotFoundException,
							is.InfoNotCompatibleException
    {
    	update( name, ii, false, false, ii.getTag() );
    }
    
    /**
     * Updates information object in the IS repository.
     * @param name information name
     * @param ii new value for the information object
     * @param keep_history keep the previous value of this information in the IS repository
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void update ( String name, Info ii, boolean keep_history ) throws	is.RepositoryNotFoundException,
							 			is.InfoNotFoundException,
							 			is.InfoNotCompatibleException
    {
    	update( name, ii, keep_history, false, ii.getTag() );
    }
    
    /**
     * Updates information object in the IS repository.
     * @param name information name
     * @param ii new value for the information object
     * @param tag tag for the new value of IS object, the previous value will be keept in the IS repository
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void update ( String name, Info ii, int tag ) throws	is.RepositoryNotFoundException,
							 	is.InfoNotFoundException,
							 	is.InfoNotCompatibleException
    {
    	update( name, ii, true, true, tag );
    }
    
    /**
     * Remove information object from the IS repository.
     * @param name information name
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     */
    public void remove ( String name ) throws   is.RepositoryNotFoundException,
		    				is.InfoNotFoundException
    {	    
	try
	{
	    is.repository rep = Repository.resolve( partition, name );

	    rep.remove( getObjectName( name ) );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.NotFound ex )
	{
	    throw new is.InfoNotFoundException(name);
	}
    }

    /**
     * Remove all information objects, which are matching given criteria, from the IS repository.
     * @param server_name IS server name
     * @param criteria information selection criteria
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InvalidCriteriaException given criteria contains invalid regular expression
     */
    public void removeAll ( String server_name, Criteria criteria ) throws   
    		is.RepositoryNotFoundException, is.InvalidCriteriaException
    {	    
	try
	{
	    is.repository rep = Repository.resolve( partition, server_name );

	    rep.remove_all( criteria.criteria );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, server_name);
	}
	catch ( is.InvalidCriteria ex )
	{
	    throw new is.InvalidCriteriaException(criteria);
	}
    }

    /**
     * Gets the value of the information object which is associated with a given tag and assign it to the <tt>info</tt>.
     * @param name information name
     * @param ii information object that will get the new value
     * @param tag the tag of the requested value
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name and the <tt>tag</tt> tag does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void getValue ( String name, Info ii, int tag ) throws	is.RepositoryNotFoundException, 
									is.InfoNotFoundException,
									is.InfoNotCompatibleException
    {
	is.infoHolder ih = new is.infoHolder();
	
	try
	{
	    is.repository rep = Repository.resolve( partition, name );

	    rep.get_value( getObjectName( name ), tag, ih );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.NotFound ex )
	{
	    throw new is.InfoNotFoundException(name);
	}
	
	ii.update(name, ih.value);
    }

    /**
     * Gets the value of the information object from the IS repository and assign it to the <tt>info</tt>.
     * @param name information name
     * @param ii information object that will get the new value
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>info</tt> object
     */
    public void getValue ( String name, Info ii ) throws	is.RepositoryNotFoundException, 
								is.InfoNotFoundException,
								is.InfoNotCompatibleException
    {
	is.infoHolder ih = new is.infoHolder();
	
	try
	{
	    is.repository rep = Repository.resolve( partition, name );

	    rep.get_last_value( getObjectName( name ), ih );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.NotFound ex )
	{
	    throw new is.InfoNotFoundException(name);
	}
	
	ii.update(name, ih.value);
    }
        
    /**
     * Returns the array of recent values for an information object from the IS repository.
     * The values will be time ordered with the most recent ones first.
     * @param cls information class
     * @param name information name
     * @param how_many maximum number of information object values to retrieve
     * @return array of user information objects. User has to cast the return value to the correct type. For example:
     * <pre>
     *  MyInfo values[];
     *  // Get all history values
     *  values = (MyInfo[])repository.getValues( MyInfo.class, my_info_name, -1 );
     *  // Get 10 history values
     *  values = (MyInfo[])repository.getValues( MyInfo.class, my_info_name, 10 );
     * </pre>
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>class</tt> type
     */
    public <T extends Info> T[] getValues ( Class<T> cls, String name, int how_many ) 
    				throws	is.RepositoryNotFoundException, 
					is.InfoNotFoundException,
					is.InfoNotCompatibleException
    {
        return getValues(cls, name, how_many, is.sorted.ByTime);
    }
    
    /**
     * Returns the array of recent values for an information object from the IS repository.
     * The order of values is defined by the last parameter.
     * @param cls information class
     * @param name information name
     * @param how_many maximum number of information object values to retrieve
     * @param order the order of values. Possible parameters are is.sorted.ByTime and is.sorted.ByTag
     * @return array of user information objects. User has to cast the return value to the correct type. For example:
     * <pre>
     *  MyInfo values[];
     *  // Get all history values time ordered
     *  values = (MyInfo[])repository.getValues( MyInfo.class, my_info_name, -1, is.sorted.ByTime );
     *  // Get 10 history values with the largest tags
     *  values = (MyInfo[])repository.getValues( MyInfo.class, my_info_name, 10, is.sorted.ByTag );
     * </pre>
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InfoNotFoundException information object with the <tt>name</tt> name does not exist
     * @exception is.InfoNotCompatibleException information object with the <tt>name</tt> name has not the same type as the <tt>class</tt> type
     */
    public <T extends Info> T[] getValues ( Class<T> cls, String name, int how_many, is.sorted order ) 
                                throws  is.RepositoryNotFoundException, 
                                        is.InfoNotFoundException,
                                        is.InfoNotCompatibleException
    {
	T[]  information = null;

	is.info_history info = history( name, how_many, order );
	    
	information = (T[])java.lang.reflect.Array.newInstance(cls,info.values.length);
	for ( int i = 0; i < info.values.length; i++ )
	{
	    try
	    {
		information[i] = cls.getConstructor( ).newInstance( );
	    }
	    catch( Exception e )
	    {
		System.err.println( "Impossible happens."
				    + "May be your information class does not have default constructor?\n" 
				    + e );
	    	continue;
	    }
	    information[i].update(name, info.values[i]);
	}

	return information;	
    }
    
    void subscribe( String server_name, 
	    	    Criteria criteria,
	    	    Listener receiver ) throws 	is.RepositoryNotFoundException,
	    	    					is.InvalidCriteriaException,
	    	    					is.AlreadySubscribedException
    {		
	if ( subscriptions.contains( server_name + criteria.toString() ) ) {
            throw new is.AlreadySubscribedException( server_name, criteria );
        }
                
	Callback callback = receiver instanceof InfoReceiver
        		? new InfoCallback(server_name, receiver)
                        : new EventCallback(partition, server_name, receiver);
	
        SubscriptionManager.addSubscription(partition, server_name, criteria, callback);
        subscriptions.put( server_name + criteria.toString(), callback );
    }
    
    /**
     * Subscribes for the changes in the IS repository.
     * Subscriber will be notified about changes of the information objects whose names match the <tt>pattern</tt> 
     * regular expression.
     * @param server_name name of the IS server to subscribe for
     * @param pattern regular expression
     * @param receiver an object that implements the InfoListener interface
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InvalidCriteriaException the given pattern is not a valid regular expression
     * @exception is.AlreadySubscribedException the same subscription has alredy been made
     * @see EventListener
     * @deprecated Exists for compatibility with the previous IS versions.
     */
    public void subscribe( String server_name, 
	    		   java.util.regex.Pattern pattern, 
	    		   InfoReceiver receiver ) throws is.RepositoryNotFoundException,
							  is.InvalidCriteriaException,
                                                        is.AlreadySubscribedException
    {		
	subscribe( server_name, new Criteria( pattern ), (Listener)receiver );
    }
    
    
    /**
     * Subscribes for the changes in the IS repository.
     * Subscriber will be notified about changes of the information objects whose names match the <tt>pattern</tt> 
     * regular expression.
     * @param server_name name of the IS server to subscribe for
     * @param pattern regular expression
     * @param receiver an object that implements the EventListener interface
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InvalidCriteriaException the given pattern is not a valid regular expression
     * @exception is.AlreadySubscribedException the same subscription has alredy been made
     * @see EventListener
     * @deprecated Exists for compatibility with the previous IS versions.
     */
    public void subscribe( String server_name, 
	    		   java.util.regex.Pattern pattern, 
	    		   EventReceiver receiver ) throws is.RepositoryNotFoundException,
			    				   is.InvalidCriteriaException,
                                                           is.AlreadySubscribedException
    {		
	subscribe( server_name, new Criteria( pattern ), (Listener)receiver );
    }

    void subscribe(String info_name, Listener receiver)
	    				throws	is.RepositoryNotFoundException, 
	    					is.AlreadySubscribedException
    {
	if (subscriptions.contains(info_name)) {
	    throw new is.AlreadySubscribedException(info_name);
	}

	Callback callback = receiver instanceof InfoReceiver
			? new InfoCallback(getServerName(info_name), receiver)
			: new EventCallback(partition, getServerName(info_name), receiver);

	SubscriptionManager.addSubscription(partition, info_name, callback);
	subscriptions.put(info_name, callback);
    }
    
    /**
     * Subscribes for the changes in the IS repository.
     * Subscriber will be notified about 
     * changes of the infromation with the <tt>info_name</tt> name. Infomation value will be passed
     * together with the notification. Consider using subscribe with EventListener interface
     * if you don't always need the infoamtion value as part of the subscription.
     * @param info_name name of the information object to subscribe for
     * @param receiver an object that implements the InfoListener interface
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.AlreadySubscribedException the same subscription has alredy been made
     * @see EventListener
     * @see InfoListener
     */
    public void subscribe( String info_name, 
	    		   InfoReceiver receiver ) throws is.RepositoryNotFoundException,
                                                        is.AlreadySubscribedException
    {		
	subscribe ( info_name, (Listener)receiver );
    }
    
    /**
     * Subscribes for the changes in the IS repository.
     * Subscriber will be notified about 
     * changes of the infromation with the <tt>info_name</tt> name. Infomation value will not be passed
     * as part of this notification. This makes this kind of subscription more efficient especially for the 
     * large information objects.
     * @param info_name name of the information object to subscribe for
     * @param receiver an object that implements the EventListener interface
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.AlreadySubscribedException the same subscription has alredy been made
     * @see EventListener
     */
    public void subscribe( String info_name, 
	    		   EventReceiver receiver ) throws 	is.RepositoryNotFoundException,
                                                        	is.AlreadySubscribedException
    {		
	subscribe ( info_name, (Listener)receiver );
    }
    
    /**
     * Subscribes for the changes in the IS repository.
     * Subscriber will be notified about 
     * changes of the infromation wich satisfies the <tt>criteria</tt> criteria. Infomation value will be passed
     * together with the notification. Consider using subscribe with EventListener interface
     * if you don't always need the infoamtion value as part of the subscription.
     * @param server_name name of the information server to subscribe for
     * @param criteria subscription criteria
     * @param receiver an object that implements the InfoListener interface
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InvalidCriteriaException the given pattern is not a valid regular expression
     * @exception is.AlreadySubscribedException the same subscription has alredy been made
     * @see Criteria
     * @see EventListener
     * @see InfoListener
     */
    public void subscribe( String server_name, 
	     		   Criteria criteria,
	    		   InfoReceiver receiver ) throws is.RepositoryNotFoundException,
			    				is.InvalidCriteriaException,
                                                        is.AlreadySubscribedException
    {		
	subscribe ( server_name, criteria, (Listener)receiver );
    }
    
    /**
     * Subscribes for the changes in the IS repository.
     * Subscriber will be notified about 
     * changes of the infromation wich satisfies the <tt>criteria</tt> criteria. Infomation value will not be passed
     * as part of this notification. This makes this kind of subscription more efficient especially for the 
     * large information objects.
     * @param server_name name of the information server to subscribe for
     * @param criteria subscription criteria
     * @param receiver an object that implements the EventListener interface
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.InvalidCriteriaException the given criteria contains invalid regular expression
     * @exception is.AlreadySubscribedException the same subscription has alredy been made
     * @see EventListener
     */
    public void subscribe( String server_name, 
	     		   Criteria criteria,
	    		   EventReceiver receiver ) throws is.RepositoryNotFoundException,
			    				   is.InvalidCriteriaException,
                                                        is.AlreadySubscribedException
    {		
	subscribe ( server_name, criteria, (Listener)receiver );
    }
    
    /**
     * Removes the subscription which has been done before.
     * @param server_name name of the IS server from which to remove the subscription
     * @param criteria criteria, which has been used for this subscription
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.SubscriptionNotFoundException the given subscription does not exist
     */
    public void unsubscribe( String server_name, 
			     Criteria criteria ) throws	is.RepositoryNotFoundException,
							is.SubscriptionNotFoundException
    {	
	Callback callback = subscriptions.remove( server_name + criteria.toString() );
	
	if ( callback == null )
	    throw new is.SubscriptionNotFoundException(server_name, criteria);
	
	SubscriptionManager.removeSubscription(partition, server_name, criteria, callback);
    }

    /**
     * Removes the subscription which has been done before.
     * @param info_name name of the IS information from which to remove the subscription
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.SubscriptionNotFoundException 
     */
    public void unsubscribe( String info_name ) throws	is.RepositoryNotFoundException,
							is.SubscriptionNotFoundException
    {		
	Callback callback = subscriptions.remove( info_name );
	
	if ( callback == null )
	    throw new is.SubscriptionNotFoundException(info_name);
	
	SubscriptionManager.removeSubscription(partition, info_name, callback);
    }
    
    /**
     * Removes the subscription which has been done before.
     * @param server_name name of the IS server from which to remove the subscription
     * @param pattern regular expression which was used for the corresponding subscription
     * @exception is.RepositoryNotFoundException corresponding IS server is not running
     * @exception is.SubscriptionNotFoundException 
     * @deprecated Exists for compatibility with the previous IS versions.
     */
    public void unsubscribe( String server_name, 
	     		     java.util.regex.Pattern pattern ) throws is.RepositoryNotFoundException,
			    			     is.SubscriptionNotFoundException
    {	
	unsubscribe( server_name, new Criteria( pattern ) );
    }
        
    private void update( String name, Info ii, boolean keep_history, boolean use_tag, int tag )
    					throws	is.RepositoryNotFoundException,
						is.InfoNotFoundException,
						is.InfoNotCompatibleException
    {
	ii.setTag( tag );
        is.InfoOutStream out = new is.InfoOutStream( );
	ii.publishGuts( out );
	is.value value = new is.value( 
	        new is.attributes(ii.getTag(), ii.getTime().getTimeMicro(), ii.getAddress(), ii.flatAnnotations()), out.getData() );
	is.info info = new is.info( getObjectName( name ), ii.getType().getType(), value );
	
	try
	{
	    is.repository rep = Repository.resolve( partition, name );

	    rep.update( info, keep_history, use_tag );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.NotCompatible ex )
	{
	    throw new is.InfoNotCompatibleException(name);
	}
	catch ( is.NotFound ex )
	{
	    throw new is.InfoNotFoundException(name);
	}
    }
        
    private void checkin( String name, Info ii, boolean keep_history, boolean use_tag, int tag )
    					throws	is.RepositoryNotFoundException,
						is.InfoNotCompatibleException
    {
	ii.setTag( tag );
        is.InfoOutStream out = new is.InfoOutStream( );
	ii.publishGuts( out );
	is.value value = new is.value( 
	        new is.attributes(ii.getTag(), ii.getTime().getTimeMicro(), ii.getAddress(), ii.flatAnnotations()), out.getData() );
	is.info info = new is.info( getObjectName( name ), ii.getType().getType(), value );
	
	try
	{
	    is.repository rep = Repository.resolve( partition, name );

            rep.checkin( info, keep_history, use_tag );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.NotCompatible ex )
	{
	    throw new is.InfoNotCompatibleException(name);
	}
    }
        
    private is.info_history history( String name, int how_many, is.sorted order ) 
    	throws is.RepositoryNotFoundException, is.InfoNotFoundException
    {	
	is.info_historyHolder info = new is.info_historyHolder();

	try
	{
	    is.repository rep = Repository.resolve( partition, name );
	    rep.get_last_values( getObjectName( name ), how_many, order, info );
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, getServerName(name));
	}
	catch ( is.NotFound ex )
	{
	    throw new is.InfoNotFoundException(name);
	}
	
	return info.value;
    }        
    
    static String getServerName( String name )
    {
	int pos = name.indexOf( "." );
	if ( pos != -1 )
	{
	    return name.substring( 0, pos );
	}
        else
        {
            return name;
        }
    }    
    
    static String getObjectName( String name )
    {
	int pos = name.indexOf( "." );
	if ( pos != -1 )
	{
	    return name.substring( pos + 1 );
	}
        else
        {
            throw new is.InvalidNameException(name);
        }
    }    
    
    static is.repository resolve( Partition p, String name )
    	throws is.RepositoryNotFoundException
    {
	try {
	    return p.lookup( is.repository.class, getServerName( name ) );	
	}
	catch(Exception e) {
	    throw new is.RepositoryNotFoundException(p, name);
	}
    }    
}
