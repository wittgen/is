package is;

import ipc.Partition;

/**
 * An abstract adapter class for receiving callbacks from the IS repository. The
 * methods in this class are empty. This class exists as convenience for
 * creating info listener objects. Info events let you track when an information
 * is created, updated or deleted from the IS repository. Extend this class to
 * create an InfoEvent listener and override the methods for the events of
 * interest. (If you implement the InfoListener interface, you have to define
 * all of the methods in it. This abstract class defines empty methods for them
 * all, so you can only have to define methods for events you care about.)
 * Create a callback object using your extended class and then register it with
 * an IS repository using the subscribe method of this class. When an
 * information is created, updated or deleted, the relevant method in your
 * object is invoked and the InfoEvent is passed to it.
 * 
 * @author Sergei Kolos
 * @see Listener
 * @see InfoEvent
 */
public abstract class Receiver
{
    private final Repository repository;
    private final String server_name;
    private final String info_name;
    private final Criteria criteria;
    private final Listener listener;

    private Receiver(Partition partition, String server_name, String info_name,
	    Criteria criteria, boolean receive_value)
    {
	this.repository = new Repository(partition);
	this.criteria = criteria;
	this.server_name = server_name;
	this.info_name = info_name;
        this.listener = createListener(receive_value);
    }

    /**
     * Creates InfoEvent listener for the <tt>server_name</tt> IS server in the
     * <tt>partition</tt> partition. Subscriber will be notified about changes
     * of the information objects whose names match the <tt>pattern</tt> regular
     * expression.
     * 
     * @param partition
     *            partition which IS server belongs to
     * @param server_name
     *            IS server name to subscribe
     * @param pattern
     *            regular expression, that is used for subscription.
     * @param receive_value
     *            If set to true, then subscriber will always receive the value
     *            of the changed information. If false, then the value is not sent
     *		  and subscriber will receive only timestamp of the corresponding event.
     */
    protected Receiver(Partition partition, String server_name,
	    java.util.regex.Pattern pattern, boolean receive_value)
    {
	this(partition, server_name, "", new Criteria(pattern),
		receive_value);
    }

    /**
     * Creates InfoEvent listener for the <tt>server_name</tt> IS server in the
     * <tt>partition</tt> partition. Subscriber will be notified about changes
     * of the information objects whose names match the <tt>pattern</tt> regular
     * expression. This constructor creates callback, which will receive
     * information value from the IS repository.
     * 
     * @param partition
     *            partition which IS server belongs to
     * @param server_name
     *            IS server name to subscribe
     * @param pattern
     *            regular expression, that is used for subscription.
     */
    protected Receiver(Partition partition, String server_name,
	    java.util.regex.Pattern pattern)
    {
	this(partition, server_name, "", new Criteria(pattern), true);
    }

    /**
     * Creates InfoEvent listener for the <tt>info_name</tt> IS information in the
     * <tt>partition</tt> partition. Subscriber will be notified about changes
     * of this information object.
     * 
     * @param partition
     *            partition which IS server belongs to
     * @param info_name
     *            name of the information object to subscribe to
     * @param receive_value
     *            If set to true, then subscriber will always receive the value
     *            of the changed information. If false, then the value is not sent
     *		  and subscriber will receive only timestamp of the corresponding event.
     */
    protected Receiver(Partition partition, String info_name, boolean receive_value)
    {
	this(partition, Repository.getServerName(info_name), 
        	Repository.getObjectName(info_name), null, receive_value);
    }

    /**
     * Creates InfoEvent listener for the <tt>info_name</tt> IS information in the
     * <tt>partition</tt> partition. Subscriber will be notified about changes
     * of this information object. This subscriber will always receive full value 
     * of the changed information object.
     * 
     * @param partition
     *            partition which IS server belongs to
     * @param info_name
     *            name of the information object to subscribe to
     * @param receive_value
     *            If set to true, then subscriber will always receive the value
     *            of the changed information. If false, then the value is not sent
     *		  and subscriber will receive only timestamp of the corresponding event.
     */
    protected Receiver(Partition partition, String info_name)
    {
	this(partition, Repository.getServerName(info_name), 
        	Repository.getObjectName(info_name), null, true);
    }

    /**
     * Creates InfoEvent listener for the <tt>server_name</tt> IS server in the
     * <tt>partition</tt> partition. Subscriber will be notified about changes
     * of the information objects whose names match the <tt>pattern</tt> regular
     * expression.
     * 
     * @param partition
     *            partition which IS server belongs to
     * @param server_name
     *            IS server name to listen information events on
     * @param criteria
     *            notification will be done only for information objects, which
     *            match thios criteria
     * @param receive_value
     *            If set to true, then subscriber will always receive the value
     *            of the changed information. If false, then the value is not sent
     *		  and subscriber will receive only timestamp of the corresponding event.
     * @see Criteria
     */
    protected Receiver(Partition partition, String server_name, Criteria criteria,
	    boolean receive_value)
    {
	this(partition, server_name, "", criteria, receive_value);
    }

    /**
     * Creates InfoEvent listener for the <tt>server_name</tt> IS server in the
     * <tt>partition</tt> partition. Subscriber will be notified about changes
     * of the information objects whose names match the <tt>pattern</tt> regular
     * expression. This subscriber will always receive full value of the changed 
     * information object.
     * 
     * @param partition
     *            partition which IS server belongs to
     * @param server_name
     *            IS server name to listen information events on
     * @param criteria
     *            notification will be done only for information objects, which
     *            match thios criteria
     * @see Criteria
     */
    protected Receiver(Partition partition, String server_name, Criteria criteria)
    {
	this(partition, server_name, "", criteria, true);
    }

    Listener createListener(boolean receive_value)
    {
        if (receive_value) {
	    return new InfoListener() {
		    public void infoCreated(InfoEvent e) {
			Receiver.this.infoCreated(e);
		    }
		    public void infoUpdated(InfoEvent e) {
			Receiver.this.infoUpdated(e);
		    }
		    public void infoDeleted(InfoEvent e) {
			Receiver.this.infoDeleted(e);
		    }
	    };
	}
	else {
	    return new EventListener() {
		    public void infoCreated(InfoEvent e) {
			Receiver.this.infoCreated(e);
		    }
		    public void infoUpdated(InfoEvent e) {
			Receiver.this.infoUpdated(e);
		    }
		    public void infoDeleted(InfoEvent e) {
			Receiver.this.infoDeleted(e);
		    }
	    };
	}
    }
    
    /**
     * Returns partition which was used at the construction of this object.
     * 
     * @return partition
     */
    public ipc.Partition getPartition()
    {
	return repository.getPartition();
    }

    /**
     * Creates new subscription in the IS server whose name has been specified
     * at construction time.
     * 
     * @exception is.RepositoryNotFoundException
     *                corresponding IS server is not running
     * @exception is.InvalidCriteriaException
     *                information name pattern provided for the constructor is
     *                not a valid regular expression
     */
    public void subscribe() throws 	is.RepositoryNotFoundException,
	    				is.InvalidCriteriaException,
                                        is.AlreadySubscribedException
    {
        if (criteria != null)
	    repository.subscribe(server_name, criteria, listener);
	else
	    repository.subscribe(server_name + '.' + info_name, listener);
    }

    /**
     * Removes the subscription which has been created by the subscribe method
     * of this class.
     * 
     * @exception is.RepositoryNotFoundException
     *                corresponding IS server is not running
     * @exception is.SubscriptionNotFoundException
     */
    public void unsubscribe() throws 	is.RepositoryNotFoundException,
	    				is.SubscriptionNotFoundException
    {
	if (criteria != null)
	    repository.unsubscribe(server_name, criteria);
	else
	    repository.unsubscribe(server_name + '.' + info_name);
    }

    /**
     * Invoked when information has been created.
     * 
     * @param e	Contains information about the event which caused this callback
     */
    public void infoCreated(InfoEvent e)
    {
    }

    /**
     * Invoked when information has been updated.
     * 
     * @param e	Contains information about the event which caused this callback
     */
    public void infoUpdated(InfoEvent e)
    {
    }

    /**
     * Invoked when information has been deleted.
     * 
     * @param e	Contains information about the event which caused this callback
     */
    public void infoDeleted(InfoEvent e)
    {
    }
}
