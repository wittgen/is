package is;

import java.util.StringTokenizer;
import java.util.zip.Inflater;
import org.jacorb.orb.CDRInputStream;

/**
 * InfoInStream implements the Istream interface.
 * @author Sergei Kolos
 */
class InfoInStream implements Istream
{
    private CDRInputStream	in;
    
    InfoInStream( byte[] data )
    {
	in = new org.jacorb.orb.CDRInputStream( ipc.Core.getORB(), data );
	boolean le = in.read_boolean();
	in.setLittleEndian( le );
	boolean compressed = in.read_boolean();
	int uncompressed_size = in.read_long( );
        if ( compressed )
        {
            try
            {
                int header_size = in.get_pos();
		byte[] uncompressed_data = new byte[uncompressed_size];
		Inflater zin = new Inflater();
                zin.setInput( data, header_size, data.length - header_size );
                zin.inflate( uncompressed_data );

		in = new org.jacorb.orb.CDRInputStream( ipc.Core.getORB(), uncompressed_data );
		in.setLittleEndian( le );
            }
            catch( java.util.zip.DataFormatException e )
            {
            	System.err.println( "ERROR in [InfoInStream::InfoInStream] : " + e );
            }
        }
	in.mark( Integer.MAX_VALUE );
    }
    
    void reset()
    {
	try {
	    in.reset();
	}
	catch( java.io.IOException e )
	{
	    System.err.println( "Impossible happens: " + e );
	}
    }
    
    /**
     * Reads a single enumeration value from this stream.
     * @return enumeration value read from this stream
     */
    public <E extends Enum<E>> E getEnum( Class<E> cl )
    {
    	int ordinal = getInt();
        E[] values = cl.getEnumConstants();
        return ( values != null && values.length > ordinal ? values[ordinal] : null );
    }
    
     /**
     * Reads a single boolean value from this stream.
     * @return boolean value read from this stream
     */
    public boolean getBoolean()
    {
	return in.read_boolean();
    }

   /**
     * Reads a single byte value from this stream.
     * @return byte value read from this stream
     */
    public byte getByte()
    {
	return in.read_octet( );
    }
    
    /**
     * Reads a single short value from this stream.
     * @return short value read from this stream
     */
    public short getShort( )
    {
	return in.read_short( );
    }
    
    /**
     * Reads a single int value from this stream.
     * @return int value read from this stream
     */
    public int getInt( )
    {
	return in.read_long( );
    }

    /**
     * Reads a single long value from this stream.
     * @return int value read from this stream
     */
    public long getLong( )
    {
        return in.read_longlong( );
    }
    
    /**
     * Reads a single float value from this stream.
     * @return float value read from this stream
     */
    public float getFloat()
    {
	return in.read_float( );
    }
    
    /**
     * Reads a single double value from this stream.
     * @return double value read from this stream
     */
    public double getDouble()
    {
	return in.read_double( );
    }
    
    /**
     * Reads a single string value from this stream.
     * @return String value read from this stream
     */
    public String getString()
    {
	return in.read_string( );
    }
    
    /**
     * Reads a single date value from this stream.
     * @return Date value read from this stream
     */
    public Date getDate()
    {
	return new Date( in.read_ulong() * 1000000l );
    }

    /**
     * Reads a single time value from this stream.
     * @return Time value read from this stream
     */
    public Time getTime()
    {
	return new Time( in.read_longlong() );
    }

    /**
     * Reads a single Information object from this stream.
     * @param cl Class of the Information object
     * @return Time value read form this stream
     */
    public <T extends Info> T getInfo( Class<T> cl )
    {
        try
        {
            T info = (T)cl.getConstructor().newInstance();
            info.refreshGuts( this );
            return info;
        }
        catch (java.lang.Exception e )
        {
            return null;
        }
    }

    /**
     * Reads an array of enumeration values from this stream.
     * @return array of enumeration values from this stream
     */
    public <E extends Enum<E>> E[] getEnumArray( Class<E> cl )
    {
	E[] array = (E[])java.lang.reflect.Array.newInstance( cl, in.read_long() );
	for ( int i = 0; i < array.length; i++ )
	    array[i] = getEnum( cl );
	return array;
    }
    
    /**
     * Reads an array of boolean values from this stream.
     * @return array of booleans read from this stream
     */
    public boolean[] getBooleanArray( )
    {
	return org.omg.CORBA.BooleanSeqHelper.read( in );
    }
    
    /**
     * Reads an array of byte values from this stream.
     * @return array of bytes read from this stream
     */
    public byte[] getByteArray( )
    {
	return org.omg.CORBA.OctetSeqHelper.read( in );
    }
    
    /**
     * Reads an array of short values from this stream.
     * @return array of shorts read from this stream
     */
    public short[] getShortArray( )
    {
	return org.omg.CORBA.ShortSeqHelper.read( in );
    }
    
    /**
     * Reads an array of int values from this stream.
     * @return array of ints read from this stream
     */
    public int[] getIntArray( )
    {
	return org.omg.CORBA.LongSeqHelper.read( in );
    }

    /**
     * Reads an array of long values from this stream.
     * @return array of ints read from this stream
     */
    public long[] getLongArray( )
    {
        return org.omg.CORBA.LongLongSeqHelper.read( in );
    }

    /**
     * Reads an array of float values from this stream.
     * @return array of floats read from this stream
     */
    public float[] getFloatArray( )
    {
	return org.omg.CORBA.FloatSeqHelper.read( in );
    }
    
    /**
     * Reads an array of double values from this stream.
     * @return array of doubles read from this stream
     */
    public double[] getDoubleArray( )
    {
	return org.omg.CORBA.DoubleSeqHelper.read( in );
    }
    
    /**
     * Reads an array of string values from this stream.
     * @return array of Strings read from this stream
     */
    public String[] getStringArray( )
    {
	String[] array = new String[ in.read_long() ];
	for ( int i = 0; i < array.length; i++ )
	    array[i] = getString();
	return array;
    }
    
    /**
     * Reads an array of date values from this stream.
     * @return array of Dates read from this stream
     */
    public Date[] getDateArray( )
    {
	Date[] array = new Date[ in.read_long() ];
	for ( int i = 0; i < array.length; i++ )
	    array[i] = getDate();
	return array;
    }
    
    /**
     * Reads an array of time values from this stream.
     * @return array of Times read from this stream
     */
    public Time[] getTimeArray( )
    {
	Time[] array = new Time[ in.read_long() ];
	for ( int i = 0; i < array.length; i++ )
	    array[i] = getTime();
	return array;
    }
    
    /**
     * Reads an array of Information objects from this stream.
     * @param cl Class of the Information object
     * @return array of Times read form this stream
     */
    public <T extends Info> T[] getInfoArray( Class<T> cl )
    {
	T[] array = (T[])java.lang.reflect.Array.newInstance( cl, in.read_long() );
	for ( int i = 0; i < array.length; i++ )
	    array[i] = getInfo( cl );
	return array;
    }

    /**
     * Reads a vector of Information objects from this stream.
     * @param cl Class of the Information object
     * @return vecotr of Info objects read from this stream
     */
    public <T extends Info> java.util.Vector<T> getInfoVector( Class<T> cl )
    {
        int s = in.read_long();
        java.util.Vector<T> v = new java.util.Vector<T>( s );
        for ( int i = 0; i < s; i++ )
        {
            v.add( getInfo( cl ) );
        }
        return v;
    }
}
