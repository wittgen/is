package is;

import java.util.Vector;
import java.util.Hashtable;

/**
 * This class represents the type of the IS information. It can be used to get the type name and 
 * also for the type comparison. The equality operation defined in this class is based on
 * comparison of the information types structure. For example IS types for the following
 * two classes will be identical because these classes define the attributes of the same type in the same order.
 * <pre>
 * class Info1 extends Info
 * {
 *     Info1()
 *     {
 *         super( "Info1" );
 *     }
 *     ...
 *     int attr11;
 *     float attr12;
 * }
 * 
 * class Info2 extends NamedInfo
 * {
 *     Info2( Partition p, String name )
 *     {
 *         super( p, name, "Info2" );
 *     }
 *     ...
 *     int attr21;
 *     float attr22;
 * }
 * </pre>
 * @author Sergei Kolos
 */
public class Type
{        
    static final String UnknownName = new String("Unknown");
    static final Type Null = new Type("Null");
    static final Hashtable<Byte,String>	typenames;    
    
    public static final byte BOOLEAN = 1;
    public static final byte S8 = 2;
    public static final byte U8 = 3;
    public static final byte S16 = 4;
    public static final byte U16 = 5;
    public static final byte S32 = 6;
    public static final byte U32 = 7;
    public static final byte S64 = 8;
    public static final byte U64 = 9;
    public static final byte FLOAT = 10;
    public static final byte DOUBLE = 11;
    public static final byte DATE = 12;
    public static final byte TIME = 13;
    public static final byte STRING = 14;
    public static final byte INFO = 15;
    
    static final byte ArrayMask = 64;
    
    static 
    {
	typenames = new Hashtable<Byte,String>();
	typenames.put( Type.BOOLEAN, "bool" );
	typenames.put( Type.S8, "s8" );
	typenames.put( Type.S16, "s16" );
	typenames.put( Type.S32, "s32" );
	typenames.put( Type.S64, "s64" );
	typenames.put( Type.U8, "u8" );
	typenames.put( Type.U16, "u16" );
	typenames.put( Type.U32, "u32" );
	typenames.put( Type.U64, "u64" );
	typenames.put( Type.FLOAT, "float" );
	typenames.put( Type.DOUBLE, "double" );
	typenames.put( Type.STRING, "string" );
	typenames.put( Type.TIME, "time" );
	typenames.put( Type.DATE, "date" );
    }
    
    private class Core implements Ostream
    {
	private java.io.ByteArrayOutputStream bout = new java.io.ByteArrayOutputStream();
        private java.lang.StringBuilder sout = new java.lang.StringBuilder();
	private Vector<is.type>	nested_types = new Vector<is.type>();
        
	byte[] getData( ) {
	    return bout.toByteArray();
	}

        String getID( ) {
            return sout.toString();
        }

	is.type[] getNestedTypes( ) {
	    return nested_types.toArray( new is.type[0] );
	}
	
	public <E extends Enum<E>> Ostream put( java.lang.Enum<E> v ) {
	    bout.write( S32 );
            sout.append( (char)S32 );
	    return this;
	}
	public Ostream put( boolean v ) {
	    bout.write( BOOLEAN );
            sout.append( (char)BOOLEAN );
	    return this;
	}
	public Ostream put( byte v, boolean sign ) {
	    bout.write( sign ? S8 : U8 );
            sout.append( (char)(sign ? S8 : U8) );
	    return this;
	}	
	public Ostream put( short v, boolean sign ) {
	    bout.write( sign ? S16 : U16 );
            sout.append( (char)(sign ? S16 : U16) );
	    return this;
	}
	public Ostream put( int v, boolean sign ) {
	    bout.write( sign ? S32 : U32 );
            sout.append( (char)(sign ? S32 : U32) );
	    return this;
	}
        public Ostream put( long v, boolean sign ) {
            bout.write( sign ? S64 : U64 );
            sout.append( (char)(sign ? S64 : U64) );
            return this;
        }
	public Ostream put( float v ) {
	    bout.write( FLOAT );
            sout.append( (char)FLOAT );
	    return this;
	}
	public Ostream put( double v ) {
	    bout.write( DOUBLE );
            sout.append( (char)DOUBLE );
	    return this;
	}
	public Ostream put( String v ) {
	    bout.write( STRING );
            sout.append( (char)STRING );
	    return this;
	}	
	public Ostream put( Date v ) {
	    bout.write( DATE );
            sout.append( (char)DATE );
	    return this;
	}
	public Ostream put( Time v ) {
	    bout.write( TIME );
            sout.append( (char)TIME );
	    return this;
	}	
	public Ostream put( Info v, Type type ) {
	    bout.write( INFO );
            sout.append( (char)INFO );
            sout.append( "{" + type.getType().id + "}" );
            nested_types.add( type.getType() );
	    return this;
	}	
	public <E extends Enum<E>> Ostream put( java.lang.Enum<E>[] v ) {
	    bout.write( S32 | ArrayMask );
            sout.append( (char)(S32 | ArrayMask) );
	    return this;
	}
	public Ostream put( boolean[] v ) {
	    bout.write( BOOLEAN | ArrayMask );
            sout.append( (char)(BOOLEAN | ArrayMask) );
	    return this;
	}
	public Ostream put( byte[] v, boolean sign ) {
	    bout.write( ( sign ? S8 : U8 ) | ArrayMask );
            sout.append( (char)(( sign ? S8 : U8 ) | ArrayMask) );
	    return this;
	}	
	public Ostream put( short[] v, boolean sign ) {
	    bout.write( ( sign ? S16 : U16 ) | ArrayMask );
            sout.append( (char)(( sign ? S16 : U16 ) | ArrayMask) );
	    return this;
	}
	public Ostream put( int[] v, boolean sign ) {
	    bout.write( ( sign ? S32 : U32 ) | ArrayMask );
            sout.append( (char)(( sign ? S32 : U32 ) | ArrayMask) );
	    return this;
	}
        public Ostream put( long[] v, boolean sign ) {
            bout.write( ( sign ? S64 : U64 ) | ArrayMask );
            sout.append( (char)(( sign ? S64 : U64 ) | ArrayMask) );
            return this;
        }
	public Ostream put( float[] v ) {
	    bout.write( FLOAT | ArrayMask );
            sout.append( (char)(FLOAT | ArrayMask) );
	    return this;
	}
	public Ostream put( double[] v ) {
	    bout.write( DOUBLE | ArrayMask );
            sout.append( (char)(DOUBLE | ArrayMask) );
	    return this;
	}
	public Ostream put( String[] v ) {
	    bout.write( STRING | ArrayMask );
            sout.append( (char)(STRING | ArrayMask) );
	    return this;
	}	
	public Ostream put( Date[] v ) {
	    bout.write( DATE | ArrayMask );
            sout.append( (char)(DATE | ArrayMask) );
	    return this;
	}
	public Ostream put( Time[] v ) {
	    bout.write( TIME | ArrayMask );
            sout.append( (char)(TIME | ArrayMask) );
	    return this;
	}	
	public <T extends Info> Ostream put( T[] v, Type type ) {
	    bout.write( INFO | ArrayMask );
            sout.append( (char)(INFO | ArrayMask) );
            sout.append( "{" + type.getType().id + "}" );
            nested_types.add( type.getType() );
	    return this;
	}
        public <T extends Info> Ostream put( java.util.Vector<T> vector, Type type )	{
	    bout.write( INFO | ArrayMask );
            sout.append( (char)(INFO | ArrayMask) );
            sout.append( "{" + type.getType().id + "}" );
            nested_types.add( type.getType() );
	    return this;
        }
    }
    
    private is.type type;
    final boolean inverted;
    final boolean weak;

    private Type( )
    {
        this.type = new is.type( new String(), UnknownName, new byte[0], new is.type[0] );
        this.inverted = false;
        this.weak = false;
    }

    private Type( Type t, boolean inverted, boolean weak )
    {
        this.type = t.type;
        this.inverted = inverted;
        this.weak = weak;
    }

    private Type( String name )
    {
        this.type = new is.type( new String(), name, new byte[0], new is.type[0] );
        this.inverted = false;
        this.weak = false;
    }

    Type( is.type type )
    {
        this.type = type;
        this.inverted = false;
        this.weak = false;
    }

    Type( String name, Info info )
    {
	Core core = new Core();
	info.publishGuts( core );
        this.type = new is.type( core.getID(), name, core.getData(), core.getNestedTypes() );
        this.inverted = false;
        this.weak = false;
    }
    
    is.type getType()
    {
    	return type;
    }
    
    /**
     * The return value can be used to designate objects of this type as weel as all sub-types
     * os this type for the IS repository subscriptions.
     * @return Type object which can be used for extended subscriptions.
     * @see Criteria
     */
    public Type asSuperType( )
    {
    	return new Type( this, false, true );
    }
    
    /**
     * The return value can be used to designate objects of any other type but this one
     * for the IS repository subscriptions.
     * @return Type object which can be used for extended subscriptions.
     * @see Criteria
     */
    public Type asNotType( )
    {
    	return new Type( this, true, false );
    }
    
    /**
     * The return value can be used to designate objects of any type which is neither this one nor
     * any sub-type of this one for the IS repository subscriptions.
     * for the IS repository subscriptions.
     * @return Type object which can be used for extended subscriptions.
     * @see Criteria
     */
    public Type asNotSuperType( )
    {
    	return new Type( this, true, true );
    }
    
    /**
     * Checks whether the attribute at the given position is array or a single value.
     * @param index attribute position, first attributes has position 0
     * @return true if attributes is an array, false otherwise.
     */
    public boolean isAttributeArray( int index )
    {
    	return ( (type.code[index] & ArrayMask) != 0 );
    }
    
    /**
     * Returns the type code of the attribute at the specified position in this information object.
     * @param index attribute position, first attributes has position 0
     * @return type code of the attribute at the specified position
     */
    public byte getAttributeType( int index )
    {
    	return ( (byte)(type.code[index] & (~ArrayMask)) );
    }
    
    /**
     * Returns the type of the Infomation Object attribute at the specified position in this information object.
     * @param index attribute position, first attributes has position 0. The attributes at this position must has
     *		the INFO type code.
     * @return type of the attribute at the specified position
     */
    public Type getAttributeInfoType( int index )
    {
	int info_number = 0;
	for ( int i = 0; i < index; i++ )
	    if ( getAttributeType( i ) == INFO ) info_number++;
	if ( getAttributeType( index ) == INFO
	    && type.nested_types.length > info_number )
	    return new Type(type.nested_types[info_number]);
	else
	    return new Type();
    }
    
    /**
     * Returns the type name of the attribute at the specified position in this information object.
     * @param index attribute position, first attributes has position 0
     * @return type name of the attribute at the specified position
     */
    public String getAttributeTypeName ( int index )
    {
	int info_number = 0;
	for ( int i = 0; i < index; i++ )
	    if ( getAttributeType( i ) == INFO ) info_number++;
	if ( getAttributeType( index ) == INFO
	    && type.nested_types.length > info_number )
	    return type.nested_types[info_number].name;
	else
	    return typenames.get( getAttributeType( index ) );
    }
 
    /**
     * Returns the number of attributes for this information object.
     * @return number of attributes
     */
    public int getAttributeCount( )
    {
    	return type.code.length;
    }
    
    /**
     * This constructor is used by generated information classes.
     */
    public <T extends Info> Type( T info )
    {
        this.type = info.getType().type;
        this.inverted = false;
        this.weak = false;
    }
    
    /**
     * Indicates whether this type is "compatible" with the <tt>type</tt> type. "Compatibility" means that
     * both types have the same structure from the IS point of view, i.e. the same number of the IS attributes
     * of the same type, whiah are defined in the same order.
     * @param type the reference type with which to compare
     * @return true if this type is the descendant of the type argument; false otherwise
     */
    public boolean compatibleWith ( is.Type type )
    {
        return this.type.id.equals( type.type.id );
    }

    /**
     * Indicates whether this type is an ascendant of the <tt>type</tt> type. The ascending relationship means
     * that the <tt>type</tt> type must define all the IS attributes, which exists for the current type, in the 
     * same order in which they are defined for the current type. The current type may have any number of extra
     * attributes.
     * @param type the reference type with which to compare
     * @return true if this type is the descendant of the type argument; false otherwise
     */
    public boolean superTypeOf ( is.Type type )
    {
    	return type.subTypeOf( this );
    }

    /**
     * Indicates whether this type is a descendant of the <tt>type</tt> type. The descending relationship means
     * that the current type must define all the IS attributes, which exists for the <tt>type</tt> type, in the 
     * same order in which they are defined for the <tt>type</tt> type. The <tt>type</tt> type may have any number 
     * of extra attributes.
     * @param type the reference type with which to compare
     * @return true if this type is the descendant of the type argument; false otherwise
     */
    public boolean subTypeOf ( is.Type type )
    {
	return this.type.id.startsWith(type.type.id);
    }
    
    /**
     * Indicates whether the <tt>type</tt> type is "equal to" this one. 
     * @param type the reference type with which to compare
     * @return true if this type is the same as the type argument; false otherwise
     */
    public boolean equals( is.Type type )
    {
        if ( !this.type.id.equals( type.type.id ) )
	    return false;
	return this.type.name.equals( type.type.name );
    }
    
    /**
     * Gets the name of the information type. This is the name that is provided as
     * argument for the Info and NamedInfo constructors. If the name was not provided
     * this method returns the "Unknown" literal.
     * @return information type name
     */
    public String getName( )
    {
        return type.name;
    }
}
