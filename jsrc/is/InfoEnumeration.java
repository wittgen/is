package is;

import ipc.Partition;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * The InfoEnumeration enumerates the information objects which are selected
 * from the IS repository according to the arguments of the InfoEnumeration
 * constructor.
 * 
 * @author Sergei Kolos
 */
public class InfoEnumeration extends ArrayList<Info> implements
	java.util.Enumeration<Info>
{
    class Stream extends is.streamPOA
    {
	private ArrayList<Info> information;
	private boolean eof_received = false;

	Stream(ArrayList<Info> i)
	{
	    information = i;
	}

	public void push(is.info_history[] info)
	{
	    for ( int i = 0; i < info.length; ++i )
	    {
	    	information.add(new Info(
	    			InfoEnumeration.this.server_name + "." + info[i].name,
	    			new Type(info[i].type),
	    			info[i].values[0].attr) {});
	    }
	}

	public synchronized void eof()
	{
	    eof_received = true;
	    notify();
	}

	synchronized void waitForCompletion()
	{
	    while (!eof_received) {
		try {
		    wait();
		}
		catch (java.lang.InterruptedException ex) {
		    ;
		}
	    }
	}
    }

    private static final java.util.regex.Pattern defaultRegularExpression = java.util.regex.Pattern
	    .compile(".*");

    private final String server_name;
    private int pos;

    private void deactivate(is.stream stream)
    {
        try {
            ipc.Core.getRootPOA().deactivate_object( ipc.Core.getRootPOA().reference_to_id( stream ) );
        }
        catch( org.omg.PortableServer.POAPackage.ObjectNotActive ex ) {}
        catch( org.omg.PortableServer.POAPackage.WrongAdapter ex ) {}
        catch( org.omg.PortableServer.POAPackage.WrongPolicy ex ) {}
    }

    /**
     * Creates a new information list initialized with the information objects
     * belonging to the <tt>name</tt> IS server in the <tt>partition</tt>
     * partition. Only information objects whose names match the <tt>rexp</tt>
     * regular expression are listed.
     * 
     * @param partition
     *            IPC partition
     * @param server_name
     *            IS server name
     * @param criteria
     *            criteria to select information objects
     * @exception is.InvalidCriteriaException
     *                the given criteria contains invalid regular expression
     */
    public InfoEnumeration(ipc.Partition partition, String server_name,
	    Criteria criteria) throws is.RepositoryNotFoundException,
	    is.InvalidCriteriaException
    {
	this.server_name = server_name;
	try {
	    is.repository r = Repository.resolve(partition, server_name);

	    Stream stream = new Stream(this);
            is.stream corba_ref = stream._this(ipc.Core.getORB());
	    r.create_stream(criteria.criteria, corba_ref, 0, is.sorted.ByTime);
	    stream.waitForCompletion();
            deactivate(corba_ref);
	}
	catch (org.omg.CORBA.SystemException ex) {
	    throw new is.RepositoryNotFoundException(partition, server_name);
	}
	catch (is.InvalidCriteria ex) {
	    throw new is.InvalidCriteriaException(criteria);
	}
    }

    /**
     * Creates a new information list initialized with all the information
     * objects belonging to the <tt>name</tt> IS server in the
     * <tt>partition</tt> partition.
     * 
     * @param partition
     *            IPC partition
     * @param server_name
     *            IS server name
     */
    public InfoEnumeration(ipc.Partition partition, String server_name)
	    throws is.RepositoryNotFoundException, is.InvalidCriteriaException
    {
	this(partition, server_name, new Criteria(defaultRegularExpression));
    }

    /**
     * Creates a new information list initialized with the information objects
     * belonging to the <tt>name</tt> IS server in the <tt>partition</tt>
     * partition. Only information objects whose names match the <tt>rexp</tt>
     * regular expression are listed.
     * 
     * @param partition
     *            IPC partition
     * @param name
     *            IS server name
     * @param pattern
     *            regular expression for the information names
     * @exception is.InvalidCriteriaException
     *                is the rexp is not valid regular expression
     */
    public InfoEnumeration(ipc.Partition partition, String server_name,
	    java.util.regex.Pattern pattern)
	    throws is.RepositoryNotFoundException, is.InvalidCriteriaException
    {
	this(partition, server_name, new Criteria(pattern));
    }

    public String getServerName()
    {
	return server_name;
    }

    /**
     * @return if and only if this enumeration object contains at least one more
     *         element to provide; false otherwise.
     */
    public boolean hasMoreElements()
    {
	return (pos < size());
    }

    /**
     * Returns the next element of this enumeration if this enumeration object
     * has at least one more element to provide.
     * 
     * @return the next element of this enumeration.
     * 
     * @throws NoSuchElementException
     *             if no more elements exist.
     */
    public Info nextElement()
    {
	try {
	    return get(pos++);
	}
	catch (Exception e) {
	    throw new java.util.NoSuchElementException();
	}
    }

    /**
     * Resets the enumeration's current position <i>before</i> the first
     * element, i.e. to the state which this object had just after construction.
     */
    public void reset()
    {
	pos = 0;
    }
}
