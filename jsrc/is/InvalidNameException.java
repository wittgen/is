package is;

/**
 * Thrown to indicate that the IS information name does not
 * have the necessary formatwhich must be "Repository_Name.Information_Name"
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class InvalidNameException extends RuntimeException 
{
    /**
     * Constructs an InvalidNameException with the specified
     * information name.
     *
     * @param name the information name
     */
    public InvalidNameException( String name ) 
    {
	super(
        	"'" + name + 
                "' is not a valid IS name. Infomation name must be of <server name>.<object name> format");
    }
}
