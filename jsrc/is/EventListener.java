package is;

/**
 * The listener interface for receiving "interesting" information events
 * (create, update and delete information) from the IS repository. The class
 * that is interested in receiving information events either implements this
 * interface (and all the methods it contains) or extends the abstract Receiver
 * class (overriding only the methods of interest). The listener object created
 * from that class is then registered with a particular information object(s)
 * using either the subscribe method of the Receiver class (if you chose to
 * extend the Receiver class) or the subscribe method of the Repository class
 * (if you implemented the InfoListener interface). An info event is generated
 * when the information object is created, updated or removed form the IS. When
 * an info event occurs the relevant method in the listener object is invoked,
 * and the InfoEvent is passed to it.
 * 
 * @author Sergei Kolos
 * @see InfoEvent
 * @see Receiver
 * @see Repository
 */

public interface EventListener extends InfoCreatedListener,
	InfoUpdatedListener, InfoDeletedListener, EventReceiver
{
}
