package is;

/**
 * A date class which allows to read dates from IS repository with microseconds precision.
 * @author Sergei Kolos
 * @see Info
 */
public class Date extends java.util.Date
{    
    protected static final java.text.SimpleDateFormat date_formatter 
    	= new java.text.SimpleDateFormat( "yyyy-MMM-dd", java.util.Locale.US );
    
    protected static final java.text.SimpleDateFormat date_formatter_backup 
    	= new java.text.SimpleDateFormat( "yyyy-MM-dd", java.util.Locale.US );
    
    protected long microseconds;
    
    /**
     * Constructs object and initializes it so that it represents the time at which it was constructed 
     */
    public Date( )
    {
        microseconds = super.getTime() * 1000l;
    }
    
    /**
     * Constructs object and initializes it so that it represents the time given by the input string
     * The date string must be using the ISO format: "yyyy-MMM-dd [HH:mm:ss[.ffffff]]"
     */
    public Date( String date )
    {        
	try {
	    synchronized (date_formatter) {
		super.setTime( date_formatter.parse( date ).getTime() );
	    }
        }
        catch( java.text.ParseException e ) {
	    try {
		synchronized (date_formatter_backup) {
		    super.setTime( date_formatter_backup.parse( date ).getTime() );
		}
	    }
	    catch( java.text.ParseException ex ) { }
        }

	microseconds = super.getTime() * 1000l;
    }
    
    /**
     * Constructs object from the number of microseconds since January 1, 1970, 00:00:00 GMT.
     */
    public Date( long microseconds )
    {
	super( microseconds / 1000l );
        this.microseconds = microseconds;
    }
    
    /**
     * Tests if this date is after the specified date.
     * @return true if and only if the instant represented by this 
     * Date object is strictly later than the instant represented by anotherDate; 
     * false otherwise.	   
     */
    boolean after( Date anotherDate )
    {
    	return ( microseconds > anotherDate.microseconds );
    }
    
    /**
     * Tests if this date is before the specified date.
     * @return true if and only if the instant represented by this 
     * Date object is strictly earlier than the instant represented by anotherDate; 
     * false otherwise.	   
     */
    boolean before( Date anotherDate )
    {
    	return anotherDate.after( this );
    }
    
    /**
     * Compares two Dates for ordering.
     * @return the value 0 if the argument Date is equal to this Date; 
     * a value less than 0 if this Date is before the Date argument; 
     * and a value greater than 0 if this Date is after the Date argument.	   
     */
    public int compareTo( Date anotherDate )
    {
        long diff = microseconds - anotherDate.microseconds;
        return ( diff == 0 ? 0 : diff < 0 ? -1 : +1 );
    }
    
    /**
     * Sets this Date object to represent a point in time that is time microseconds after January 1, 1970 00:00:00 GMT.
     */
    public void setTimeMicro( long microseconds )
    {
        setTime( microseconds / 1000l );
        this.microseconds = microseconds;
    }
    
    /**
     * Sets this Date object to represent a point in time that is time milliseconds after January 1, 1970 00:00:00 GMT.
     */
    public void setTime( long milliseconds )
    {
        super.setTime( milliseconds );
        this.microseconds = milliseconds * 1000l;
    }
    
    /**
     * Returns the number of microseconds since January 1, 1970, 00:00:00 GMT represented by this object.
     */
    public long getTimeMicro()
    {
    	return microseconds;
    }
    
    /**
     * Returns the string representation of this object.
     * The string will be given in ISO format: "yyyy-MMM-dd"
     */
    public String toString()
    {
	synchronized (date_formatter) {
	    return date_formatter.format( this );
	}
    }
}
