package is;

/**
 * The listener interface for receiving "interesting" information events
 * (create, update and delete information) from the IS repository. This class is
 * used as an abstarct base for the EventListener and InfoListener interfaces.
 * Do not implement this interface directly, use one of the EventListener and
 * InfoListener interfaces instead. An info event is generated when the
 * information object is created, updated or removed form the IS. When an info
 * event occurs the relevant method in the listener object is invoked, and the
 * InfoEvent is passed to it.
 * 
 * @author Sergei Kolos
 * @see InfoListener
 * @see EventListener
 */

public interface Listener
{
}
