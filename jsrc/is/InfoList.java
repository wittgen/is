package is;

import ipc.Partition;
import java.util.ArrayList;

/**
 * The InfoList enumerates the infromation objects in
 * the IS repository according to the arguments of the 
 * InfoList constructor. The stream read objects together
 * with their values, so contary to the InfoList class, the
 * getInfo functions of this one never throw.
 * @author Sergei Kolos
 */
public class InfoList
{
    class Stream extends is.streamPOA
    {
	private ArrayList<is.info> array = new ArrayList<is.info>();
	private boolean eof_received = false;
	
	public void push(is.info_history[] info)
	{
	    for ( int i = 0; i < info.length; ++i )
		for ( int n = 0; n < info[i].values.length; ++n )
		    array.add(new is.info(InfoList.this.server + "." + info[i].name, info[i].type, info[i].values[n]));
	}

	public synchronized void eof()
	{
	    eof_received = true;
	    notify();
	}
	
	synchronized ArrayList<is.info> getInfoList()
	{
	    int received = array.size();
	    while (!eof_received)
	    {
		try {
		    wait(1000);
		    if (received == array.size()) {
		        break;
		    }
		    received = array.size();
		}
		catch(java.lang.InterruptedException ex) {
		    break;
		}
	    }
	    return array;
	}
    }
    
    private static final java.util.regex.Pattern defaultRegularExpression = java.util.regex.Pattern.compile( ".*" );
    
    private String		server;
    private ArrayList<is.info>	information;
    
    private void deactivate(is.stream stream)
    {
        try {
            ipc.Core.getRootPOA().deactivate_object( ipc.Core.getRootPOA().reference_to_id( stream ) );
        }
        catch( org.omg.PortableServer.POAPackage.ObjectNotActive ex ) {}
        catch( org.omg.PortableServer.POAPackage.WrongAdapter ex ) {}
        catch( org.omg.PortableServer.POAPackage.WrongPolicy ex ) {}
    }
    
    /**
     * Creates a new information list initialized with the information objects belonging
     * to the <tt>name</tt> IS server in the <tt>partition</tt> partition. Only information objects whose
     * names match the <tt>rexp</tt> regular expression are listed.
     * @param partition IPC partition
     * @param server_name IS server name
     * @param criteria criteria to select information objects
     * @exception is.InvalidCriteriaException the given criteria contains invalid regular expression
     */
    public InfoList( ipc.Partition partition, String server_name, Criteria criteria )
		throws is.InvalidCriteriaException, is.RepositoryNotFoundException
    {
    	initialize(partition, server_name, criteria);
    }        

    /**
     * Creates a new information list initialized with all the information objects belonging
     * to the <tt>name</tt> IS server in the <tt>partition</tt> partition.
     * @param partition IPC partition
     * @param server_name IS server name
     */
    public InfoList( ipc.Partition partition, String server_name )
		throws is.RepositoryNotFoundException
    {
    	try {
            initialize( partition, server_name, new Criteria( defaultRegularExpression ) );
        }
        catch (is.InvalidCriteriaException ex) {
            throw new RuntimeException(
            	"Impossible happens, IS server denies default regular expression as information selection criteria", ex);
        }
    }        

    /**
     * Creates a new information list initialized with the information objects belonging
     * to the <tt>name</tt> IS server in the <tt>partition</tt> partition. Only information objects whose
     * names match the <tt>rexp</tt> regular expression are listed.
     * @param partition IPC partition
     * @param name IS server name
     * @param pattern regular expression for the information names
     * @exception is.InvalidCriteriaException is the rexp is not valid regular expression
     */
    public InfoList( ipc.Partition partition, String server_name, java.util.regex.Pattern pattern ) 
    		throws is.InvalidCriteriaException, is.RepositoryNotFoundException
    {	
	initialize( partition, server_name, new Criteria( pattern ) );
    }        

    public int size()
    {
    	return information.size();
    }
    
    public int getSize()
    {
    	return information.size();
    }

    public String getServerName()
    {
    	return server;
    }

    public String getName( int i )
    {
	return information.get(i).name;
    }
    
    public Type getType( int i )
    {
	return new Type( information.get(i).type );
    }
    
    public Time getTime( int i )
    {
	return new Time( information.get(i).value.attr.time );
    }
    
    public int getTag( int i )
    {
	return information.get(i).value.attr.tag;
    }
    
    public AnyInfo getInfo( int i )
    {
	AnyInfo info = new AnyInfo();
	try {
            fillInfo(i, info);
        } catch (InfoNotCompatibleException e) {
            // never happens
            e.printStackTrace();
        }
	return info;
    }
    
    public void getInfo( int i, Info info ) throws is.InfoNotCompatibleException
    {
	Type type = getType( i );
	if (	!( info instanceof AnyInfo )
	     && !type.subTypeOf( info.getType() ) )
	{
	    throw new is.InfoNotCompatibleException(getName(i));
	}
	fillInfo(i, info);
    }
    
    public void getInfo( int i, AnyInfo info ) throws is.InfoNotCompatibleException
    {
	fillInfo(i, info);
    }
    
    private void initialize( ipc.Partition partition, String server_name, Criteria criteria )
		throws is.InvalidCriteriaException, is.RepositoryNotFoundException
    {	
	this.server = server_name;
        try
	{
	    is.repository r = Repository.resolve( partition, server_name );
	    
	    Stream stream = new Stream();
            is.stream corba_ref = stream._this(ipc.Core.getORB());
	    r.create_stream( criteria.criteria, corba_ref, 1, is.sorted.ByTime );
	    information = stream.getInfoList();
            deactivate(corba_ref);
	}
	catch ( org.omg.CORBA.SystemException ex )
	{
	    throw new is.RepositoryNotFoundException(partition, server_name);
	}
	catch ( is.InvalidCriteria ex )
	{
	    throw new is.InvalidCriteriaException(criteria);
	}
    }
        
    private void fillInfo( int i, Info info ) throws InfoNotCompatibleException
    {
	is.info ii = information.get(i);
        info.update(getName(i), ii);
    }
}
