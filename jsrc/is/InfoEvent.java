package is;

import ipc.Partition;

/**
 * An event which indicates that a certain event occurred in the IS repository for a given
 * Information object: the information was created, updated, deleted or a subscription
 * from the current application was successfully established for the object.
 * 
 * @author Sergei Kolos
 * @see InfoListener
 * @see Receiver
 */
public class InfoEvent
{
    private String name;
    private Type type;
    private Time time;
    private int tag;
    private Partition partition;
    private is.info value;

    InfoEvent(is.info info)
    {
	name = info.name;
        tag = info.value.attr.tag;
	type = new Type(info.type);
	time = new Time(info.value.attr.time);
	value = info;
    }

    InfoEvent(Partition p, is.event event)
    {
	name = event.name;
        tag = event.attr.tag;
	type = new Type(event.type);
	time = new Time(event.attr.time);
	partition = p;
    }

    /**
     * Gets name of the information object that has been changed.
     * 
     * @return information name
     */
    public String getName()
    {
	return name;
    }

    /**
     * Gets type of the information object that has been changed.
     * 
     * @return information type
     */
    public Type getType()
    {
	return type;
    }

    /**
     * Gets time of the last modification of the information object.
     * 
     * @return modification time
     */
    public Time getTime()
    {
	return time;
    }

    /**
     * Sets the attributes values of the info object to the values corresponding
     * to the current event. You can pass to this method either the object of
     * the AnyInfo class or the object of the same class as the class of the
     * object whose change is reported.
     * 
     * @param info
     *            information object to be updated
     * @exception InfoNotCompatibleException
     *                the info object type is not the same as the type of the
     *                object whose changes are notified by this event
     */
    public void getValue(Info info) throws is.InfoNotCompatibleException,
	    					is.ValueReadingException
    {
        if (value == null) {
            readData();
        }

        info.update(name, value);
    }

    private void readData() throws is.ValueReadingException
    {
	is.infoHolder ih = new is.infoHolder();

	try {
	    is.repository rep = Repository.resolve(partition, name);
	    rep.get_value(Repository.getObjectName(name), tag, ih);
	    value = ih.value;
	}
	catch (org.omg.CORBA.SystemException ex) {
	    throw new is.ValueReadingException(partition,
		    Repository.getServerName(name));
	}
	catch (is.RepositoryNotFoundException ex) {
	    throw new is.ValueReadingException(partition, 
		    Repository.getServerName(name), Repository.getObjectName(name));
	}
	catch (is.NotFound ex) {
	    throw new is.ValueReadingException(partition, 
		    Repository.getServerName(name), Repository.getObjectName(name));
	}
    }
}
