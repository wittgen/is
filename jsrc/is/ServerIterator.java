package is;

import ipc.*;

/**
 * The server iterator class allows application to enumerate all the IS servers 
 * registered in the specific partition.
 * @author Sergei Kolos
 */
public class ServerIterator extends ipc.ObjectEnumeration<is.repository>
{    
    /**
     * Constructs a server iterator for the specified partition.
     * @param partition 
     */
    public ServerIterator( Partition partition ) throws ipc.InvalidPartitionException
    {
	super( partition, is.repository.class );
    }
    
    /**
     * Returns the next ipc.Element element for this iterator.
     * Allows to use this class as implementation of the java.util.Enumeration interface.
     * @return the next IS server name
     */
    public is.repository nextServer()
    {
	return nextElement().reference;
    }
    
    /**
     * Returns the next server name from this server iterator.
     * @return the next IS server name
     */
    public String nextServerName()
    {
	return nextElement().name;
    }    
}
