package is;

import java.io.*;
import java.lang.reflect.*;

/**
 * This class can be used to read the iformation whose structure is unknown at compilation tyme. 
 * @author Sergei Kolos
 * @see Info
 */
public class AnyInfo extends Info
{        
    private InfoInStream			in_stream;    
    private int					internal_index = 0;
    private java.lang.Object			hash[] = null;
       
    /**
     * Creates a new information object that can be used for reading arbitrary information from IS.
     */
    public AnyInfo( )
    {
	super( Type.Null );
    }

    private AnyInfo( InfoInStream in, Type type )
    {
	super( type );
        in_stream = in;
	hash = new java.lang.Object[getAttributeCount()];
        for ( int i = 0; i < getAttributeCount(); i++ )
	    nextAttribute();
        internal_index = 0;
        in_stream = null;
    }

    /**
     * Gets the name associated with this information object. The valid name
     * is associated with this object by a successful invocation of any operation, which
     * reads information objects from the IS repository, for example Repository.getValue.
     * @return information name
     */
    public String getName()
    {
        return super.getName();
    }
    
    /**
     * Returns the number of attributes in this information object.
     * @return the number of attributes in this object
     */
    public int getAttributeCount( )
    {
	return getType().getAttributeCount();
    }

   /**
     * Returns the value of the attribute at the current position in this information object.
     * The current position will be set to the next attribute
     * @return the value of the attribute at the current position
     */
    public java.lang.Object nextAttribute( )
    {
	if ( hash[internal_index] == null )
	{	    
	    if ( getType().isAttributeArray( internal_index ) )
		hash[internal_index] = readArray( in_stream, internal_index );
	    else
		hash[internal_index] = readPrimitive( in_stream, internal_index );
	}   
	java.lang.Object r = hash[internal_index];
        
        if (++internal_index == getAttributeCount())
            internal_index = 0;
        
        return r;
    }
    
    /**
     * Returns the value of the primitive attribute at the current position in this information object.
     * The current position will be set to the next attribute
     * @return the value of the attribute at the current position
     */
    public <T> T nextPrimitiveAttribute( )
    {
	return (T)nextAttribute( );
    }
    
    /**
     * Returns the value of the array attribute at the current position in this information object.
     * The current position will be set to the next attribute
     * @return the value of the attribute at the current position
     */
    public <T> T[] nextArrayAttribute( )
    {
	return (T[])nextAttribute( );
    }
    
    /**
     * Returns the type code of the attribute at the current position in this information object.
     * @return type name of the attribute at the specified position
     */
    public byte getAttributeType( )
    {
    	return getAttributeType( internal_index );
    }
    
    /**
     * @return true if the attribute at the current position is an array, false otherwise
     */
    public boolean isAttributeArray( )
    {
    	return getType().isAttributeArray( internal_index );
    }
    
     /**
     * Returns type name of the attribute at the current position in this information object.
     * @return type name of the attribute at the current position
     */
    public String getAttributeTypeName( )
    {
    	return getType().getAttributeTypeName( internal_index );
    }
    
    /**
     * Returns the type code of the attribute at the specified position in this information object.
     * @param index attribute position, first attributes has position 0
     * @return type name of the attribute at the specified position
     */
    public byte getAttributeType( int index )
    {
    	return getType().getAttributeType( index );
    }
    
    /**
     * @param index attribute position, first attributes has position 0
     * @return true if the attribute at specified position is an array, false otherwise
     */
    public boolean isAttributeArray( int index )
    {
    	return getType().isAttributeArray( index );
    }
    
     /**
     * Returns type name of the attribute at the specified position in this information object.
     * @param index attribute position, first attributes has position 0
     * @return type name of the attribute at the specified position
     */
    public String getAttributeTypeName( int index )
    {
    	return getType().getAttributeTypeName( index );
    }
    
    /**
     * Returns the value of the attribute at the index position in this information object.
     * The current position will be set to the next attribute
     * @param index attribute position, first attributes has position 0
     * @return the value of the attribute at the current position
     */
    public java.lang.Object getAttribute( int index )
    {
        if ( hash[index] != null )
	    return hash[index];
	    
	if ( internal_index > index )
	    reset();
	while ( internal_index != index )
	    nextAttribute();
	    
	return nextAttribute( );
    }
    
    /**
     * Returns the value of the primitive attribute at the specified position in this information object.
     * @param index attribute position, first attributes has position 0
     * @return the value of the attribute at the specified position
     */
    public <T> T getPrimitiveAttribute( int index )
    {
	return (T)getAttribute( index );
    }
    
    /**
     * Returns the value opf the array attribute at the specified position in this information object.
     * @param index attribute position, first attributes has position 0
     * @return the value of the attribute at the specified position
     */
    public <T> T[] getArrayAttribute( int index )
    {
	return (T[])getAttribute( index );
    }
    
    /**
     * Resets the object to the state, in which it was immediately after construction.
     * In another words it sets the input position to the first attribute of the object.
     */
    public void reset()
    {
	if ( in_stream != null ) 
            in_stream.reset();
	internal_index = 0;
    }	

    public final void publishGuts( is.Ostream out )
    {
	throw new RuntimeException( "Instances of the AnyInfo class can not be published." );
    }
    
    public final void refreshGuts( is.Istream in )
    {
	in_stream = (InfoInStream)in;
        internal_index = 0;
    }

    /**
     * Returns string representation of the information object.
     * @return string representation
     */
    public final String toString()
    {
	String out = new String();
	for( int i = 0; i < getAttributeCount(); i++ )
	{
	    Object attr = nextAttribute();
		
	    if ( attr.getClass().isArray() )
	    {
		int size = Array.getLength( attr );
		out += attr.getClass().getComponentType().getName() + "[" + size + "] = ";
		if ( size > 0 )
		{
		    for ( int j = 0; j < size - 1; j++ )
		    {
			out += Array.get( attr, j ) + ", ";
		    }
		    out += Array.get( attr, size - 1 );
		}
	    }
	    else
	    {
		out += attr.getClass().getName() + " = " + attr;
	    }
	    out += "\n";
	}
	return out;
    }
    	
    void setType( Type type )
    {
	super.setType( type );
	hash = new java.lang.Object[getAttributeCount()];
    }
    
    private java.lang.Object readPrimitive( InfoInStream in, int index )
    {	
	switch ( getAttributeType( index ) )
	{
	    case Type.BOOLEAN:
	    	return in.getBoolean( );
	    case Type.S8:
	    case Type.U8:
	    	return in.getByte( );
	    case Type.S16:
	    case Type.U16:
	    	return in.getShort( );
	    case Type.S32:
	    case Type.U32:
	    	return in.getInt( );
	    case Type.S64:
	    case Type.U64:
	    	return in.getLong( );
	    case Type.FLOAT:
	    	return in.getFloat( );
	    case Type.DOUBLE:
	    	return in.getDouble( );
	    case Type.STRING:
	    	return in.getString( );
	    case Type.TIME:
	    	return in.getTime( );
	    case Type.DATE:
	    	return in.getDate( );
	    case Type.INFO:
		return new AnyInfo( in, getType().getAttributeInfoType( index ) );
	    default:
	    	throw new RuntimeException( "Impossible happened. IS Information attribute type = " + getType().getAttributeInfoType( index ) );
	}
    }
    
    private java.lang.Object readArray( InfoInStream in, int index )
    {
	switch ( getAttributeType( index ) )
	{
	    case Type.BOOLEAN:
	    	return in.getBooleanArray( );
	    case Type.S8:
	    case Type.U8:
	    	return in.getByteArray( );
	    case Type.S16:
	    case Type.U16:
	    	return in.getShortArray( );
	    case Type.S32:
	    case Type.U32:
	    	return in.getIntArray( );
	    case Type.S64:
	    case Type.U64:
	    	return in.getLongArray( );
	    case Type.FLOAT:
	    	return in.getFloatArray( );
	    case Type.DOUBLE:
	    	return in.getDoubleArray( );
	    case Type.STRING:
	    	return in.getStringArray( );
	    case Type.TIME:
	    	return in.getTimeArray( );
	    case Type.DATE:
	    	return in.getDateArray( );
	    case Type.INFO:
		AnyInfo[] ai = new AnyInfo[in.getInt()];
                for ( int j = 0; j < ai.length; j++ )
                {
		    ai[j] = (AnyInfo)readPrimitive( in, index );
                }
		return ai;
	    default:
	    	throw new RuntimeException( "Impossible happened. IS Information attribute type = " + getType().getAttributeInfoType( index ) );
	}
    }
}
