package is;

import ipc.Object;

class CallbackHolder<T extends is.callback> extends ipc.Object<T>
	implements Callback
{
    private final InfoCreatedListener cl;
    private final InfoUpdatedListener ul;
    private final InfoDeletedListener dl;
    private final InfoSubscribedListener sl;
    private final int event_mask;

    CallbackHolder( Listener listener )
    {
	this.cl = listener instanceof InfoCreatedListener
		? ((InfoCreatedListener)listener) 
		: new InfoCreatedListener() { public final void infoCreated(InfoEvent e) {} };
	this.ul = listener instanceof InfoUpdatedListener
		? ((InfoUpdatedListener)listener) 
		: new InfoUpdatedListener() { public final void infoUpdated(InfoEvent e) {} };
	this.dl = listener instanceof InfoDeletedListener
		? ((InfoDeletedListener)listener) 
		: new InfoDeletedListener() { public final void infoDeleted(InfoEvent e) {} };
	this.sl = listener instanceof InfoSubscribedListener
		? ((InfoSubscribedListener)listener) 
		: new InfoSubscribedListener() { public final void infoSubscribed(InfoEvent e) {} };
		
	event_mask = 
		((listener instanceof InfoCreatedListener    ? 1 : 0) << 0 ) |
		((listener instanceof InfoUpdatedListener    ? 1 : 0) << 1 ) |
		((listener instanceof InfoDeletedListener    ? 1 : 0) << 2 ) |
		((listener instanceof InfoSubscribedListener ? 1 : 0) << 3 );
    }

    public String toString()
    {
	return ipc.Core.objectToString(objectReference(), ipc.Core.CORBALOC);
    }
    
    public is.callback objectReference()
    {
	return _this();
    }
    
    public int eventMask()
    {
	return event_mask;
    }

    public final void receive(InfoEvent info, is.reason r)
    {
	switch (r.value())
	{
	    case is.reason._Created:
		cl.infoCreated(info);
		break;
	    case is.reason._Updated:
		ul.infoUpdated(info);
		break;
	    case is.reason._Deleted:
		dl.infoDeleted(info);
		break;
	    case is.reason._Subscribed:
		sl.infoSubscribed(info);
		break;
	}
    }
}
