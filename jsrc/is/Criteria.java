package is;

public class Criteria
{
    public enum Logic { AND, OR };
    
    criteria criteria;
    
    Criteria( criteria criteria )
    {
    	this.criteria = criteria;
    }

    public String toString( )
    {
	return (  criteria.name_.value_
		+ criteria.type_.value_.name
		+ new String( criteria.type_.value_.code ) 
                + criteria.type_.logic_.toString() );
    }
    
    /**
     * Creates criteria, which contains a regular expresssion for the information names.
     * @param pattern regular expression, that is used for subscription. 
     */
    public Criteria( java.util.regex.Pattern pattern )
    {
	this.criteria = new is.criteria( new is.criteriaPackage.name( pattern.pattern(), false ),
					is.criteriaPackage.logic.And,
					new is.criteriaPackage.type( Type.Null.getType(),
			 		is.criteriaPackage.typePackage.logic.Ignore ) );
    }
    
    /**
     * Creates criteria, which contains a type of information objects.
     * @param type a type of information object
     */
    public Criteria( Type type )
    {
	is.criteriaPackage.typePackage.logic type_logic = is.criteriaPackage.typePackage.logic.Exact;
        if ( type.weak && type.inverted )
	    type_logic = is.criteriaPackage.typePackage.logic.NotBase;
        else if ( type.weak )
	    type_logic = is.criteriaPackage.typePackage.logic.Base;
        else if ( type.inverted )
	    type_logic = is.criteriaPackage.typePackage.logic.Not;
	this.criteria = new is.criteria( new is.criteriaPackage.name( "", true ),
					     is.criteriaPackage.logic.And,
						     new is.criteriaPackage.type( type.getType(),
			 			     type_logic ) );
    }
    
    /**
     * Creates criteria, which contains both a regular expresssion for the information names and
     * also a type of information objects. There are two possible combinations of those itemds,
     * either logical AND or logical OR as defined by the third parameter of this constructor.
     * @param pattern regular expression. 
     * @param type a type of information object
     * @param logic defines a way in which previous two parameters are combined. Can be either
     *		Criteria.AND or Criteria.OR.
     */
    public Criteria( java.util.regex.Pattern pattern, Type type, Logic logic )
    {
	is.criteriaPackage.typePackage.logic type_logic = is.criteriaPackage.typePackage.logic.Exact;
        if ( type.weak && type.inverted )
	    type_logic = is.criteriaPackage.typePackage.logic.NotBase;
        else if ( type.weak )
	    type_logic = is.criteriaPackage.typePackage.logic.Base;
        else if ( type.inverted )
	    type_logic = is.criteriaPackage.typePackage.logic.Not;
        
        this.criteria = new is.criteria( new is.criteriaPackage.name( pattern.pattern(), false ),
			 			logic == Logic.AND	? is.criteriaPackage.logic.And
									: is.criteriaPackage.logic.Or,
						new is.criteriaPackage.type( type.getType(),
						type_logic ) );
    }
}
