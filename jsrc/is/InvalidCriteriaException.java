package is;

/**
 * Thrown to indicate that the expression provided is not a 
 * valid regular expression.
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class InvalidCriteriaException extends Exception 
{
    /**
     * Constructs an InvalidCriteriaException with the specified
     * criteria.
     *
     * @param criteria the detail criteria
     */
    public InvalidCriteriaException( Criteria criteria ) 
    {
	super("Invalid criteria '" + criteria.toString() + " is used");
    }
}
