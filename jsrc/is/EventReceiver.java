package is;

/**
 * This interface has to be extended by an IS subscriber which wants
 * to receive only simple notifications when certain events happens in the
 * IS repository. Contrary to the is.InfoReceiver an implementation
 * of this interface never receives the value of the modified infromation object.
 * Instead only fixed size notification object, which contains information name
 * type and time stamp, is sent to this receiver. 
 * 
 * @author Sergei Kolos
 * @see EventListener
 * @see Repository
 */

public interface EventReceiver extends Listener
{
}
