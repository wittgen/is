#ifndef IS_CALLBACK_EVENT_H
#define IS_CALLBACK_EVENT_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      callbackevent.h
//
//      public header file for the IS library
//
//      Sergei Kolos March 2006
//
//      description:
//		ISCallbackEvent allows to get pass the information to a 
//				user defined callback function
//////////////////////////////////////////////////////////////////////////////////////
#include <boost/noncopyable.hpp>

#include <is/info.h>

template<class, class > class ISCallbackImpl;

class ISCallbackEvent: boost::noncopyable {
    template<class, class > friend class ISCallbackImpl;

public:
    const char * name() const {
        return m_name.c_str();
    }

    const char * objectName() const {
        return m_event.name;
    }

    const char * serverName() const {
        return m_server_name.c_str();
    }

    ISInfo::Reason reason() const {
        return m_reason;
    }

    void * parameter() const {
        return m_user_param;
    }

    const IPCPartition & partition() const {
        return m_partition;
    }

    ISType type() const {
        return ISType(m_event.type);
    }

    OWLTime time() const {
        return OWLTime(m_event.attr.time / 1000000, m_event.attr.time % 1000000);
    }

    int tag() const {
        return m_event.attr.tag;
    }

protected:
    ISCallbackEvent(const IPCPartition & partition,
            const std::string & server_name, void * user_param,
            is::reason reason, const is::event & event);

protected:
    const IPCPartition & m_partition;
    const std::string & m_server_name;
    const std::string m_name;
    const ISInfo::Reason m_reason;
    const is::event & m_event;
    void * m_user_param;
};

#endif
