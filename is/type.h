#ifndef IS_TYPE_H
#define IS_TYPE_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      istype.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//        Class ISType supports dynamic information type identification
//////////////////////////////////////////////////////////////////////////////////////

#include <map>
#include <vector>

#include <boost/type_traits/is_enum.hpp>
 
#include <is/types.h>
#include <is/ostream.h>
#include <is/is.hh>

#define IS_TYPEID(_,y)			y
#define IS_TYPEID_SEPARATOR		,

class ISInfoDocument;

class ISType
{
    friend class ISInfo;
    friend class ISNamedInfo;
  
    static const std::string unknown;
    static const std::string null;
    static const std::string error;
    
  public:
    static const ISType Null;
    
    enum Basic {  
	Error = 0,
	IS_TYPES(IS_TYPEID)  
    };
        
    ISType( );
    ISType( const ISInfoDocument & doc );

    bool operator== ( const ISType & ) const;
    bool operator!= ( const ISType & ) const;
    
    ISType operator!() const;
    ISType operator~() const;

    bool compatibleWith ( const ISType & ) const;
    bool superTypeOf ( const ISType & ) const;
    bool subTypeOf ( const ISType & ) const;

    const std::string & name ( ) const;
    size_t entries ( ) const;
    Basic  entryType ( size_t index ) const;
    const std::string & entryTypeName ( size_t index ) const;
    ISType entryInfoType ( size_t index ) const;
    bool   entryArray ( size_t index ) const;
    
    static Basic basicTypeFromName( const std::string & t );
    static const std::string & basicTypeName( Basic t );
    static const size_t max_type_name_width = 12;
    
    template <typename T>
    void operator<<( const T & );
    
    template <typename T>
    void put( const T *, size_t , size_t, const T & );

    void operator<<( const ISInfo & );
    
    void put( const ISInfo * , size_t , size_t , const ISInfo & );
    
  public:
    ISType( const is::type & );
    
    void type( is::type & type ) const;
    
    bool inverted() const { return m_inverted; }
    
    bool weak() const { return m_weak; }

  private:

    static const unsigned char ArrayMask = 64;
    
    struct Basic2TypeName : public std::map< Basic, std::string >
    {
    	Basic2TypeName();
    };
    static Basic2TypeName basic_2_type_name;
            
    ISType( const std::string & name );
    
    std::string m_id;
    std::string	m_name;
    std::string m_data;
    bool m_inverted;
    bool m_weak;
    std::vector<ISType>	m_nested_types;
};

///////////////////////////////////////////////////////////
// Ouptput operators
///////////////////////////////////////////////////////////

std::ostream& operator<< ( std::ostream & , const ISType & );
std::ostream& operator<< ( std::ostream & , const ISType::Basic );

//////////////////////////////////////////////////////////////
// Define mapping from type to the type id
//////////////////////////////////////////////////////////////
#define IS_TYPE_TO_TYPEID(x,y) \
    template<>  struct type2id<x,false> { \
        static const ISType::Basic id = ISType::y; \
        static const bool is_array = false; }; \
    template<>  struct type2id<std::vector<x>,false> { \
        static const ISType::Basic id = ISType::y; \
        static const bool is_array = true; };

#define IS_TYPE_TO_TYPEID_SEPARATOR
#define IS_TYPE_TO_TYPEID_OBJECT_TYPE	ISInfo

namespace is
{
    ///////////////////////////////////////////
    // Defines Type to ID mapping
    ///////////////////////////////////////////
    template <typename T, bool an_enum = boost::is_enum<T>::value>
    struct type2id;

    template <typename T>
    struct type2id<T,false> {
    	static const ISType::Basic id = ISType::InfoObject;
        static const bool is_array = false;
    };

    template <typename T>
    struct type2id<std::vector<T>,false> {
    	static const ISType::Basic id = ISType::InfoObject;
        static const bool is_array = true;
    };

    ///////////////////////////////////////////
    // Enumerations are mapped to S32
    ///////////////////////////////////////////
    template <typename T>
    struct type2id<T,true> { 
    	static const ISType::Basic id = ISType::S32; 
        static const bool is_array = false;
    };

    template <typename T>
    struct type2id<std::vector<T>,true> { 
    	static const ISType::Basic id = ISType::S32; 
        static const bool is_array = true; 
    };
    
    ///////////////////////////////////////////
    // Other mappings are generated
    ///////////////////////////////////////////
    IS_TYPES(IS_TYPE_TO_TYPEID)
}

//////////////////////////////////////////////////////////////
// Define mapping from type id to the corresponding C++ type
//////////////////////////////////////////////////////////////
#define IS_TYPEID_TO_TYPE(x,y)		template<>  struct id2type<ISType::y> { \
						typedef x type; };
                                                
#define IS_TYPEID_TO_TYPE_SEPARATOR
#define IS_TYPEID_TO_TYPE_OBJECT_TYPE	ISInfo

namespace is
{
    ///////////////////////////////////////////
    // Defines ID to Type mapping
    ///////////////////////////////////////////
    template <ISType::Basic id>
    struct id2type;
    
    IS_TYPES(IS_TYPEID_TO_TYPE)
}

///////////////////////////////////////////////////////////
// Ouptput functions for basic IS types
///////////////////////////////////////////////////////////
namespace is
{
    template <typename T>
    std::ostream &
    print( std::ostream & out, const T & val )
    {
	out << ISType::basicTypeName( is::type2id<T>::id ) << "("
	    << static_cast<const typename boost::integral_promotion<T>::type &>( val ) << ")";
	return out;
    }

    template <typename T>
    std::ostream &
    print( std::ostream & out, const std::vector<T> & val )
    {
	out << ISType::basicTypeName( is::type2id<T>::id ) << "[" << val.size() << "] (" ;
	if ( val.size() > 0 )
	{
	    for ( size_t i = 0; i < val.size() - 1; ++i )
		out << static_cast<const typename boost::integral_promotion<T>::type &>( val[i] ) << ", ";
	    out << static_cast<const typename boost::integral_promotion<T>::type &>( val[val.size() - 1] );
	}
	out << ")";
	return out;
    }
}

///////////////////////////////////////////////////////////
// ISType inline methods and operators implementation
///////////////////////////////////////////////////////////

inline
ISType::ISType( const std::string & name )
  : m_name( name ),
    m_inverted( false ),
    m_weak( false )
{ ; }


inline
ISType::ISType( )
  : m_name( ISType::null ),
    m_inverted( false ),
    m_weak( false )
{ ; }

template <typename T>
void
ISType::operator<<( const T & ) {
    m_data += static_cast<char>( is::type2id<T>::id );
    m_id += static_cast<char>( is::type2id<T>::id );
}

template <typename T>
void
ISType::put( const T *, size_t , size_t, const T & ) {
    m_data += static_cast<char>( static_cast<unsigned char>(is::type2id<T>::id) | ArrayMask );
    m_id += static_cast<char>( static_cast<unsigned char>(is::type2id<T>::id) | ArrayMask );
}

inline bool
ISType::operator== ( const ISType & ist ) const
{
    return m_inverted ? ( m_id != ist.m_id || m_name != ist.m_name )
		     : ( m_id == ist.m_id && m_name == ist.m_name );
}

inline bool
ISType::operator!= ( const ISType & ist ) const
{
    return !operator==( ist );
}

inline ISType
ISType::operator!( ) const
{
    ISType copy( *this );
    copy.m_inverted = true;
    return copy;
}

inline ISType
ISType::operator~( ) const
{
    ISType copy( *this );
    copy.m_weak = true;
    return copy;
}

inline bool
ISType::compatibleWith ( const ISType & ist ) const
{
    return ( m_inverted ? m_id != ist.m_id : m_id == ist.m_id );
}

inline bool
ISType::superTypeOf ( const ISType & ist ) const
{
    return ist.subTypeOf( *this );
}

inline bool
ISType::subTypeOf ( const ISType & ist ) const
{
    size_t pos = m_id.rfind( ist.m_id, 0 );
    return ( m_inverted ? pos != 0 : pos == 0 );
}

inline const std::string & 
ISType::name () const
{
    return m_name;
}

inline size_t
ISType::entries ( ) const
{
    return m_data.size();
}

inline ISType::Basic
ISType::entryType ( size_t index ) const
{
    return ( index < m_data.size() 
    			? static_cast<ISType::Basic>(((unsigned char)m_data[index]) & (~ArrayMask)) 
			: ISType::Error );
}

inline bool
ISType::entryArray ( size_t index ) const
{
    return ( index < m_data.size() ? ( m_data[index] & ArrayMask ) != 0 : false );
}

#endif    // IS_TYPE_H
