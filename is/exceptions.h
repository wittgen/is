#ifndef IS_EXCEPTIONS_H
#define IS_EXCEPTIONS_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/exceptions.h
//
//      private header file for the IS library
//
//      Sergei Kolos March 2004
//
//      description:
//              Declares IS exceptions
//////////////////////////////////////////////////////////////////////////////////////

#include <ers/ers.h>
#include <ipc/exceptions.h>

namespace is
{
    inline void fatal( const ers::Issue & issue )
    {
    	ers::fatal( issue );
	exit( 13 );
    }
}

/*! \namespace daq
 *  This is a wrapping namespace for all IS exceptions.
 */ 
namespace daq {
 
    /*! \namespace daq::is
     *  This is a wrapping namespace for all IS exceptions.
     */
 
    /*! \class is::Exception
     *  This is a base class for all IS exceptions.
     */
    ERS_DECLARE_ISSUE( is, Exception, ERS_EMPTY, ERS_EMPTY )
                                                                                                                                         
    /*! \class is::NotFound
     *  Base class for the daq::is::RepositoryNotFound and daq::is::InfomationNotFound exceptions.
     */
    ERS_DECLARE_ISSUE_BASE( is, NotFound, Exception, ERS_EMPTY, ERS_EMPTY, ERS_EMPTY )
    
    /*! \class is::RepositoryNotFound
     *  This issue is reported if respective IS server is not running.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				RepositoryNotFound,
                                is::NotFound,
                                "IS repository '" << name << "' does not exist",
                                ERS_EMPTY,
                                ((std::string)name )
    			)

    /*! \class is::AnnotationNotFound
     *  This issue is reported if respective IS server is not running.
     */
    ERS_DECLARE_ISSUE_BASE(	is,
				AnnotationNotFound,
				is::NotFound,
				"Annotation with '" << name << "' name does not exist",
				ERS_EMPTY,
				((std::string)name )
                        )
                        
    /*! \class is::DocumentNotFound
     *  This issue is reported if respective IS server is not running.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				DocumentNotFound,
                                is::NotFound,
                                "IS Meta information for the '" << name << "' class does not exist",
                                ERS_EMPTY,
                                ((std::string)name )
    			)
                        
    /*! \class is::AttributeNotFound
     *  This issue is reported if respective IS server is not running.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				AttributeNotFound,
                                is::NotFound,
                                "Can not find the '" << name << "' attribute in the IS meta-data description",
                                ERS_EMPTY,
                                ((std::string)name )
    			)
                        
    /*! \class is::InfoNotFound
     *  This issue is reported if respective information does not exist in the IS repository.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				InfoNotFound,
                                is::NotFound,
                                "IS information '" << name << "' does not exist",
                                ERS_EMPTY,
                                ((std::string)name )
    			)                        
    
    /*! \class is::ProviderNotFound
     *  This issue is reported if provider for the given information does not exist.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				ProviderNotFound,
                                is::NotFound,
                                "Provider of the IS information '" << name << "' does not exist",
                                ERS_EMPTY,
                                ((std::string)name )
    			)                        
    
    /*! \class is::SubscriptionNotFound
     *  This issue is reported if subscription which was requested does not exist.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				SubscriptionNotFound,
                                is::NotFound,
                                "Subscription for the IS information with the '" << criteria << "' criteria does not exist",
                                ERS_EMPTY,
                                ((std::string)criteria )
    			)                        
    
    /*! \class is::InfoNotCompatible
     *  This issue is reported if the IS information objects which is used for reading or updating
     *	information has type which is incompatible with the information types it is trying to deal with.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				InfoNotCompatible,
                                is::Exception,
                                "IS information '" << name << "' has incompatible type",
                                ERS_EMPTY,
                                ((std::string)name )
    			)                        
    
    /*! \class is::InfoAlreadyExist
     *  This issue is reported if one tries to create an IS information objects which already exists in the IS repository.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				InfoAlreadyExist,
                                is::Exception,
                                "IS information '" << name << "' already exists in the repository",
                                ERS_EMPTY,
                                ((std::string)name )
    			)                        
    
    /*! \class is::InvalidName
     *  This issue is reported if invalid name provided for IS information object.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				InvalidName,
                                is::Exception,
                                "IS information name '" << name << "' is invalid.\n"
                                "The valid name must have the '<REPOSITORY_NAME>.<INFOMATION_NAME>' format",
                                ERS_EMPTY,
                                ((std::string)name )
    			)                        
    
    /*! \class is::InvalidIterator
     *  This issue is reported if one tries to access element of an iterator at invalid position.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				InvalidIterator,
                                is::Exception,
				"Invalid position " << position << " in the iterator which contains " << length << " elements",
				ERS_EMPTY,
                                ((int)position )
				((int)length )
			)

    /*! \class is::InvalidCriteria
     *  This issue is reported if invalid ISCriteria object was used.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				InvalidCriteria,
                                is::Exception,
				"Invalid regular expression '" << expression << "' was used for the IS criteria",
				ERS_EMPTY,
				((std::string)expression )
			)

    /*! \class is::InvalidSubscriber
     *  This issue is reported if this subscriber can not be contacted by the IS server.
     */
    ERS_DECLARE_ISSUE_BASE(     is,
                                InvalidSubscriber,
                                is::Exception,
                                "The '" << name << "' IS server can't communicate with this subscriber, check the network configuration",
                                ERS_EMPTY,
                                ((std::string)name )
                        )

    /*! \class is::AlreadySubscribed
     *  This issue is reported if exactly the same subscription as was requested already exists.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				AlreadySubscribed,
                                is::Exception,
				"Subscription with the '" << expression << "' expression already exists",
				ERS_EMPTY,
				((std::string)expression )
			)

    /*! \class is::ServerNotFound
     *  This issue is reported if respective IS server is not running.
     */
    ERS_DECLARE_ISSUE_BASE(	is, 
    				ServerNotFound,
                                is::NotFound,
				"IS server '" << name << "' does not exists in the '" << partition << "' partition",
                                ERS_EMPTY,
                                ((std::string)name )
				((std::string)partition )
    			)

    ERS_DECLARE_ISSUE_BASE(	is,
				ServerAlreadyExist,
                                is::Exception,
				"IS server '" << name << "' already exists in the '" << partition << "' partition",
                                ERS_EMPTY,
				((std::string)name )
				((std::string)partition )
			)

    ERS_DECLARE_ISSUE_BASE(	is,
				InvalidServerName,
                                is::Exception,
				"Invalid name '" << name << "' has been given for the IS server in the '" << partition << "' partition",
                                ERS_EMPTY,
				((std::string)name )
				((std::string)partition )
			)

    ERS_DECLARE_ISSUE_BASE(     is,
                                ValueConversionFailed,
                                is::Exception,
                                "Value conversion for annotation has failed",
                                ERS_EMPTY,
                                ERS_EMPTY
                        )

}

#endif // IS_EXCEPTIONS_H
