/*
 * infowatcher.h
 *
 *  Created on: Nov 12, 2013
 *      Author: kolos
 */

#ifndef _IS_INFOWATCHER_H_
#define _IS_INFOWATCHER_H_

#include <functional>
#include <mutex>

#include <is/inforeceiver.h>

template<class I>
class ISInfoWatcher: public I,
    protected ISInfoReceiver
{
    static void empty_callback(const std::string &, const I &) { ; }
    
public:
    typedef std::function<void (const std::string & , const I & )> Callback;

    ISInfoWatcher(const IPCPartition & partition, const std::string & name,
	Callback callback = &ISInfoWatcher::empty_callback)
	: ISInfoReceiver(partition, false),
	  m_callback(callback)
    {
	scheduleSubscription(name, &ISInfoWatcher<I>::callback, this, { ISInfo::Created,
	    ISInfo::Updated, ISInfo::Subscribed });
    }

    ISInfoWatcher(const IPCPartition & partition, const std::string & server_name,
	const std::string & regex, 
	Callback callback = &ISInfoWatcher::empty_callback)
	: ISInfoReceiver(partition, false),
	  m_callback(callback)
    {
	scheduleSubscription(server_name, regex && ~this->type(), &ISInfoWatcher<I>::callback, this, {
	    ISInfo::Created, ISInfo::Updated, ISInfo::Subscribed });
    }

    I value() const
    {
	std::unique_lock < std::mutex > lock(m_mutex);
	return *this;
    }

private:
    void callback(const ISCallbackInfo * info)
    {
	std::unique_lock < std::mutex > lock(m_mutex);
	try
	{
	    info->value(*this);
	    m_callback(info->objectName(), *this);
	}
	catch (daq::is::Exception & ex)
	{
	    ers::log(ex);
	}
    }

private:
    mutable std::mutex m_mutex;
    Callback m_callback;
};

#endif /* _IS_INFOWATCHER_H_ */
