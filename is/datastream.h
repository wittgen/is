#ifndef IS_DATASTREAM_H
#define IS_DATASTREAM_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/datastream.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//              Classes odatastream and idatastream are used 
//              for murshalling and unmarshalling IS objects
//////////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <boost/shared_array.hpp>

#include <is/marshal.h>
#include <is/ostream.h>
#include <is/istream.h>
#include <is/type.h>
#include <is/info.h>

namespace is
{
  ///////////////////////////////////////////////////////////
  // Helper functions
  ///////////////////////////////////////////////////////////

  class odatastream
  {
    friend class idatastream;

  public:
    odatastream( );
    
    void data( is::data & data );
    
    template <typename T>
    void
    operator<<( const T & val )
    {
	is::marshal( val, m_data );
    }
    
    void
    operator<<( const ISInfo & val )
    {
    	ostream_tie<odatastream> out( *this );
        const_cast<ISInfo&>( val ).publishGuts( out );
    }
    
    template <typename T>
    void
    put( const T * val, size_t size, size_t , const T & )
    {
	marshaller<T>( m_data, size, (T*)val );
    }

    void
    put( const ISInfo * val, size_t items, size_t item_size, const ISInfo & )
    {
	(CORBA::ULong)items >>= m_data;
    	ostream_tie<odatastream> out( *this );
        char * values = reinterpret_cast<char*>( const_cast<ISInfo*>(val) );
	for ( size_t i = 0; i < items; i++ )
        {
	    reinterpret_cast<ISInfo*>(values)->publishGuts( out );
            values += item_size;
        }
    }

  private:
    ////////////////////////////////////
    // Disallow object copying
    ////////////////////////////////////
    odatastream( const odatastream & );
    odatastream& operator= ( const odatastream & );
    
    cdrMemoryStream	m_data;
  };


  class idatastream
  {
  public:
    idatastream( )
      : m_pos( 0 )
    { ; }
    
    idatastream( is::data & data );
    
    idatastream( is::odatastream & out );

    idatastream( const idatastream & );
    idatastream& operator=( const idatastream & );

    template <typename T>
    void
    operator>> ( T & val )
    {
	is::unmarshal( val, m_data );
	m_pos++;
    }
    
    void
    operator>> ( ISInfo & val )
    {
    	istream_tie<idatastream> in( *this );
        val.refreshGuts( in );
    }
    
    template <typename T>
    void
    get( T * val, size_t size, size_t )
    {
	unmarshaller<T>( m_data, size, val );
	m_pos++;
    }

    void
    get( ISInfo * val, size_t size, size_t item_size )
    {
        unmarshal_size(); // skip the number of elements field
        
    	istream_tie<idatastream> in( *this );
        char * values = reinterpret_cast<char*>(val);

	for ( size_t i = 0; i < size; i++ )
        {
	    reinterpret_cast<ISInfo*>(values)->refreshGuts( in );
            values += item_size;
        }
    }

    void rewind( );
        
    size_t read_size();
    
    size_t unmarshal_size();
    
    size_t position() const { return m_pos; }

  private:        
    cdrMemoryStream			m_data;
    boost::shared_array<CORBA::Octet>	m_data_holder;
    size_t				m_pos;
  };
}

#endif
