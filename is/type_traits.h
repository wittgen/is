#ifndef IS_TYPE_TRAITS_H
#define IS_TYPE_TRAITS_H

//////////////////////////////////////////////////////////////////////////////////////
//
//            is/typet_raits.h
//
//            private header file for the IS library
//
//            Sergei Kolos March 2004
//
//            description:
//		Type traits are used by templates to keep them generic
//		and flexible at the same time
//////////////////////////////////////////////////////////////////////////////////////
namespace is
{
    namespace type_traits
    {
	// This class converts copy constructor of type T to the default constructor
	// It's used to resize std::vector for a type which may not have copy constructor
	template <class T> struct copy_wrapper : T
	{
	    copy_wrapper() : T() { ; }
	    copy_wrapper( const copy_wrapper & ) : T() { ; }
	    copy_wrapper & operator=( const copy_wrapper & ) { return *this; }
	};
    }
}

#endif
