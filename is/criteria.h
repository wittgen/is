#ifndef IS_CRITERIA_H
#define IS_CRITERIA_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      criteria.h
//
//      public header file for the IS library
//
//      Sergei Kolos March 2005
//
//      description:
//		ISCriteria used for making IS subscriptions
//////////////////////////////////////////////////////////////////////////////////////

#include <string>
#include <is/is.hh>
#include <is/type.h>
#include <is/exceptions.h>

class ISCriteria : public is::criteria
{
  public:
    enum Logic { AND, OR };
    
    explicit ISCriteria( const std::string & reg_exp ) ;
    ISCriteria( const ISType & type ) ;
    ISCriteria( const std::string & reg_exp, const ISType & type, Logic logic = AND ) ;

    std::string toString() const;

  private:
    void constructor(	const std::string & reg_exp,
			bool p_ignore = false, 
			const ISType & type = ISType::Null, 
			bool t_ignore = true,
			Logic logic = AND ) ;
};

std::ostream & operator<<( std::ostream & out, const is::criteria & crt );

ISCriteria operator&&( const std::string & reg_exp, const ISType & type );
ISCriteria operator&&( const ISType & type, const std::string & reg_exp );
ISCriteria operator||( const std::string & reg_exp, const ISType & type );
ISCriteria operator||( const ISType & type, const std::string & reg_exp );

#endif
