/*
 * annotation.h
 *
 *  Created on: Apr 29, 2019
 *      Author: kolos
 */

#ifndef IS_ANNOTATION_H_
#define IS_ANNOTATION_H_

#include <string>
#include <boost/lexical_cast.hpp>

class ISAnnotation {
public:
    ISAnnotation() = default;

    template <class T>
    explicit ISAnnotation(const T & value)
    try :
        m_value(boost::lexical_cast<std::string>(value)) {
    } catch (const boost::bad_lexical_cast & ex) {
        throw daq::is::ValueConversionFailed(ERS_HERE, ex);
    }

    bool operator==(const ISAnnotation & a) const {
        return m_value == a.m_value;
    }

    bool operator!=(const ISAnnotation & a) const {
        return m_value != a.m_value;
    }

    const std::string & data() const {
        return m_value;
    }

    template <class T>
    void setValue(const T & value) {
        try {
            m_value = boost::lexical_cast<std::string>(value);
        } catch (const boost::bad_lexical_cast & ex) {
            throw daq::is::ValueConversionFailed(ERS_HERE, ex);
        }
    }

    template <class T>
    T getValue() const {
        try {
            return boost::lexical_cast<T>(m_value);
        } catch (const boost::bad_lexical_cast & ex) {
            throw daq::is::ValueConversionFailed(ERS_HERE, ex);
        }
    }

private:
    std::string m_value;
};

inline std::ostream & operator<<(std::ostream & out, const ISAnnotation & a) {
    return (out << a.data());
}

#endif /* IS_ANNOTATION_H_ */
