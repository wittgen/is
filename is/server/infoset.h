#ifndef IS_SERVER_INFOSET_H
#define IS_SERVER_INFOSET_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server/infoset.h
//
//      private header file for the IS library
//
//      Sergei Kolos Mai 2013
//
//      description:
//              Class ISInfoSet defines contanser for the IS objects
//////////////////////////////////////////////////////////////////////////////////////

#include <cstring>

#include <boost/shared_ptr.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include <is/server/infoholder.h>

using namespace boost::multi_index;

struct NameHasher
{
    std::size_t operator()(const char * x) const
    {
	std::size_t seed = 0;
	for (; *x; ++x)
	    boost::hash_combine(seed, *x);
	return seed;
    }
};

struct NameEqual
{
    bool operator()(const char * x, const char * y) const
    {
	return !std::strcmp(x, y);
    }
};

struct NameLess
{
    bool operator()(const char * x, const char * y) const
    {
	return (std::strcmp(x, y) < 0);
    }
};

typedef boost::multi_index_container<
	boost::shared_ptr<ISInfoHolder>,
        indexed_by<
	    hashed_unique<
		const_mem_fun<ISInfoHolder, const char *, &ISInfoHolder::name>,
		NameHasher, NameEqual>,
	    ordered_unique<
		const_mem_fun<ISInfoHolder, const char *, &ISInfoHolder::name>,
		NameLess>
	>
> ISInfoSet;

typedef ISInfoSet::nth_index<1>::type ISInfoSetSorted;

#endif
