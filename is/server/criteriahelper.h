#ifndef IS_CRITERIA_HELPER_H
#define IS_CRITERIA_HELPER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server/criteriahelper.h
//
//      private header file for the IS library
//
//      Sergei Kolos December 2010
//
//      description:
//              Class ISCriteriaHelper is a wrapper above is::criteria
//////////////////////////////////////////////////////////////////////////////////////

#include <functional>

#include <boost/noncopyable.hpp>
#include <boost/regex.hpp>

#include <owl/regexp.h>
#include <is/is.hh>

namespace is
{
    bool types_equal( const is::type & t1, const is::type & t2 );

    bool super_sub_type( const is::type & t1, const is::type & t2 );
}

class ISCriteriaHelper : boost::noncopyable
{
    typedef std::function<bool ( const char * , const is::type & )> Matcher;
    typedef std::function<bool ( const char * )> NameMatcher;
    typedef std::function<bool ( const is::type & )> TypeMatcher;
      
  public:
    ISCriteriaHelper( const is::criteria & criteria );
    
    const is::criteria & criteria() const 
    { return m_criteria; }
    
    const std::string & toString() const;

    bool match( const char * name, const is::type & type ) const
    { return m_match( name, type ); }
    
  private:
    bool AND( NameMatcher name_matcher, TypeMatcher type_matcher, 
    		const char * name, const is::type & type ) const
    { return ( name_matcher(name) && type_matcher(type) ); }
    
    bool OR( NameMatcher name_matcher, TypeMatcher type_matcher, 
    		const char * name, const is::type & type ) const
    { return ( name_matcher(name) || type_matcher(type) ); }
    
    bool matchNameBoost( const char * name ) const
    { return boost::regex_match( name, m_boost_regex ); }
    
    bool matchName( const char * name ) const
    { return m_owl_regex.exact_match( name ); }
    
    bool matchTypeBase( const is::type & type ) const
    { return super_sub_type( m_criteria.type_.value_, type ); }
    
    bool matchTypeExact( const is::type & type ) const
    { return is::types_equal( m_criteria.type_.value_, type ); }
    
    bool matchTypeNot( const is::type & type ) const
    { return !is::types_equal( m_criteria.type_.value_, type ); }

    bool matchTypeNotBase( const is::type & type ) const
    { return !super_sub_type( m_criteria.type_.value_, type ); }

  private:
    is::criteria m_criteria;
    std::string	 m_string;
    boost::regex m_boost_regex;
    OWLRegexp	 m_owl_regex;
    Matcher	 m_match;
};

#endif
