#ifndef m___IS_REPLICA_H_
#define m___IS_REPLICA_H_

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server/replica.h
//
//      private header file for the IS server
//
//      Sergei Kolos January 2008
//
//      description:
//              Class ISReplicaObject implements the IS server facility
//////////////////////////////////////////////////////////////////////////////////////

#include <ipc/alarm.h>

#include <is/server/repository.h>

class ISReplica
{  
    friend class ISReplicaObject;
    
  public:
    ISReplica(	const IPCPartition & partition,
    		const std::string & name,
                const std::string & restore_file,
                const std::string & backup_file,
                int backup_period );
    
    ~ISReplica();

  private:
    void backup( );

    void restore( const std::string & restore_file );
    
    bool periodicAction();
    
  private:
    ISRepository *	m_repository;
    std::string 	m_backup_file;
    int 		m_backup_period;
    IPCAlarm *		m_backup_alarm;
};

#endif
