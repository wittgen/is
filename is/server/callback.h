#ifndef IS_SERVER_CALLBACK_H
#define IS_SERVER_CALLBACK_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      receiver.h
//
//      private header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		declaration of the ISReceiver class
//////////////////////////////////////////////////////////////////////////////////////
#include <bitset>
#include <string>

#include <is/is.hh>

////////////////////////////////////////////////////////////////////////////////////////////////
// Class ISCallback defines the interface to the is::callback reference holder
////////////////////////////////////////////////////////////////////////////////////////////////
class ISReceiver;

struct ISCallback
{
    static ISCallback * create(const std::string & id, int event_mask,
	is::callback_ptr callback, const boost::shared_ptr<ISReceiver> & r);

    ISCallback(const std::string & id, int event_mask)
	: m_id(id),
	  m_event_mask(event_mask)
    {
	;
    }

    virtual ~ISCallback()
    {
	;
    }

    const std::string & getID() const
    {
	return m_id;
    }

    const std::bitset<4> & eventMask() const
    {
	return m_event_mask;
    }

    virtual bool inform(const is::info & i, is::reason r) const = 0;

    virtual bool is_equivalent(is::callback_ptr cb) const = 0;

    virtual void dump(std::ostream & out) = 0;

protected:
    const std::string m_id;
    const std::bitset<4> m_event_mask;
};

#endif
