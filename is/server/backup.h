#include <iostream>
#include <string>
#include <fstream>

#include <is/is.hh>

class bofstream : public std::ofstream
{
public:
    bofstream(const std::string & fname);

    ~bofstream();

private:
    std::string m_name;
    std::string m_bck_name;
};

class bifstream : public std::ifstream
{
public:
    bifstream(const std::string & fname);

private:
    std::string m_name;
    std::string m_bck_name;
};

std::ostream & operator<<=( std::ostream & out, const std::string & str );

std::istream & operator>>=( std::istream & in, std::string & str );

std::ostream & operator<<=( std::ostream & out, const char * str );

std::ostream & operator<<= ( std::ostream & out, const CORBA::String_var & str );

std::istream & operator>>=( std::istream & in, CORBA::String_var & str );

std::ostream & operator<<=( std::ostream & out, is::callback_ptr cb );

std::istream & operator>>=( std::istream & in, is::callback_var & cb );

std::ostream & operator<<=( std::ostream & out, const is::data & dd );

std::istream & operator>>=( std::istream & in, is::data & dd );

std::ostream & operator<<=( std::ostream & out, const is::address & dd );

std::istream & operator>>=( std::istream & in, is::address & dd );

std::ostream & operator<<=( std::ostream & out, const is::type & tt );

std::istream & operator>>=( std::istream & in, is::type & tt );

std::ostream & operator<<=( std::ostream & out, const is::value & vv );

std::istream & operator>>=( std::istream & in, is::value & vv );

std::ostream & operator<<=( std::ostream & out, const CORBA::String_member & vv );

std::istream & operator>>=( std::istream & in, CORBA::String_member & vv );

std::ostream & operator<<=( std::ostream & out, const is::criteria & crt );

std::istream & operator>>=( std::istream & in, is::criteria & crt );
