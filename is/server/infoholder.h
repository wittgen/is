#ifndef IS_INFOHOLDER_H
#define IS_INFOHOLDER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/internal/infoholder.h
//
//      private header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//              Class ISInfoHolder provides storage for IS objects
//
//////////////////////////////////////////////////////////////////////////////////////

#include <list>
#include <string>
#include <thread>

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

#include <is/is.hh>
#include <is/server/callback.h>
#include <is/server/callbackholder.h>
#include <is/server/mirror.h>
#include <is/server/receiverset.h>

class ISCriteriaHelper;

using namespace boost::multi_index;

class ISInfoHolder : public ISCallbackHolder
{
  public:

    ISInfoHolder( const is::info & info, const ISReceiverSet & receivers,
		   ISReceiverSetLock & receivers_mutex, ISMirror & m );
    ISInfoHolder( const std::string & name, const is::type & type, ISMirror & m );

    ~ISInfoHolder( );

    const char * name() const
    { return m_name.c_str(); }

    const is::type & type( ) const
    { return m_type; }

    is::data * data() const;

    is::data * data( CORBA::Long tag ) const;

    void addCallback( const boost::shared_ptr<ISCallback> & r );

    void getTags( is::tag_list_out & tags ) const;

    void getLastValue( is::info & info ) const;

    void getLastValues( is::info_history & info, int num, is::sorted order ) const {
        (order == is::ByTime ? getLastValues<1>(info, num) : getLastValues<0>(info, num));
    }

    void getTagValue( CORBA::Long tag, is::info & info ) const;

    void read( std::istream & in );

    void update( const is::info & info, bool keep_history, bool use_tag );

    void write( std::ostream & out ) const;

  private:
    template <int>
    void getLastValues( is::info_history & info, int num ) const;

    struct attr_tag
    {
	typedef CORBA::Long result_type;
	result_type operator()(const is::value & r) const { return r.attr.tag; }
    };

    struct attr_time
    {
	typedef is::time result_type;
	result_type operator()(const is::value & r) const { return r.attr.time; }
    };

    typedef boost::multi_index_container<
	is::value,
	indexed_by<
	    ordered_unique<attr_tag>,
	    ordered_non_unique<attr_time>
	>
    > Values;

    typedef Values::nth_index<1>::type ValuesTimeOrdered;

    typedef std::list<boost::weak_ptr<ISCallback>> Callbacks;

  private:
    mutable std::mutex	m_data_mutex;
    Values		m_value;
};

template <int INDEX>
void
ISInfoHolder::getLastValues(is::info_history & info, int history_length) const
{
    info.name = m_name.c_str();
    info.type = m_type;

    std::unique_lock<std::mutex> lock(m_data_mutex);
    CORBA::ULong end = history_length < 0 || history_length > (int) m_value.size()
                ? m_value.size()
                : history_length == 0 ? 1 : (CORBA::ULong) history_length;

    info.values.length(end);

    CORBA::ULong i = 0;
    for (auto it = m_value.get<INDEX>().rbegin();
        it != m_value.get<INDEX>().rend() && i != end; ++it, ++i)
    {
        info.values[i].attr = it->attr;
        if (history_length) {
            info.values[i].data = it->data;
        }
    }
}

#endif
