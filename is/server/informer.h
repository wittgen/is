#ifndef IS_INFORMER_H
#define IS_INFORMER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server/informer.h
//
//      private header file for the IS library
//
//      Sergei Kolos December 2001
//
//      description:
//              Class ISInformer is used for notifying IS server subscribers
//////////////////////////////////////////////////////////////////////////////////////

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

#include <boost/noncopyable.hpp>

#include <ers/ers.h>

#include <is/is.hh>
#include <is/server/callback.h>
#include <is/server/config.h>

ERS_DECLARE_ISSUE( is,
    SlowSubscriber,
    "The '" << subscriber << "' subscriber of the '" << sever_name
    << "' IS server is not able to cope with the callbacks rate, "
    << "some callbacks will be dropped",
    ((std::string)subscriber )
    ((std::string)sever_name )
    )

class ISInformer: public boost::noncopyable
{
public:
    ISInformer(const std::string & server, is::callback_ptr r);

    virtual ~ISInformer();

    const std::string & getReceiverName() const
    {
	return m_receiver_name;
    }

    const std::string & getReceiverId() const
    {
	return m_receiver_id;
    }

    const std::string & getServerName() const
    {
	return m_server_name;
    }

    bool inform(const is::info_callback_var & c, const is::info & i, is::reason r)
    {
	return send(c, i, r);
    }

    bool inform(const is::event_callback_var & c, const is::info & i, is::reason r)
    {
	return send(c, reinterpret_cast<const is::event&>(i), r);
    }

    virtual bool failed()
    {
	return m_failed;
    }

protected:
    ISInformer(const std::string & server, const std::string & receiver);

private:
    enum Status
    {
	Success, Timeout, Error
    };

    void thread_wrapper();

    void handleTimeout();

    bool handleSystemException(CORBA::SystemException & ex);

    template<typename C, typename E>
    bool send(const C & callback, const E & event, is::reason reason);

    template<typename C, typename E>
    void addToQueue(const C & callback, const E & event, is::reason reason);

    struct Invocation
    {
	Invocation(const is::info_callback_var & c, const is::info & info, is::reason reason)
	    : m_event(),
	      m_info(info),
	      m_callback(
		  std::bind(&is::_objref_info_callback::receive, c, std::cref(m_info),
		      reason))
	{
	    ;
	}

	Invocation(const is::event_callback_var & c, const is::event & event, is::reason reason)
	    : m_event(event),
	      m_info(),
	      m_callback(
		  std::bind(
		      &is::_objref_event_callback::receive, c,
		      std::cref(m_event), reason))
	{
	    ;
	}

	void operator()()
	{
	    m_callback();
	}

    private:
	typedef std::function<void()> Callback;

	const is::event m_event;
	const is::info m_info;
	Callback m_callback;
    };

protected:
    unsigned short m_max_active_callbacks;
    std::atomic_ushort m_active_callbacks;
    bool m_failed;
    unsigned int m_timedout;
    bool m_terminated;
    bool m_slow_reported;
    uint64_t m_dropped_counter;
    std::mutex m_mutex;
    std::condition_variable m_condition;
    std::string m_server_name;
    std::string m_receiver_name;
    std::string m_receiver_id;
    std::queue<std::shared_ptr<Invocation>> m_invocations;
    std::thread m_thread;
};

template<typename C, typename E>
void ISInformer::addToQueue(const C & callback, const E & event, is::reason reason)
{
    {
	std::unique_lock<std::mutex> lock(m_mutex);
	if (m_terminated)
	    return ;

	size_t size = m_invocations.size();
	if (size < ISConfig::MaxCallbacksQueueSize)
	{
	    m_invocations.push(std::shared_ptr<Invocation>(new Invocation(callback, event, reason)));
	    if (!size) {
		m_condition.notify_one();
	    }
	    return ;
	}

	std::queue<std::shared_ptr<Invocation>> ii;
	m_invocations.swap(ii);
	m_invocations.push(std::shared_ptr<Invocation>(new Invocation(callback, event, reason)));

	m_dropped_counter += ii.size();
	lock.unlock();
    }

    ERS_LOG( m_dropped_counter << " callbacks have been dropped for the '"
	<< m_receiver_name << "' receiver");
    ers::warning(is::SlowSubscriber(ERS_HERE, m_receiver_name, m_server_name));
}

template<typename C, typename E>
bool ISInformer::send(const C & callback, const E & event, is::reason reason)
{
    if (m_failed)
	return false;

    if (!m_timedout && m_active_callbacks < m_max_active_callbacks
	&& reason < is::Deleted)
    {
	try
	{
	    ++m_active_callbacks;
            callback->receive(event, reason);
	    --m_active_callbacks;
	}
	catch (CORBA::TIMEOUT &)
	{
	    --m_active_callbacks;
	    handleTimeout();
	    addToQueue(callback, event, reason);
	}
	catch (CORBA::SystemException & ex)
	{
	    --m_active_callbacks;
	    bool timeout = handleSystemException(ex);
	    if (timeout) {
	        addToQueue(callback, event, reason);
	    } else {
	        return false;
	    }
	}
    }
    else
    {
	addToQueue(callback, event, reason);
    }

    return true;
}

#endif
