#ifndef IS_MARSHAL_H
#define IS_MARSHAL_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/marshal.h
//
//      public header file for the IS library
//
//      Sergei Kolos October 2004
//
//      description:
//              Provides type marshallers for efficient data marshalling.
//////////////////////////////////////////////////////////////////////////////////////
#include <boost/type_traits/is_class.hpp>

#include <omniORB4/CORBA.h>
#include <owl/time.h>

namespace is
{
  void marshal( bool b, cdrStream & in );
  void marshal( char b, cdrStream & in );
  void marshal( unsigned char b, cdrStream & in );
  void marshal( short b, cdrStream & in );
  void marshal( unsigned short b, cdrStream & in );
  void marshal( int b, cdrStream & in );
  void marshal( unsigned int b, cdrStream & in );
  void marshal( int64_t b, cdrStream & in );
  void marshal( uint64_t b, cdrStream & in );
  void marshal( float b, cdrStream & in );
  void marshal( double b, cdrStream & in );
  void marshal( const std::string & str, cdrStream & in );
  void marshal( const OWLDate & date, cdrStream & in );
  void marshal( const OWLTime & time, cdrStream & in );

  void unmarshal( bool & b, cdrStream & out );
  void unmarshal( char & b, cdrStream & out );
  void unmarshal( unsigned char & b, cdrStream & out );
  void unmarshal( short & b, cdrStream & out );
  void unmarshal( unsigned short & b, cdrStream & out );
  void unmarshal( int & b, cdrStream & out );
  void unmarshal( unsigned int & b, cdrStream & out );
  void unmarshal( int64_t & b, cdrStream & out );
  void unmarshal( uint64_t & b, cdrStream & out );
  void unmarshal( float & b, cdrStream & out );
  void unmarshal( double & b, cdrStream & out );
  void unmarshal( std::string & str, cdrStream & out );
  void unmarshal( OWLDate & date, cdrStream & out );
  void unmarshal( OWLTime & time, cdrStream & out );
  
  /////////////////////////////////////////////////
  // Use different sequences for efficiency
  /////////////////////////////////////////////////
  template <typename T>
  struct sequence
  {
    typedef _CORBA_Unbounded_Sequence_w_FixSizeElement<T,sizeof(T),sizeof(T)> Type;
    typedef T		DataType; 
  };
  
  template <>
  struct sequence<bool>
  { 
    typedef _CORBA_Sequence_Boolean	Type; 
    typedef CORBA::Boolean		DataType; 
  };
  
  template <>
  struct sequence<char>
  {
    typedef _CORBA_Sequence_Octet	Type;
    typedef CORBA::Char			DataType; 
  };
  
  template <>
  struct sequence<unsigned char>
  {
    typedef _CORBA_Sequence_Octet	Type;
    typedef CORBA::Octet		DataType; 
  };
  
  template <>
  struct sequence<int>
  {
    typedef CORBA::LongSeq		Type;
    typedef CORBA::Long			DataType; 
  };
  
  template <>
  struct sequence<unsigned int>
  {
    typedef CORBA::ULongSeq		Type;
    typedef CORBA::ULong		DataType; 
  };
  
  /////////////////////////////////////////////////
  // Marshalling wrappers
  /////////////////////////////////////////////////
  template <typename T, bool a_class = boost::is_class<T>::value>
  struct marshaller : public sequence<T>::Type
  {
    inline marshaller( cdrStream & out, CORBA::ULong len, T * value )
      : sequence<T>::Type( len, len, (typename sequence<T>::DataType*)value, false )
    { (*this) >>= out; }
  };

  template <typename T>
  struct marshaller<T,true>
  {
    inline marshaller( cdrStream & out, CORBA::ULong len, T * value )
    {
    	len >>= out;
	for( CORBA::ULong i = 0; i < len; i++ )
	    marshal( value[i], out );
    }
  };

  /////////////////////////////////////////////////
  // Unmarshalling wrappers
  /////////////////////////////////////////////////
  template <typename T, bool a_class = boost::is_class<T>::value>
  struct unmarshaller : public sequence<T>::Type
  {
    inline unmarshaller( cdrStream & in, CORBA::ULong len, T *& value )
    {
	sequence<T>::Type::replace( len, len, (typename sequence<T>::DataType*)value, 0 );
	(*this) <<= in;
    }
  };

  template <typename T>
  struct unmarshaller<T,true>
  {
    inline unmarshaller( cdrStream & in, CORBA::ULong , T *& value )
    {
    	CORBA::ULong len;
        len <<= in;
	for( CORBA::ULong i = 0; i < len; i++ )
	    unmarshal( value[i], in );
    }
  };
}

inline void 
is::marshal( bool b, cdrStream & out )
{
    out.marshalBoolean( b );
}

inline void 
is::marshal( char b, cdrStream & out )
{
    out.marshalChar( b );
}

inline void 
is::marshal( unsigned char b, cdrStream & out )
{
    out.marshalOctet( b );
}

inline void 
is::marshal( short d, cdrStream & out )
{
    ((CORBA::Short)d) >>= out;
}

inline void 
is::marshal( unsigned short d, cdrStream & out )
{
    ((CORBA::UShort)d) >>= out;
}

inline void 
is::marshal( int d, cdrStream & out )
{
    ((CORBA::Long)d) >>= out;
}

inline void 
is::marshal( unsigned int d, cdrStream & out )
{
    ((CORBA::ULong)d) >>= out;
}

inline void 
is::marshal( int64_t d, cdrStream & out )
{
    ((CORBA::LongLong)d) >>= out;
}

inline void 
is::marshal( uint64_t d, cdrStream & out )
{
    ((CORBA::ULongLong)d) >>= out;
}

inline void 
is::marshal( float d, cdrStream & out )
{
    d >>= out;
}

inline void 
is::marshal( double d, cdrStream & out )
{
    d >>= out;
}

inline void 
is::marshal( const std::string & str, cdrStream & out )
{
    out.marshalRawString( str.c_str() );
}

inline void 
is::marshal( const OWLDate & date, cdrStream & out )
{
    (CORBA::ULong)date.c_time() >>= out;
}

inline void 
is::marshal( const OWLTime & time, cdrStream & out )
{
    (CORBA::LongLong)time.total_mksec_utc() >>= out;
}

inline void 
is::unmarshal( bool & b, cdrStream & in )
{
    b = in.unmarshalBoolean( );
}

inline void 
is::unmarshal( char & b, cdrStream & in )
{
    b = in.unmarshalChar( );
}

inline void 
is::unmarshal( unsigned char & b, cdrStream & in )
{
    b = in.unmarshalOctet( );
}

inline void 
is::unmarshal( short & b, cdrStream & in )
{
    CORBA::Short l;
    l <<= in;
    b = l;
}

inline void 
is::unmarshal( unsigned short & b, cdrStream & in )
{
    CORBA::UShort ul;
    ul <<= in;
    b = ul;
}

inline void 
is::unmarshal( int & b, cdrStream & in )
{
    CORBA::Long l;
    l <<= in;
    b = l;
}

inline void 
is::unmarshal( unsigned int & b, cdrStream & in )
{
    CORBA::ULong ul;
    ul <<= in;
    b = ul;
}

inline void 
is::unmarshal( int64_t & b, cdrStream & in )
{
    CORBA::LongLong l;
    l <<= in;
    b = l;
}

inline void 
is::unmarshal( uint64_t & b, cdrStream & in )
{
    CORBA::ULongLong ul;
    ul <<= in;
    b = ul;
}

inline void 
is::unmarshal( float & b, cdrStream & in )
{
    b <<= in;
}

inline void 
is::unmarshal( double & b, cdrStream & in )
{
    b <<= in;
}

inline void 
is::unmarshal( std::string & str, cdrStream & in )
{
    char * _ptr = in.unmarshalRawString();
    str = _ptr;
    delete[] _ptr;
}

inline void 
is::unmarshal( OWLDate & date, cdrStream & in )
{
    CORBA::ULong t;
    t <<= in;
    date = OWLDate( t );
}

inline void 
is::unmarshal( OWLTime & time, cdrStream & in )
{
    CORBA::LongLong t;
    t <<= in;
    time = OWLTime( t/1000000, t%1000000 );
}

#endif
