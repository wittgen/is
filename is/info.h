#ifndef IS_INFO_H
#define IS_INFO_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/info.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		ISInfo is the base class for IS objects definition
//////////////////////////////////////////////////////////////////////////////////////
#include <map>

#include <is/type.h>
#include <is/istream.h>
#include <is/ostream.h>
#include <is/infoprovider.h>
#include <is/exceptions.h>
#include <is/annotation.h>

namespace is
{
    class idatastream;
    class odatastream;
}

class ISInfo
{
    friend class ISCallbackInfo;
    friend class ISInfoIterator;
    friend class ISInfoEnumeration;
    friend class ISInfoDictionary;
    friend class ISNamedInfo;
    friend class ISInfoAny;
    friend class ISInfoDynAny;
    friend class ISInfoStream;
    template <class T> friend class ISProxy;
    
  public:    
    typedef std::map<std::string, ISAnnotation> Annotations;
    
    typedef is::reason Reason;

    static const Reason Created = is::Created;
    static const Reason Updated = is::Updated;
    static const Reason Deleted = is::Deleted;
    static const Reason Subscribed = is::Subscribed;
    
    typedef is::sorted HistorySorted;
    static const HistorySorted ByTime = is::ByTime;
    static const HistorySorted ByTag = is::ByTag;

    virtual ~ISInfo ( )
    { ; }    
    
    virtual void publishGuts ( ISostream & out ) = 0;
    virtual void refreshGuts ( ISistream & in  ) = 0;
    
    virtual std::ostream & print( std::ostream & out ) const;
        
    const ISType & type ( ) const;
    
    const OWLTime & time ( ) const
    { return m_time; }
    
    int tag ( ) const
    { return m_tag; }
    
    bool providerExist( ) const;
    
    void sendCommand( const std::string & command ) const ;
    
    std::string provider()
    { return ISInfoProvider::makeConnectString( m_address ); }
    
    template <class T>
    bool instanceOf( )
    { return ( ( dynamic_cast<T*>( this ) != 0 ) ); }

    template <class T>
    void setAnnotation(const std::string & name, const T & value) {
    	m_annotations[name].setValue(value);
    }

    ISAnnotation & getAnnotation(const std::string & name);

    const ISAnnotation & getAnnotation(const std::string & name) const;

    Annotations & annotations() {
    	return m_annotations;
    }

    const Annotations & annotations() const {
        return m_annotations;
    }

  protected:
    ISInfo ( const std::string & name = ISType::unknown )
      : m_type( name ),
	m_time( OWLTime::Microseconds ),
        m_tag( 0 ),
        m_polymorphic( false )
    { ; }

  private:
    void fromWire(  const IPCPartition & partition,
    		    const char * name,
                    const is::type & type,
                    const is::value & value,
                    is::idatastream & in );

    void toWire( const std::string & object_name,
                 is::info & info,
                 is::odatastream & stream ) const;
    
  private:
    IPCPartition        m_partition;
    ISType	        m_type;
    mutable OWLTime	m_time;
    mutable int		m_tag;
    mutable std::string	m_name;
    mutable is::address	m_address;
    bool		m_polymorphic;
    Annotations	        m_annotations;
};

std::ostream& operator<< ( std::ostream& , ISInfo::Reason );
std::ostream& operator<< ( std::ostream& , const ISInfo & );

#endif    // define IS_INFO_H
