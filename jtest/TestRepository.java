import ipc.*;
import is.*;
import is.test.*;
import java.lang.reflect.*;
 
public class TestRepository implements CommandListener, Runnable
{
    ipc.Partition partition;
    String servername;
    
    TestRepository(ipc.Partition partition, String servername) {
	this.partition = partition;
	this.servername = servername;
    }
    
    public void command( String name, String cmd )
    {
	System.out.println( "Command " + cmd + " received for the " + name + " info " );
    }
    
    public void run() {
	
	LargeInfo it = new LargeInfo( );
	Repository rep = new Repository( partition );
	String name = servername + "." + Thread.currentThread().getName();
	for (int n = 0; n < 10; ++n) {
	    System.out.print( "Publishing " + name + " object:" );
	    long s = System.nanoTime();
	    try
	    {
		rep.checkin( name, it );
	    }
	    catch ( Exception ex )
	    {
		System.err.println( Thread.currentThread().getName() + ">> " + ex.getMessage() );
		ex.printStackTrace();
	    }
	    long e = System.nanoTime();
	    System.out.println(" time = " + (e-s)/1000000.);		
	    
	    try {
		Thread.sleep( 5000 );
	    }
	    catch ( Exception ex )
	    {
		System.err.println( ex.getMessage() );
	    }
	}
    }
    
    public static void main(String args[])
    {	
	if ( args.length < 1 )
	{
	    System.err.println( "You must provide the IS server name." );
	    return;
	}
	
	ipc.Partition partition = args.length > 1 ? new ipc.Partition(args[1]) : new ipc.Partition();
	int threads = args.length > 2 ? Integer.parseInt(args[2]) : 1;
	
	for (int i = 0; i < threads; ++i ) {
	    (new Thread(new TestRepository(partition, args[0]))).start();
	}
    }
}

