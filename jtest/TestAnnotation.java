import java.util.Map;

import ipc.*;
import is.*;
import is.test.*;
 
public class TestAnnotation
{            
    public static void main(String args[])
    {	
	if ( args.length < 1 )
	{
	    System.err.println( "You must provide the IS server name." );
	    return;
	}
	
	ipc.Partition partition = args.length > 1 ? new ipc.Partition(args[1]) : new ipc.Partition();
	String serverName = args[0];
        Repository rep = new Repository( partition );
        String name = serverName + ".TestAnnotation";

        try {
            LargeInfo info = new LargeInfo();
            rep.checkin(name, info);
            
            rep.getValue(name, info);
            System.out.println("Annotations size = " + info.getAnnotations().size());
            
            info.setAnnotation("ann1", 10);
            info.setAnnotation("ann2", 10.1234567);
            rep.checkin(name, info);
            
            LargeInfo info1 = new LargeInfo();
            rep.getValue(name, info1);
            System.out.println("Annotations size = " + info1.getAnnotations().size());
            System.out.println("Annotations are:");
            for (Map.Entry<String, Annotation> e : info1.getAnnotations().entrySet()) {
                System.out.println(e.getKey() + " = " + e.getValue().data());
            }
            System.out.println("Annotations are:");
            System.out.println("ann1 = " + info1.getAnnotation("ann1").getValue(Integer::valueOf));
            System.out.println("ann2 = " + info1.getAnnotation("ann2").getValue(Double::valueOf));
        }
        catch ( Exception ex ) {
            System.err.println( ex.getMessage() );
            ex.printStackTrace();
        }
	
    }
}

