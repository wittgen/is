import ipc.*;
import is.*;
import is.test.*;
import java.lang.reflect.*;
 
public class TestNamedInfo
{
    public static void main(String args[])
    {	
	if ( args.length < 1 )
	{
	    System.err.println( "You must provide the IS server name." );
	    System.exit( 2 );
	}
	
	String name = args[0] + ".2";
	try
	{
	    Partition p = ( args.length < 2 ) ? new Partition() : new Partition( args[1] );
	    LargeInfoNamed it = new LargeInfoNamed( p, name );
	    it.checkin();
	    it.checkout();
	    print( System.out , it );
	    System.out.println( "\n" );
	    System.out.println( "Testing commands ... " );
	    
	    it.sendCommand( "test_command_1" );
	    it.sendCommand( "test_command_2" );
	    it.sendCommand( "test_command_3" );
	    System.out.println( "done" );
	
	    AnyInfo ai = new AnyInfo();
	    Repository repository = new Repository( p );
	    repository.getValue( name, ai );
	    
	    System.out.println( "Testing commands ... " );
	    
	    ai.sendCommand( "test_command_1" );
	    ai.sendCommand( "test_command_2" );
	    ai.sendCommand( "test_command_3" );
	    System.out.println( "done" );
	
	    System.out.println( "Info has type \"" + ai.getType().getName() + "\"" );
	
	    InfoDocument id = new InfoDocument( p, ai );
	
	    for( int i = 0; i < ai.getAttributeCount(); i++ )
	    {
		java.lang.Object attr = ai.getAttribute(i);
		InfoDocument.Attribute adoc = id.getAttribute(i);
		System.out.print( "Attribute \"" + adoc.getName() + "\" of type \"" 
			     + adoc.getType() + "\" - "
			     + adoc.getDescription() + " : " ); 
	    
		if ( attr.getClass().isArray() )
		{
		    for ( int j = 0; j < Array.getLength( attr ); j++ )
		    {
			System.out.print( Array.get( attr, j ) + " " );
		    }
		    System.out.println();
		}
		else
		{
		    System.out.println( attr );
		}
	    }
	
	    System.err.println( "Updating information" );
	    it.checkin();
	    System.out.println( "Test completed successfully\n\n" );
	}
	catch ( is.RepositoryNotFoundException e )
	{
	    System.err.println( "ERROR::IS server not found" );
	    System.exit( 1 );
	}
	catch ( is.ProviderNotFoundException e )
	{
	    System.err.println( "ERROR::Information provider not found" );
	    System.exit( 1 );
	}
	catch ( is.InfoNotFoundException e )
	{
	    System.err.println( "ERROR::Info not found" );
	    System.exit( 1 );
	}
	catch ( is.InfoNotCompatibleException e )
	{
	    System.err.println( "ERROR::Info is not compatible " );
	    System.exit( 1 );
	}
	catch ( is.UnknownTypeException e )
	{
	    System.err.println( "ERROR::No information about IS object type" );
	}
	catch ( Error e )
	{
	    System.err.println( "Java error::" );
	    e.printStackTrace();
	    System.exit( 1 );
	}
    }
    
    
    static void printArray( java.io.PrintStream out, java.lang.Object arr )
    {
	out.print( arr.getClass().getComponentType().getName() + "[" + Array.getLength( arr ) + "] = " );
	for ( int i = 0; i < Array.getLength( arr ); i++ )
	{
	    out.print( Array.get( arr, i ) + " " );
	}
	out.println();
    }
    
    static void print( java.io.PrintStream out , LargeInfoNamed ist )
    {
	out.println( ist.bool_ );
	out.println( ist.char_ );
	out.println( ist.short_ );
	out.println( ist.int_ );
	out.println( ist.uchar_ );
	out.println( ist.ushort_ );
	out.println( ist.uint_ );
	out.println( ist.float_ );
	out.println( ist.double_ );
	out.println( ist.string_ );
	out.println( ist.date_ );
	out.println( ist.time_ );
	out.println( ist.enum_ );
	printArray( out, ist.bool_a_ );
	printArray( out, ist.char_a_ );
	printArray( out, ist.short_a_ );
	printArray( out, ist.int_a_ );
	printArray( out, ist.uchar_a_ );
	printArray( out, ist.ushort_a_ );
	printArray( out, ist.uint_a_ );
	printArray( out, ist.float_a_ );
	printArray( out, ist.double_a_ );
	printArray( out, ist.string_a_ );
	printArray( out, ist.date_a_ );
	printArray( out, ist.time_a_ );
	printArray( out, ist.enum_a_ );
    }
}

