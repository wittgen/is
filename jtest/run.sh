#!/bin/sh
#################################################################
#
#	Test Script for the IS package
#	Created by Sergei Kolos; 10.02.02
#
#################################################################

test_command_result()
{
  echo -n "Testing command status ........... "
  if test $? -ne 0
  then
        echo ERROR
	exit 1
  fi
  echo OK
}

if test $TDAQ_IPC_INIT_REF
then
	ref_param="-Dtdaq.ipc.init.ref=$TDAQ_IPC_INIT_REF"
fi

echo Java command is {$1 -classpath $2 $ref_param TestReceiver IStest 1 \".*\"}
$1 -classpath $2 $ref_param TestReceiver IStest 1 ".*" &
test_command_result

echo Java command is {$1 -classpath $2 $ref_param TestSubscriber IStest 1 \".*\"}
$1 -classpath $2 $ref_param TestSubscriber IStest 1 ".*" &
test_command_result
sleep 5

echo Java command is {$1 -classpath $2 $ref_param TestRepository IStest}
$1 -classpath $2 $ref_param TestRepository IStest &
test_command_result
sleep 5

echo Java command is {$1 -classpath $2 $ref_param TestNamedInfo IStest}
$1 -classpath $2 $ref_param TestNamedInfo IStest
test_command_result

sleep 11
