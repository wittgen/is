import ipc.*;
import is.*;
import java.lang.reflect.*;
import java.lang.Runnable;
 
public class TestSubscriber implements InfoListener
{
    private boolean is_re;
    
    public TestSubscriber( boolean is_re )
    {
    	this.is_re = is_re;
    }
    
    public void infoCreated( InfoEvent e )
    {
        System.out.print( "TestSubscriber:: " + ( is_re ? "regular expression callback: " : "simple callback: " ) );
        System.out.println( "TestSubscriber:: Info " + e.getName() + " is created" );
	AnyInfo ai = new AnyInfo( );
	e.getValue( ai );    
	System.out.println( ai.toString() );    
    }
    
    public void infoUpdated( InfoEvent e )
    {
        System.out.print( "TestSubscriber:: " + ( is_re ? "regular expression callback: " : "simple callback: " ) );
        System.out.println( "TestSubscriber:: Info " + e.getName() + " is updated" );
	AnyInfo ai = new AnyInfo( );
	e.getValue( ai );    
	System.out.println( ai.toString() );    
    }
    
    public void infoDeleted( InfoEvent e )
    {
        System.out.print( "TestSubscriber:: " + ( is_re ? "regular expression callback: " : "simple callback: " ) );
        System.out.println( "TestSubscriber:: Info " + e.getName() + " is deleted" );
	AnyInfo ai = new AnyInfo( );
	e.getValue( ai );    
    
	System.out.println( ai.toString() );
    }
    
    public static void main(String args[])
    {    
	if ( args.length < 2 )
	{
            System.err.println( "You must provide the IS server name, IS info name and subscription expression." );
            System.exit(2);
	}
    
	TestSubscriber r1 = new TestSubscriber( false );
	TestSubscriber r2 = new TestSubscriber( true );
    
	Partition p = ( args.length < 4 ) ? new Partition() : new Partition( args[3] );
    
	Repository rep = new Repository( p );
        try
	{
            rep.subscribe( args[0] + "." + args[1], r1 );
            rep.subscribe( args[0], new Criteria( java.util.regex.Pattern.compile( args[2] ) ), r2 );

	    ///////////////////////////////////////////////////
	   // Sleep for 10 seconds and stop the receiver
	   ///////////////////////////////////////////////////
           Thread.sleep( 10000 );
    
	    rep.unsubscribe( args[0] + "." + args[1] );
	    rep.unsubscribe( args[0], new Criteria( java.util.regex.Pattern.compile( args[2] ) ) );
	}
	catch ( java.lang.InterruptedException e )
	{
            System.err.println( "TestSubscriber:: Interrupted" );
            System.exit( 1 );
	}
	catch ( Exception e )
	{
            System.err.println( e.getMessage() );
            System.exit( 1 );
	}
    
	System.out.println( "TestSubscriber:: Test completed successfully\n\n" );
    }
    
    
    static void printArray( java.io.PrintStream out, java.lang.Object arr )
    {
	out.print( arr.getClass().getComponentType().getName() + "[" + Array.getLength( arr ) + "] = " );
	for ( int i = 0; i < Array.getLength( arr ); i++ )
	{
	    out.print( Array.get( arr, i ) + " " );
	}
	out.println();
    }	
}
