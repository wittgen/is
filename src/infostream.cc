#include <limits>
#include <sstream>
#include <string>

#include <is/datastream.h>
#include <is/infodictionary.h>
#include <is/infostream.h>
#include <is/server.h>

namespace
{
    int getTimeout() {
        int timeout = 10;
        const char *env = getenv("TDAQ_IS_STREAM_TIMEOUT");
        if (env != 0) {
            std::istringstream in(env);
            long val = 0;
            in >> val;
            if (val > 1000) {
                timeout = val/1000;
            }
        }
        return timeout;
    }

    const int Timeout = getTimeout();
}

class ISInfoStreamImpl: public IPCObject<POA_is::stream>
{
public:
    ISInfoStreamImpl(ISInfoStream & stream)
	: m_stream(stream)
    {
    }

    ~ISInfoStreamImpl()
    {
	ERS_DEBUG( 1, "Stream implementation is destroyed");
    }

private:
    void push(const is::info_list & ll)
    {
	for (CORBA::ULong i = 0; i < ll.length(); ++i )
	    m_stream.add(ll[i]);
    }

    void eof()
    {
	m_stream.constructionCompleted();
    }

private:
    ISInfoStream & m_stream;
};

////////////////////////////////////////////////////////////////////////////////////////////////
ISInfoStream::InfoHolder::InfoHolder(const std::string & sid, const char * oid,
    const is::type & type, const is::value & value)
    : m_oid(oid),
      m_name(sid + '.' + m_oid),
      m_type(type),
      m_value(value)
{
    ;
}

////////////////////////////////////////////////////////////////////////////////////////////////
ISInfoStream::ISInfoStream(
    const IPCPartition & partition, const std::string & server_name,
    const ISCriteria & criteria, bool synchronous, int history_depth,
    ISInfo::HistorySorted order)
    : m_partition(partition),
      m_server_name(server_name),
      m_pos(0),
      m_eof(false),
      m_impl(new ISInfoStreamImpl(*this))
{
    IS_INVOKE(create_stream( criteria, m_impl->_this(), history_depth, order ),
	m_partition, m_server_name, std::string())

    if (!synchronous)
	return;

    {
	std::unique_lock<std::mutex> lock(m_mutex);

	// if no new info arrived within the Timeout seconds
	// then something is wrong with the IS server
	// so the construction may never finish by itself
	while (!m_eof)
	{
	    unsigned int size = m_info.size();
	    m_condition.wait_for(lock, std::chrono::seconds(Timeout));
	    if (!m_eof && size == m_info.size())
	    {
		ERS_LOG("The number of received objects did not change in the last " << Timeout << " seconds");
		return;
	    }
	}
    }

    ERS_DEBUG(1, "Stream construction is finished, " << m_info.size() << " object values were received");

    m_impl->_destroy(true);
    m_impl = 0;
}

ISInfoStream::~ISInfoStream()
{
    if (m_impl)
	m_impl->_destroy(true);

    for (size_t i = 0; i < m_info.size(); ++i)
    {
	delete m_info[i];
    }
}

void ISInfoStream::constructionCompleted()
{
    ERS_DEBUG(1, "Received eof from the IS server");
    std::unique_lock<std::mutex> lock(m_mutex);
    m_eof = true;
    m_condition.notify_one();
}

void ISInfoStream::add(const is::info_history & ih)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    int size = (int)m_info.size();
    for (size_t i = 0; i < ih.values.length(); ++i)
    {
        m_info.push_back(new InfoHolder(
                m_server_name, ih.name, ih.type, ih.values[i]));
    }

    if (m_pos == size) {
	m_condition.notify_one();
    }
}

bool ISInfoStream::eof() const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    m_condition.wait_for(lock, std::chrono::seconds(Timeout),
	[this]{return m_pos != (int)m_info.size() || m_eof;});

    return (m_pos == (int)m_info.size());
}

void ISInfoStream::skip(size_t entries)
{
    for (size_t i = 0; i < entries; ++i)
    {
	if (eof())
	    break;

	std::unique_lock<std::mutex> lock(m_mutex);
	delete m_info[m_pos];
	m_info[m_pos] = 0;
	++m_pos;
    }
}

ISInfoStream &
ISInfoStream::operator>>(ISInfo & info)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    __assert_position__();

    if (not m_info[m_pos]->m_value.data.length()) {
        ISInfoDictionary d(m_partition);
        d.getValue(m_info[m_pos]->m_name, m_info[m_pos]->m_value.attr.tag, info);
    }
    else {
        is::idatastream in(m_info[m_pos]->m_value.data);
        info.fromWire(m_partition, m_info[m_pos]->m_name.c_str(), m_info[m_pos]->m_type,
            m_info[m_pos]->m_value, in);
    }

    delete m_info[m_pos];
    m_info[m_pos] = 0;
    ++m_pos;
    return *this;
}

int ISInfoStream::tag() const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    __assert_position__();

    return m_info[m_pos]->m_value.attr.tag;
}

ISType ISInfoStream::type() const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    __assert_position__();

    return ISType(m_info[m_pos]->m_type);
}

OWLTime ISInfoStream::time() const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    __assert_position__();

    return OWLTime(m_info[m_pos]->m_value.attr.time / 1000000,
	m_info[m_pos]->m_value.attr.time % 1000000);
}

const std::string &
ISInfoStream::name() const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    __assert_position__();

    return m_info[m_pos]->m_name;
}

const std::string &
ISInfoStream::objectName() const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    __assert_position__();

    return m_info[m_pos]->m_oid;
}
